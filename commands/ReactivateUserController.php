<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Log;
use app\models\LogAcaoEnum;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ReactivateUserController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {
        $trans = Yii::$app->beginTransaction();
        try {
            $sql = "
            UPDATE USUARIO SET STATUS = 0 WHERE ID IN (
                SELECT FK_CODIGO FROM `log` 
                WHERE 
                ACAO = " . LogAcaoEnum::CANCEL . "
                AND TABELA = 'usuario'
                AND now() > date_add(data, INTERVAL 2 DAY)
            )";

            $command = Yii::$app->db->createCommand($sql);
            $command->execute();
            $trans->commit();
        } catch (\Exception $e) {
            $trans->rollBack();
            return ExitCode::OSERR;
        }
        return ExitCode::OK;
    }
}
