<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/ 
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Patrick Tapajós <patrick.tapajos@gmail.com>
 * @since 2.0
 */
class ConsultaAtualizarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/constants.js',
        'js/vue-components/consulta/atualizar/index.js'
    ];
    public $depends = [
        'app\assets\CalendarAsset',
        'app\assets\VueAsset',
    ];
}
