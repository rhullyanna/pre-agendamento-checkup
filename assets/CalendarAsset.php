<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/ 
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Patrick Tapajós <patrick.tapajos@gmail.com>
 * @since 2.0
 */
class CalendarAsset extends AssetBundle
{
    // public $basePath = '@webroot';
    public $baseUrl = 'https://cdn.jsdelivr.net/npm/';
    public $css = [
        '@fullcalendar/core@4.4.0/main.css',
        '@fullcalendar/daygrid@4.4.0/main.css',
        '@fullcalendar/timegrid@4.4.0/main.css',
        // '@fullcalendar/list@4.4.0/main.css',

    ];
    public $js = [
        '@fullcalendar/core@4.4.0/main.js',
        '@fullcalendar/daygrid@4.4.0/main.js',
        '@fullcalendar/timegrid@4.4.0/main.js',
        // '@fullcalendar/list@4.4.0/main.js',

    ];
    public $depends = [];
}
