<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/ 
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Patrick Tapajós <patrick.tapajos@gmail.com>
 * @since 2.0
 */
class CirurgicoAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://cdn.jsdelivr.net/npm/vue-loading-overlay@3.3.2/dist/vue-loading.css',
        'http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
    ];
    public $js = [
        'js/constants.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js',
        'https://cdn.jsdelivr.net/npm/vue-loading-overlay@3.3.2/dist/vue-loading.min.js',
        'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
        'js/vue-components/cirurgico/index.js'
    ];
    public $depends = [
        'app\assets\VueAsset',
    ];
}
