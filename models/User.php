<?php

namespace app\models;

use app\models\PermissaoEnum;
use app\modules\paciente\models\Paciente;
use app\modules\admin\models\Administrador;
use app\modules\admin\models\Medico;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $roleId;
    public $name;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $rule;
    public $status;

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $model = Usuario::findOne(['id' => $id]);
        if ($model) {
            $user = new User;
            $user->id = $id;
            $user->rule = $model->permissao;

            switch ($user->rule) {
                case PermissaoEnum::PACIENTE:
                    $paciente = Paciente::findOne(['usuario_id' => $id]);
                    $user->roleId = $paciente->id;
                    $user->name = $paciente->nome;
                    break;
                case PermissaoEnum::ADMIN:
                    $admin = Administrador::findOne(['usuario_id' => $id]);
                    $user->roleId = $admin->id;
                    $user->name = $admin->nome;
                    break;
                case PermissaoEnum::MEDICO:
                    $medico = Medico::findOne(['usuario_id' => $id]);
                    $user->roleId = $medico->id;
                    $user->name = $medico->nome;
                    break;

                default:
                    # code...
                    break;
            }

            $user->username = $model->login;

            return $user;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username, $role)
    {
        $model = Usuario::findOne(['login' => $username, 'permissao' => $role]);

        if ($model) {
            $user = new User;
            $user->username = $model->login;
            $user->password = $model->senha;
            $user->status = $model->status;
            $user->id = $model->id;
            return $user;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
