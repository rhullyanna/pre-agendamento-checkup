<?php

namespace app\models;

abstract class LogAcaoEnum
{

    const DELETE       = 0;
    const INSERT       = 1;
    const UPDATE       = 2;
    const CANCEL       = 3;
    const UPDATE_PASS  = 4;
    const LOGIN  = 5;

    public static function list()
    {
        return [
            LogAcaoEnum::INSERT  => 'Cadastro',
            LogAcaoEnum::UPDATE  => 'Atualização',
            LogAcaoEnum::DELETE  => 'Exclusão',
            LogAcaoEnum::CANCEL  => 'Cancelamento',
            LogAcaoEnum::UPDATE_PASS  => 'Atualização de senha',
            LogAcaoEnum::LOGIN  => 'Login',
        ];
    }
}
