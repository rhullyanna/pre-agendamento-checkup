<?php 

namespace app\models;

 abstract class SexoEnum {
	const MASCULINO       = 'M';
	const FEMININO      = 'F';
	const OUTROS    = 'O';

	public static function list(){
		return [
			SexoEnum::MASCULINO => 'Masculino',
			SexoEnum::FEMININO => 'Feminino',
            SexoEnum::OUTROS  => 'Outros',
		];
	}
}