<?php

namespace app\models;

use app\models\StatusEnum;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $role;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            [['username'], 'validatePermission'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Usuário',
            'password' => 'Senha',
            'rememberMe' => 'Manter conectado'
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, 'User not registered');
            } else if (!$user->validatePassword(md5($this->password))) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    public function validatePermission($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ($user->status == StatusEnum::INATIVO) {
                $this->addError($attribute, 'Usuário está inativo e não pode acessar o sistema.');
            }
        }
    }


    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        $this->username = preg_replace('/[^0-9]/', '', $this->username);
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username, $this->role);
        }

        return $this->_user;
    }

    public function loginLog()
    {
        $log = new Log();
        $log->usuario_id = Yii::$app->user->identity->id;
        $log->fk_codigo = Yii::$app->user->identity->id;
        $log->acao = LogAcaoEnum::LOGIN;
        $log->tabela = 'usuario';
        $log->save(false);
    }
}
