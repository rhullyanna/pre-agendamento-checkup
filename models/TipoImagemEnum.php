<?php

namespace app\models;

abstract class TipoImagemEnum
{
    const IDENTIFICACAO   = 1;
    const CONVENIO      = 2;
    const PEDIDO_MEDICO = 3;

    public static function list()
    {
        return [
            TipoImagemEnum::IDENTIFICACAO => 'Identificação',
            TipoImagemEnum::CONVENIO => 'Convênio',
            TipoImagemEnum::PEDIDO_MEDICO => 'Pedido Médico',
        ];
    }
}
