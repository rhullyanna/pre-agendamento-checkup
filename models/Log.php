<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property int $id
 * @property string $data
 * @property string $acao
 * @property int $fk_codigo
 * @property string $tabela
 * @property int $usuario_id
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data', 'acao', 'fk_codigo', 'tabela', 'usuario_id'], 'required'],
            [['data'], 'safe'],
            [['fk_codigo', 'usuario_id'], 'integer'],
            [['acao', 'tabela'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'acao' => 'Acao',
            'fk_codigo' => 'Fk Codigo',
            'tabela' => 'Tabela',
            'usuario_id' => 'Usuario',
        ];
    }
}
