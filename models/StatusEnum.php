<?php

namespace app\models;

abstract class StatusEnum
{
	const ATIVO       = 0;
	const INATIVO      = 1;
	const CANCELADO      = 9;

	public static function list()
	{
		return [
			StatusEnum::ATIVO => 'Ativo',
			StatusEnum::INATIVO => 'Inativo',
			StatusEnum::CANCELADO => 'Cancelado',
		];
	}
}
