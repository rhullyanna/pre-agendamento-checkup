<?php 

namespace app\models;

 abstract class PermissaoEnum {
	const ADMIN       = '0';
	const MEDICO      = '1';
	const PACIENTE    = '2';

	public static function list(){
		return [
			PermissaoEnum::ADMIN => 'Administrador',
			PermissaoEnum::MEDICO => 'Médico',
      		PermissaoEnum::PACIENTE  => 'Paciente',
		];
	}
}