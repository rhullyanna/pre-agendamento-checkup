<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario_permissao".
 *
 * @property int $id
 * @property int $usuario_id
 * @property string $descricao_permissao
 * @property int $status
 *
 * @property Usuario $usuario
 */
class UsuarioPermissao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario_permissao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id', 'descricao_permissao', 'status'], 'required'],
            [['usuario_id', 'status'], 'integer'],
            [['descricao_permissao'], 'string', 'max' => 60],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'descricao_permissao' => 'Descricao Permissao',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
}
