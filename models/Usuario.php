<?php

namespace app\models;

use app\components\LogActiveRecord;
use app\models\PermissaoEnum;
use app\modules\paciente\models\Paciente;
use yiibr\brvalidator\CpfValidator;
use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $login
 * @property string $senha

 * @property UsuarioPermissao[] $usuarioPermissaos
 */
class Usuario extends LogActiveRecord
{

    public $_senha_atual;
    public $_nova_senha;
    public $_nova_senha_confirmacao;


    const SCENARIO_FORGOT_PASSWORD    = 'forgot_password';
    const SCENARIO_UPDATE_PASSWORD    = 'update_password';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_FORGOT_PASSWORD] = ['login'];
        $scenarios[self::SCENARIO_UPDATE_PASSWORD] = ['login', '_senha_atual', '_nova_senha', '_nova_senha_confirmacao'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login'], 'required', 'on' => [self::SCENARIO_FORGOT_PASSWORD]],
            [['login', '_senha_atual', '_nova_senha', '_nova_senha_confirmacao'], 'required', 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            ['_nova_senha_confirmacao', 'compare', 'compareAttribute' => '_nova_senha', 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            [['_nova_senha'], 'validateNewPass', 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            [['_senha_atual'], 'validateOldPass', 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            [['login'], CpfValidator::className()],
            [['login'], 'string', 'max' => 14],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'CPF',
            'senha' => 'Senha',
            '_nova_senha_confirmacao' => 'Confirmar Nova Senha'
        ];
    }


    public function getPaciente()
    {
        return $this->hasOne(Paciente::className(), ['usuario_id' => 'id']);
    }

    public function getMedico()
    {
        return $this->hasOne(Medico::className(), ['usuario_id' => 'id']);
    }


    public function beforeValidate()
    {
        $this->login = preg_replace('/[^0-9]/', '', $this->login);
        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->_senha_atual = $this->generatePass();
            $this->status = StatusEnum::ATIVO;
        }

        if ($this->scenario == self::SCENARIO_UPDATE_PASSWORD) {
            $this->senha = md5($this->_nova_senha);
            $this->getLog()->acao = LogAcaoEnum::UPDATE_PASS;
        }

        return parent::beforeSave($insert);
    }

    //TODO implementar inserção na tabela de usuario_permissao
    // public function afterSave($insert, $changedAttributes)
    // {
    //     if($insert){
    //         $this->sendEmailPass($this->_senha_atual);
    //     }
    // }

    public function sendEmailPass($email, $senha = null)
    {

        if ($senha == null) {
            $senha = $this->_senha_atual;
        }

        $subject = Yii::$app->name;

        if ($this->scenario == self::SCENARIO_FORGOT_PASSWORD) {
            $subject = Yii::$app->name . " - Alteração de senha";
        }
        $mailer = Yii::$app->mailer;
        return $mailer->compose(['html' => 'credencial'], ['model' => $this, 'senha' => $senha])
            ->setFrom(env('EMAIL_USER'))
            ->setTo($email)
            ->setSubject($subject)
            ->send();
    }

    public function generatePass()
    {

        /*Gera senha aleatória*/
        $senha = strtolower($this->getRandomString());
        /*Encripta para salvar*/
        $this->senha = md5($senha);
        /*Retorna senha não encriptada para enviar para email*/
        return $senha;
    }

    public function getRandomString($length = 6)
    {

        $validCharacters = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ0123456789";
        $validCharNumber = strlen($validCharacters);

        $result = "";

        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, $validCharNumber - 1);
            $result .= $validCharacters[$index];
        }

        return $result;
    }

    public function validateNewPass($attribute, $params)
    {

        if (($this->_nova_senha != null) && (strlen($this->_nova_senha) < 4 || strlen($this->_nova_senha) > 8)) {
            $this->addError($attribute, 'Nova senha deve conter entre 4 e 8 caracteres.');
            return false;
        }
        return true;
    }

    public function validateOldPass($attribute, $params)
    {

        if (($this->_senha_atual !=  null) && ($this->senha != md5($this->_senha_atual))) {
            $this->addError($attribute, 'Senha atual incorreta.');
            return false;
        }
        return true;
    }

    public function isFirstLogin()
    {
        $sql = "SELECT * FROM `log` WHERE 
                usuario_id = " . Yii::$app->user->identity->id . "
                and acao = " . LogAcaoEnum::LOGIN;
        $logCount = count(Log::findBySql($sql)->all());
        return $logCount == 1;
    }
}
