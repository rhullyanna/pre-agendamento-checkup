<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "medico".
 *
 * @property int $id
 * @property string $nome
 * @property string $cpf
 * @property string $crm
 * @property int $usuario_id
 * @property int $medico_id
 *
 * @property Usuario $usuario
 */
class Medico extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'medico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'cpf', 'crm', 'usuario_id'], 'required'],
            [['usuario_id', 'medico_id'], 'integer'],
            [['nome'], 'string', 'max' => 150],
            [['cpf'], 'string', 'max' => 11],
            [['crm'], 'string', 'max' => 15],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'cpf' => 'Cpf',
            'crm' => 'Crm',
            'usuario_id' => 'Usuario ID',
            'medico_id' => 'Medico ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
}
