<?php

namespace app\models;

abstract class FormaEnum
{
    const SITE       = 0;

    public static function list()
    {
        return [
            FormaEnum::SITE  => 'Site',
        ];
    }
}
