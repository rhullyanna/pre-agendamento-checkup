<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SexoEnum;
use app\assets\PacienteAsset;
use yii\helpers\Url;

PacienteAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\modules\paciente\models\Paciente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paciente-form" id="paciente">

    <?php $form = ActiveForm::begin([
        'id' => 'paciente-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div v-cloak id="errorsForm" class="error-summary" v-show="errorsList.length > 0">
        <p>Por favor, corrija os seguintes erros:</p>
        <ul v-for="e in errorsList">
            <li>{{ e[0] }}</li>
        </ul>
    </div>

    <div class="row">

        <?= $form->field($model, 'id')->hiddenInput(['id' => 'id'])->label(false) ?>
        <?= $form->field($model, 'cd_pessoa_fisica')->hiddenInput(['id' => 'cd_pessoa_fisica', 'v-model' => 'patient_id'])->label(false) ?>
        <div class="form-group">
            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'cpf')->textInput(['id' => 'cpf', 'readonly' => !$model->isNewRecord, 'v-model' => 'cpf']) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'rg')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'data_nascimento')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '99/99/9999',
                    'options' => ['class' => 'form-control', 'id' => 'data_nascimento']
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'sexo')->dropDownList(SexoEnum::list(), ['prompt' => 'Selecione >>']) ?>
            </div>
            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'telefone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '(99)99999-9999'
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'nome_mae')->textInput(['maxlength' => true]) ?>
            </div>

        </div>

        <div class="col-lg-12 col-sm-12">
            <label>O documento deverá ser enviado como imagem, em formato JPEG, JPG ou PNG.</label>
            <div v-cloak v-show="imageError" class="error-summary">
                <span class="text-danger">{{ imageMessage }}</span>
            </div>
            <div class="form-group field required">
                <?= Html::activeLabel($model, 'doc_identificacao', ['class' => 'control-label']); ?>
                <div class="img-document-cover">
                    <img v-bind:src=" imageDocument " class="img-document" />
                </div>
                <input id="doc_identificacao" type="file" name="Paciente[doc_identificacao]" class="form-control" @change="processFileDocument($event)" />
            </div>
            <?= $form->field($model, 'doc_identificacao')->hiddenInput(['id' => 'docImageHidden'])->label(false); ?>
        </div>
    </div>

    <div class="form-group">
        <button type="button" class="btn btn-primary" @click.prevent="submit()">Salvar</button>
        <?= Html::a('Cancelar', ['default/index'], ['class' => 'btn btn-danger']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

</div>