<?php

use app\models\StatusEnum;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\paciente\models\PacienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gerenciar Acesso de Paciente';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paciente-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nome',
            'cpf',
            'rg',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Ações',
                'options' => ['width' => '70px'],
                'template' => '{view} {activate} {deactivate}',
                'buttons'  => [
                    'activate' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', ['/usuario/reativar', 'id' => $model->usuario->id], [
                            'class' => '',
                            'title' => 'Reativar Acesso',
                            'data' => [
                                'confirm' => 'Deseja reativar o acesso deste usuário?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                    'deactivate' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-remove"></span>', ['/usuario/desativar', 'id' => $model->usuario->id], [
                            'class' => '',
                            'title' => 'Desativar Acesso',
                            'data' => [
                                'confirm' => 'Deseja desativar o acesso deste usuário?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
                'visibleButtons' => [
                    'activate' => function ($model) {
                        return $model->usuario->status == StatusEnum::INATIVO;
                    },
                    'deactivate' => function ($model) {
                        return $model->usuario->status == StatusEnum::ATIVO;
                    },
                ]
            ]
        ],
    ]); ?>
</div>