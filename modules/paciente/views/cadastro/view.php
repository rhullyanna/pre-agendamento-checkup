<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\PermissaoEnum;

/* @var $this yii\web\View */
/* @var $model app\modules\paciente\models\Paciente */

$this->title = 'Visualizar Dados #' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="paciente-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can(PermissaoEnum::PACIENTE)) : ?>
            <?= Html::a('Atualizar Dados', ['atualizar', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <?php if (Yii::$app->user->can(PermissaoEnum::ADMIN)) : ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <fieldset>
        <legend>Dados Pessoais</legend>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'nome',
                'cpf',
                'rg',
                'data_nascimento',
                'email:email',
                'nome_mae',
                'nome_convenio',
            ],
        ]) ?>
    </fieldset>

    <fieldset>
        <legend>Documentação</legend>
        <?php foreach ($model->documentos as $documento) : ?>
            <div class="col-lg-4 col-sm-12">
                <label><?= $documento->getType(); ?></label>
                <div class="img-document-cover">
                    <img src="<?= $documento->imagem; ?>" class="img-document" />
                </div>
            </div>

        <?php endforeach; ?>
    </fieldset>

</div>