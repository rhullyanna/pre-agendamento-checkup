<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\paciente\models\Paciente */

$this->title = 'Atualizar Dados #' . $model->id;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paciente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
