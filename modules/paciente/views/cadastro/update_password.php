<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Alterar Senha';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="usuario-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<div class="usuario-form">

		<?php if ($model->isFirstLogin()) : ?>
			<div class="alert alert-danger">
				A senha enviada para você foi criada de maneira aleatória, procure atualizar para uma que seja melhor para você.
			</div>
		<?php endif; ?>

		<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, '_senha_atual')->passwordInput(); ?>

		<?= $form->field($model, '_nova_senha')->passwordInput(); ?>

		<?= $form->field($model, '_nova_senha_confirmacao')->passwordInput(); ?>

		<div class="form-group">
			<?= Html::submitButton('Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>
</div>