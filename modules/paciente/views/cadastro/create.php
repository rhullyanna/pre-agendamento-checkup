<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\paciente\models\Paciente */

$this->title = 'Cadastro de Paciente';
// $this->params['breadcrumbs'][] = ['label' => 'Agendamento Exames', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="paciente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>