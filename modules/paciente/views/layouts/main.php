<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => '<div class="pull-left navbar-logo"><img height="100" width="300" src="' . Yii::$app->request->baseUrl . '/images/logo-checkup.png" alt="Check Up"></div>',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'sys-navbar navbar-fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-left'],
            'items' => [
                ['label' => 'Home', 'url' => ['/paciente/default/index']],
                [
                    'label' => 'Exame',
                    'url' => ['#'],
                    'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => 'Novo Pré-Agendamento', 'url' => ['/paciente/agendamentoexame/novo']],
                        ['label' => 'Meus Pré-Agendamentos', 'url' => ['/paciente/agendamentoexame/index']],
                    ],
                ],

                [
                    'label' => 'Consulta',
                    'url' => ['#'],
                    'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => 'Novo Pré-Agendamento', 'url' => ['/paciente/agendamentoconsulta/novo']],
                        ['label' => 'Meus Pré-Agendamentos', 'url' => ['/paciente/agendamentoconsulta/index']],
                    ],
                ],

                ['label' => 'Alterar Dados', 'url' => ['/paciente/cadastro/atualizar?id=' . Yii::$app->user->identity->roleId], 'visible' => !Yii::$app->user->isGuest],
                ['label' => 'Alterar Senha', 'url' => ['/paciente/cadastro/alterarsenha'], 'visible' => !Yii::$app->user->isGuest],
                Yii::$app->user->isGuest ? (['label' => 'Login', 'url' => ['/paciente/default/login']]) : ('<li>'
                    . Html::beginForm(['/paciente/default/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->name . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>')
            ],
        ]);
        NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container text-center">
            <div class="col-lg-6">
                <p class="pull-left">&copy; Copyright – Check Up Hospital - CNPJ: 05460308/0001-33</p>
            </div>
            <div class="col-lg-6">
                <p class="pull-right"><?= Yii::powered() ?> por Rise</p>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>