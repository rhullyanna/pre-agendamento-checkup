<?php

/* @var $this yii\web\View */

$this->title = 'Sistema de Pré-agendamento Checkup';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Bem vindo ao Sistema de Pré-agendamento do Hospital Checkup!</h1>

        <p class="lead">Escolha o serviço desejado abaixo.</p>

    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="panel panel-index admin">
                    <div class="panel-heading">
                        <h3>Exame</h3>
                    </div>
                    <div class="panel-body">
                        <p>Marcação de exames</p>
                        <p><a class="btn btn-default" href="/paciente/agendamentoexame/novo">Pré-agendar &raquo;</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="panel panel-index paciente">
                    <div class="panel-heading">
                        <h3>Consulta</h3>
                    </div>
                    <div class="panel-body">
                        <p>Marcação de consultas</p>
                        <p><a class="btn btn-default" href="/paciente/agendamentoconsulta/novo">Pré-agendar &raquo;</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>