<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\paciente\models\AgendamentoExameSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agendamento-exame-search">

    <fieldset>
        <legend>Filtros</legend>
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'tipo_id') ?>
            </div>

            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'status') ?>
            </div>
            <div class="col-lg-6 col-sm-12">
                <?= $form->field($model, 'data_cadastro') ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </fieldset>
</div>