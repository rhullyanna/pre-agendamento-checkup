<?php
// _list_item.php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\StatusEnum;

?>


<div class="card">
    <?php if ($model->status != StatusEnum::CANCELADO) : ?>
        <div class="panel panel-primary">
        <?php else : ?>
            <div class="panel panel-danger">
            <?php endif; ?>
            <div class="panel-heading">Solicitação #<?= $model->id; ?></div>
            <div class="panel-body">
                <div class="col-md-2 col-lg-12">
                    <strong> Médico: </strong><?= $model->nome_medico; ?>
                </div>
                <div class="col-md-2 col-lg-12">
                    <strong> Exame: </strong><?= $model->exame_tasy_descricao; ?>
                </div>
                <div class="col-md-2 col-lg-12">
                    <strong> Status: </strong><?= $model->getStatustext(); ?>
                </div>
                <div class="col-md-2 col-lg-12">
                    <strong> Horário: </strong><?= $model->horario; ?>
                </div>
                <div class="col-md-2 col-lg-12">
                    <strong> Data de Cadastro: </strong><?= date('d/m/Y', strtotime($model->data_cadastro)); ?>
                </div>
            </div>
            <div class="panel-footer">
                <?php if ($model->status != StatusEnum::CANCELADO) : ?>
                    <?= Html::a('<button class="btn btn-warning"><span class="glyphicon glyphicon-eye-open"></span> Visualizar</button>', Url::to('view?id=' . $model->id), ['title' => 'Visualizar']) ?>
                    <?= Html::a('<button class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>Cancelar</button>', ['cancelar', 'id' => $model->id], [
                        'class' => '',
                        'title' => 'Cancelar',
                        'data' => [
                            'confirm' => 'Deseja cancelar este pré-agendamento?',
                            'method' => 'post',
                        ],
                    ]); ?>
                <?php endif; ?>

            </div>
            </div>
        </div>