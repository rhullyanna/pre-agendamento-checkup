<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\paciente\models\AgendamentoExame */

$this->title = 'Alterar Pré-Agendamento de Exame #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pré-Agendamentos - Exame', 'url' => ['index']];

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agendamento-exame-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>