<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\paciente\models\AgendamentoExame */

$this->title = 'Pré-Agendamento de Exame';
$this->params['breadcrumbs'][] = ['label' => 'Pré-Agendamentos de Exame', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agendamento-exame-create">
    <div class="alert alert-info text-center" role="alert">
        Este cadastro é um pré-agendamento, nossa equipe entrará em contato para confirmação em até 24h.
    </div>

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>