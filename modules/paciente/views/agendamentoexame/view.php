<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\paciente\models\AgendamentoExame */

$this->title = 'Visualizar Pré-agendamento de Exame #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pré-agendamento de Exame', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="agendamento-exame-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <fieldset>
        <legend>Dados Gerais</legend>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'exame_tasy_descricao',
                'nome_medico',
                'horario',
                'paciente.nome',
                'paciente.rg',
                'paciente.data_nascimento',
                ['type' => 'raw', 'attribute' => 'paciente.sexo', 'value' => $model->paciente->getSexText()],
                'convenio_tasy_nome',
                'numero_convenio',

            ],
        ]) ?>
    </fieldset>

    <fieldset>
        <legend>Documentação</legend>
        <?php foreach ($model->documentos as $documento) : ?>
            <div class="col-lg-4 col-sm-12">
                <label><?= $documento->getType(); ?></label>
                <div class="img-document-cover">
                    <img src="<?= $documento->imagem; ?>" class="img-document" />
                </div>
            </div>

        <?php endforeach; ?>
    </fieldset>

</div>