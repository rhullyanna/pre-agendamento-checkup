<?php

use yii\widgets\ActiveForm;
use app\assets\ExameAtualizarAsset;
use app\models\SexoEnum;

ExameAtualizarAsset::register($this);
?>

<div class="agendamento-exame-form" id="exame">

    <?php $form = ActiveForm::begin([
        'id' => 'agendamento-exame-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
    ]); ?>

    <div class="alert-danger">
        <?= $form->errorSummary([$model]); ?>
    </div>

    <div>
        <?= $form->field($model, 'id')->hiddenInput(['id' => 'id'])->label(false) ?>
        <?= $form->field($model, 'nr_seq_proc_interno')->hiddenInput(['id' => 'nr_seq_proc_interno'])->label(false) ?>
        <?= $form->field($model, 'cd_agenda')->hiddenInput(['id' => 'cd_agenda'])->label(false) ?>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'exame_tasy_descricao')->textInput(['id' => 'exame_tasy_descricao', 'disabled' => true]); ?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'convenio_tasy_nome')->textInput(['id' => 'convenio_tasy_nome', 'disabled' => true]) ?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'numero_convenio')->textInput(['id' => 'numero_convenio', 'disabled' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'validade_convenio')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '99/99/9999',
                    'options' => ['disabled' => true]
                ]) ?>
            </div>

        </div>

        <br>

        <div v-show="calendarLoaded" id="fieldsetCalendar">

            <div class="form-group">
                <?= $form->field($model, 'horario')->textInput(['v-model' => 'time', 'id' => 'horario', 'readonly' => 'readonly']); ?>
            </div>

            <div id="calendar"></div>

            <br>
            <div class="form-group">
                <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i> Atualizar</button>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>