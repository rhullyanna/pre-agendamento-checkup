<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\paciente\models\AgendamentoExameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pré-Agendamentos de Exame';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agendamento-exame-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Pré-Agendar Exame', ['novo'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'options' => [
                'tag' => 'div',
                'class' => 'list-wrapper',
                'id' => 'list-wrapper',
            ],
            'layout' => "{summary}\n{items}\n{pager}",
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_list_item', ['model' => $model]);
            },
        ]);
    ?>
</div>