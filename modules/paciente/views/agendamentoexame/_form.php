<?php

use yii\widgets\ActiveForm;
use app\assets\ExameNovoAsset;
use yii\helpers\Html;
use yii\helpers\Url;

ExameNovoAsset::register($this);
?>

<div class="agendamento-exame-form" id="exame">

    <?php $form = ActiveForm::begin([
        'id' => 'agendamento-exame-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
    ]); ?>

    <div v-cloak id="errorsForm" class="error-summary" v-show="erroForm">
        <p>Por favor, corrija os seguintes erros:</p>
        <ul v-for="e in errorsList">
            <li>{{ e[0] }}</li>
        </ul>
    </div>

    <div class="row">
        <div v-cloak v-show="calendarError" class="error-summary">
            <span class="text-danger">{{ calendarErrorMessage }}</span>
        </div>

        <?= $form->field($model, 'id')->hiddenInput(['id' => 'id'])->label(false) ?>
        <?= $form->field($model, 'paciente_id')->hiddenInput(['v-model' => 'user.id'])->label(false) ?>


        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'is_convenio')->dropDownList([0 => 'Particular', 1 => 'Outro'], ['prompt' => 'Selecione >>', 'v-on:change' => 'checkInsurance']) ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <div class="form-group required">
                <?= Html::activeLabel($model, 'convenio_id', ['class' => 'control-label']); ?>
                <select class="form-control" id="insuranceField" name="AgendamentoExame[convenio_id]" v-model="insurance" v-bind:disabled="!withInsurance" v-on:change="loadExam">
                    <option value=''>Selecione </option>
                    <option v-for='data in insuranceList' :value='data.id'>{{ data.descricao }}</option>
                </select>
                <?= $form->field($model, 'convenio_tasy_nome')->hiddenInput(['id' => 'convenio_tasy_nome'])->label(false); ?>
                <?= $form->field($model, 'nr_carteirinha')->hiddenInput(['id' => 'nr_carteirinha'])->label(false); ?>
            </div>
        </div>

        <div v-cloak v-show="withInsurance">
            <div class="col-lg-6 col-sm-12 required">
                <?= Html::activeLabel($model, 'numero_convenio', ['class' => 'control-label']); ?>

                <?= $form->field($model, 'numero_convenio')->textInput(
                    ['id' => 'numeroconvenio', 'v-model' => 'userInsurance.numero_convenio', 'maxlength' => true, 'v-bind:disabled' => "disabledByInsurance"]
                )->label(false) ?>
            </div>
            <div class="col-lg-6 col-sm-12 required">
                <?= Html::activeLabel($model, 'validade_convenio', ['class' => 'control-label']); ?>

                <?= $form->field($model, 'validade_convenio')->textInput(
                    ['id' => 'validade_convenio', 'v-model' => 'userInsurance.validade_convenio', 'v-bind:disabled' => "disabledByInsurance"]
                )->label(false) ?>
            </div>
        </div>

        <div v-cloak v-show="insurance != ''">
            <div class="col-lg-6 col-sm-12">
                <div class="form-group required">
                    <?= Html::activeLabel($model, 'cd_plano', ['class' => 'control-label']); ?>
                    <select class="form-control" id="insuranceField" name="AgendamentoExame[cd_plano]" v-model="plan" v-bind:disabled="disabledByInsurance">
                        <option value=''>Selecione </option>
                        <option v-for='data in planList' :value='data.id'>{{ data.descricao }}</option>
                    </select>
                </div>
            </div>

            <div class="col-lg-6 col-sm-12">
                <div class="form-group required">
                    <?= Html::activeLabel($model, 'cd_categoria', ['class' => 'control-label']); ?>
                    <select class="form-control" id="insuranceField" name="AgendamentoExame[cd_categoria]" v-model="cat" v-bind:disabled="disabledByInsurance">
                        <option value=''>Selecione </option>
                        <option v-for='data in catList' :value='data.id'>{{ data.descricao }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-group required">
                <?= Html::activeLabel($model, 'nr_seq_proc_interno', ['class' => 'control-label']); ?>
                <select class="form-control" name="AgendamentoExame[nr_seq_proc_interno]" id="examField" v-model="exam" @change="setExamInfo()" v-bind:disabled="disableExamField()">
                    <option value=''>Selecione </option>
                    <option v-for='data in examList' :value='data.id'>{{ data.descricao }}</option>
                </select>
                <input type="hidden" id="descricaoHidden" name="AgendamentoExame[exame_tasy_descricao]">
                <input type="hidden" id="cdprocedimentoHidden" name="AgendamentoExame[cd_procedimento]">
                <input type="hidden" id="ieorigemHidden" name="AgendamentoExame[ie_origem_proced]">
                <input type="hidden" id="seqexameHidden" name="AgendamentoExame[nr_seq_exame_lab]">
                <input type="hidden" id="seqagendaHidden" name="AgendamentoExame[nr_seq_agenda]">
            </div>
        </div>

        <div class="col-lg-12 col-sm-12">
            <div class="form-group field required">
                <?= Html::activeLabel($model, 'cd_agenda', ['class' => 'control-label']); ?>

                <select class="form-control" id="doctorField" name="AgendamentoExame[cd_agenda]" v-model="cd_agenda" @change="setCDMedico()" v-bind:disabled="disableExamField()">
                    <option value=''>Selecione </option>
                    <option v-for='data in doctorList' :value='data.id'>{{ data.descricao }}</option>
                </select>
                <?= $form->field($model, 'nome_medico')->hiddenInput(['id' => 'nome_medico'])->label(false); ?>
                <?= $form->field($model, 'cd_medico')->hiddenInput(['id' => 'cd_medico'])->label(false); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div v-cloak v-show="notFound" class="error-summary">
            <span class="text-danger">{{ notFoundMessage }}</span>
        </div>
        <div class="col-lg-12 col-sm-12">
            <div class="form-group text-center">
                <button type="button" class="btn btn-primary" @click.prevent="loadCalendar">Verificar Disponibilidade</button>
            </div>
        </div>
    </div>

    <div v-cloak v-show="calendarLoaded" id="fieldsetCalendar">

        <fieldset v-show="orientations != null">
            <legend>Orientações</legend>
            <div style="text-align: justify">{{ orientations }}</div>
        </fieldset>

        <fieldset>
            <legend>Horário</legend>

            <label class="text-center">Verifique a agenda de horários e selecione (basta clicar em cima do horário desejado)</label>

            <div id="calendar"></div>

            <div class="form-group">
                <?= $form->field($model, 'horario')->textInput(['id' => 'horario', 'v-model' => 'examScheduled', 'readonly' => 'readonly']); ?>
                <?= $form->field($model, 'qt_duracao')->hiddenInput(['id' => 'qt_duracao', 'v-model' => 'duration'])->label(false); ?>
            </div>

            <!-- <button type="button" class="btn btn-primary" @click="verifyLoggedIn()"></i> Solicitar</button> -->
        </fieldset>
    </div>

    <br>

    <div v-cloak v-show="isExamScheduled" id="fieldsetInfo">
        <fieldset>
            <legend>Documentação</legend>

            <label>O(s) documento(s) deverá(ão) ser enviado(s) como imagem, em formato JPEG, JPG, PNG ou PDF.</label>

            <div v-cloak v-show="imageError" class="error-summary">
                <span class="text-danger">{{ imageMessage }}</span>
            </div>

            <div class="row">
                <div class="form-group required">
                    <div class="col-lg-6 col-sm-12">
                        <?= Html::activeLabel($model, 'doc_pedido_medico', ['class' => 'control-label']); ?>
                        <div class="img-document-cover">
                            <img v-bind:src=" medicalRequest " class="img-document" />
                        </div>
                        <input type="file" name="AgendamentoExame[doc_pedido_medico]" class="form-control" @change="processFileMedical($event)" />
                        <?= $form->field($model, 'doc_pedido_medico')->hiddenInput(['id' => 'medImageHidden'])->label(false); ?>

                    </div>

                    <div class="col-lg-6 col-sm-12" v-show="withInsurance">
                        <?= Html::activeLabel($model, 'doc_convenio', ['class' => 'control-label']); ?>
                        <div class="img-document-cover">
                            <img v-bind:src=" imageInsurance " class="img-document" />
                        </div>
                        <input id="doc_convenio" type="file" name="AgendamentoExame[doc_convenio]" class="form-control" @change="processFileInsurance($event)" />
                        <?= $form->field($model, 'doc_convenio')->hiddenInput(['id' => 'convImageHidden'])->label(false); ?>
                    </div>
                </div>
            </div>
        </fieldset>
        <br>

        <fieldset>
            <legend>Termo</legend>
            <div class="form-group required">
                <?= $form->field($model, 'termo_aceite_1')->checkBox(['label' => '<label class="control-label">Entendo que este pré-agendamento não garante o atendimento</label>']) ?>
            </div>
            <div class="form-group required">
                <label>Leia atentamente o documento abaixo</label>
                <object data="<?= Yii::getAlias('@web/file/teste.pdf'); ?>" type="application/pdf" width="100%" height="480px"></object>
                <?= $form->field($model, 'termo_aceite_2')->checkBox(['label' => '<label class="control-label">Li, aceito e concordo</label>']) ?>
            </div>
        </fieldset>

        <div class="form-group text-center">
            <button type="button" class="btn btn-primary" @click.prevent="validate"></i>Pré-Agendar</button>
        </div>
    </div>

</div>

<?php ActiveForm::end(); ?>


</div>