<?php

use yii\widgets\ActiveForm;
use app\assets\ConsultaAtualizarAsset;
use app\models\SexoEnum;

ConsultaAtualizarAsset::register($this);
?>

<div class="agendamento-consulta-form" id="consulta">

    <?php $form = ActiveForm::begin([
        'id' => 'agendamento-consulta-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
    ]); ?>

    <div class="alert-danger">
        <?= $form->errorSummary([$model]); ?>
    </div>

    <div>
        <?= $form->field($model, 'id')->hiddenInput(['id' => 'id'])->label(false) ?>

        <div class="row">
            <div class="col-lg-4">
                <?= $form->field($model, 'tipo_id')->textInput(['id' => 'tipo_id', 'disabled' => true]); ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'especialidade_id')->textInput(['id' => 'especialidade_id', 'disabled' => true]); ?>
            </div>

            <div class="col-lg-4">
                <?= $form->field($model, 'medico_id')->textInput(['id' => 'medico_id', 'disabled' => true]); ?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'nome_paciente')->textInput(['id' => 'nome_paciente', 'disabled' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'rg_paciente')->textInput(['id' => 'rg_paciente', 'disabled' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'dt_nasc_paciente')->textInput(['id' => 'dt_nasc_paciente', 'disabled' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'sexo_paciente')->textInput(['id' => 'sexo_paciente', 'value' => SexoEnum::list()[$model->sexo_paciente], 'disabled' => true]) ?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'convenio_id')->textInput(['id' => 'convenio_id', 'disabled' => true]) ?>
            </div>

            <div class="col-lg-6">
                <?= $form->field($model, 'nome_convenio')->textInput(['id' => 'nome_convenio', 'disabled' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'numero_convenio')->textInput(['id' => 'numero_convenio', 'disabled' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'validade_convenio')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '99/99/9999',
                    'options' => ['disabled' => true]
                ]) ?>
            </div>


        </div>

        <br>

        <div v-show="calendarLoaded" id="fieldsetCalendar">

            <div class="form-group">
                <?= $form->field($model, 'horario')->textInput(['id' => 'horario', 'readonly' => 'readonly']); ?>
            </div>

            <div id="calendar"></div>

            <br>
            <div class="form-group">
                <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i> Atualizar</button>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>