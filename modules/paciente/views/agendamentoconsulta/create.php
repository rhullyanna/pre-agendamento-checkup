<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\paciente\models\AgendamentoConsulta */

$this->title = 'Pré-Agendamento de Consulta';
$this->params['breadcrumbs'][] = ['label' => 'Pré-Agendamentos de Consulta', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agendamento-consulta-create">

    <div class="alert alert-info text-center" role="alert">
        Este cadastro é um pré-agendamento, nossa equipe entrará em contato para confirmação em até 24h.
    </div>

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>