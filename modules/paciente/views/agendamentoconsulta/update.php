<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\paciente\models\AgendamentoConsulta */

$this->title = 'Atualizar Pré-Agendamento de Consulta #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pré-Agendamentos - Consulta', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agendamento-consulta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>