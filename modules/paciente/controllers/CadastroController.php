<?php

namespace app\modules\paciente\controllers;

use Yii;
use app\modules\paciente\models\Paciente;
use app\modules\paciente\models\PacienteSearch;
use app\models\PermissaoEnum;
use app\models\Usuario;
use app\modules\paciente\models\PacienteDocumento;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class CadastroController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view', 'atualizar', 'findmodel', 'alterarsenha', 'validatepostdata'],
                        'roles' => [PermissaoEnum::PACIENTE],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['novo', 'esquecisenha', 'validatepostdata'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Paciente models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PacienteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Paciente model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Paciente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNovo()
    {
        $model = new Paciente();
        $usuario = new Usuario();
        $model->setScenario(Paciente::SCENARIO_DEFAULT);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $trans = Yii::$app->db->beginTransaction();

            try {
                $usuario->login = $model->cpf;
                $usuario->permissao = PermissaoEnum::PACIENTE;
                $usuario->save(false);
                $model->usuario_id = $usuario->id;
                $model->save(false);
                $model->sendInfoInsertCheckup();

                Yii::$app->session->setFlash('success', "Usuário cadastrado com sucesso. Efetue login para realizar seu agendamento.");
                $trans->commit();
                return $this->redirect(['/paciente/default/login']);
            } catch (\Exception $e) {
                $trans->rollBack();
                throw $e;
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Paciente model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAtualizar($id)
    {
        $model = $this->findModel($id);
        $model->setScenario(Paciente::SCENARIO_UPDATE);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            $model->sendInfoInsertCheckup();
            Yii::$app->session->setFlash('success', "Dados atualizados com sucesso.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Paciente model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Paciente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Paciente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Paciente::findOne((int) $id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionEsquecisenha()
    {

        $model = new Usuario();
        $model->setScenario(Usuario::SCENARIO_FORGOT_PASSWORD);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $usuario = Usuario::find()->where(['login' => $model->login, 'permissao' => PermissaoEnum::PACIENTE])->one();
            if (!$usuario) {
                Yii::$app->session->setFlash('error', 'Usuário não encontrado!');
                $this->refresh();
                return;
            }

            $senha = $usuario->generatePass();
            $usuario->setScenario(Usuario::SCENARIO_FORGOT_PASSWORD);
            $trans = Yii::$app->db->beginTransaction();

            try {
                if ($usuario->validate()) {
                    $usuario->save();
                    /*&& $usuario->enviarSenhaEmail($usuario->paciente->email, $senha)*/
                    $trans->commit();
                    Yii::$app->session->setFlash('success', 'Senha enviada para o email cadastrado!');
                } else {
                    print_r($usuario->errors);
                    die;
                    $trans->rollBack();
                    Yii::$app->session->setFlash('error', 'Atenção! Ocorreu um problema durante a requisição, tente novamente mais tarde!');
                }
            } catch (\Exception $e) {
                $trans->rollBack();
                throw $e;
            }

            return $this->redirect(['default/login']);
        } else {
            return $this->render('forgot_password', [
                'model' => $model
            ]);
        }
    }

    public function actionAlterarsenha()
    {

        $model = Usuario::findOne(Yii::$app->user->id);
        $model->setScenario(Usuario::SCENARIO_UPDATE_PASSWORD);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('success', 'Senha alterada com sucesso!');
            return $this->redirect(['default/index']);
        } else {
            return $this->render('update_password', [
                'model' => $model,
            ]);
        }
    }

    public function actionValidatepostdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = ['success' => 0, 'errors' => null];
        $model = new Paciente();

        if (Yii::$app->request->post()['Paciente']['id']) {
            $model = $this->findModel(Yii::$app->request->post()['Paciente']['id']);
            $model->setScenario(Paciente::SCENARIO_UPDATE);
        }

        $model->load(Yii::$app->request->post());

        if ($model->validate()) {
            $response['success'] = 1;
        } else {
            foreach ($model->getErrors() as $value) {
                $response['errors'][] = $value;
            }
        }
        return $response;
    }
}
