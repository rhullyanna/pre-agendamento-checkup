<?php

namespace app\modules\paciente\controllers;

use app\models\LogAcaoEnum;
use Yii;

use app\modules\paciente\models\Paciente;
use app\modules\paciente\models\AgendamentoExame;
use app\modules\paciente\models\AgendamentoExameSearch;
use app\models\PermissaoEnum;
use app\models\StatusEnum;
use app\models\Usuario;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\MethodNotAllowedHttpException;

/**
 * AgendamentoExameController implements the CRUD actions for AgendamentoExame model.
 */
class AgendamentoexameController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'novo', 'loadexamtypedata', 'loadexamdata', 'loaddatescalendar', 'loaduserdata', 'validatepostdata', 'loaduserinsurancedata', 'loadinsurancedata', 'view', 'atualizar', 'findmodel', 'index', 'cancelar'
                        ],
                        'roles' => [PermissaoEnum::PACIENTE],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all AgendamentoExame models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgendamentoExameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AgendamentoExame model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AgendamentoExame model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNovo()
    {
        $model = new AgendamentoExame();
        $model->setScenario(AgendamentoExame::SCENARIO_DEFAULT);
        $model->loadFields();

        $trans = Yii::$app->db->beginTransaction();
        try {
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $model->save(false);
                Yii::$app->session->setFlash('success', "Pré-agendamento efetuado com sucesso.");
                $trans->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } catch (\Exception $e) {
            $trans->rollBack();
            $msg = $e->getMessage();
            if (strpos($e->getMessage(), 'ORA-20011') !== false) {
                $msg = "Ops! Provavelmente alguém conseguiu agendar antes de você, tente outro horáŕio, por favor";
            }
            $model->rollBackCheckupInfo();
            Yii::$app->session->setFlash('error', $msg);
            return $this->redirect(['novo']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AgendamentoExame model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAtualizar($id)
    {
        $model = $this->findModel($id);

        if ($model->isCancelado()) {
            throw new MethodNotAllowedHttpException('Pré-agendamento cancelado não pode ser alterado.');
        }

        $model->setScenario(AgendamentoExame::SCENARIO_UPDATE);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Pré-agendamento atualizado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AgendamentoExame model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionCancelar($id)
    {
        $patient = Paciente::find(['id' => Yii::$app->user->identity->id])->one();
        $action =  'index';
        $message = "Pré-agendamento cancelado!";

        $trans = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findModel($id);
            $model->setScenario(AgendamentoExame::SCENARIO_CANCEL);
            $model->getLog()->acao = LogAcaoEnum::CANCEL;
            $model->status = StatusEnum::CANCELADO;
            $model->save(false);
            $model->cancelCheckup();

            if ($patient->revocate()) {
                $patient->deactivate();
                $message =  "Você efetuou 2 cancelamentos neste mês, seu acesso foi desativado. Entre em contato com a administração.";
                Yii::$app->user->logout();
                $action = 'default/login';
            }

            Yii::$app->session->setFlash('error', $message);
            $trans->commit();
            return $this->redirect([$action]);

            Yii::$app->session->setFlash('success', "Pré-agendamento cancelado!");
            $trans->commit();
            return $this->redirect(['index']);
        } catch (\Exception $e) {
            $trans->rollBack();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Finds the AgendamentoExame model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AgendamentoExame the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AgendamentoExame::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionValidatepostdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = ['success' => 0, 'errors' => null, 'id' => null];
        $model = new AgendamentoExame();
        $model->setScenario(AgendamentoExame::SCENARIO_DEFAULT);
        $model->loadFields();

        try {
            $model->load(Yii::$app->request->post());

            if ($model->validate()) {
                $model->sendInfoCheckup();
                $model->save(false);
                $response['success'] = 1;
                $response['id'] = $model->id;
                Yii::$app->session->setFlash('success', "Pré-agendamento efetuado com sucesso.");
            } else {
                $response['errors'] =  $model->errors;
            }
        } catch (\Exception $e) {
            $response['success'] = 0;
            $model->rollBackCheckupInfo();
            $msg = $e->getMessage();
            $inicio = "ORA-";
            $fim = "#";
            if (preg_match("/{$inicio}([^#]+){$fim}/", $msg, $match) == 1) {
                $msg = $match[1];
            }
            if ($msg == '') {
                $msg = "Não foi possível marcar neste horário, por favor, selecione outro.";
            }
            $response['errors'] = ['msg' => [$msg]];
        }
        return $response;
    }
}
