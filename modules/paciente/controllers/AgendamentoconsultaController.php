<?php

namespace app\modules\paciente\controllers;

use app\models\LogAcaoEnum;
use app\modules\paciente\models\Paciente;
use app\modules\paciente\models\AgendamentoConsulta;
use app\modules\paciente\models\AgendamentoConsultaSearch;
use app\models\PermissaoEnum;
use app\models\StatusEnum;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\MethodNotAllowedHttpException;
use Yii;

/**
 * AgendamentoConsultaController implements the CRUD actions for AgendamentoConsulta model.
 */
class AgendamentoconsultaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'novo', 'view', 'atualizar', 'findmodel', 'index', 'validatepostdata', 'cancelar'
                        ],
                        'roles' => [PermissaoEnum::PACIENTE],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all AgendamentoConsulta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgendamentoConsultaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AgendamentoConsulta model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AgendamentoConsulta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNovo()
    {
        $model = new AgendamentoConsulta();
        $model->setScenario(AgendamentoConsulta::SCENARIO_DEFAULT);
        $paciente = Paciente::findOne(Yii::$app->user->identity->roleId);
        $model->cd_pessoa_fisica = $paciente->cd_pessoa_fisica;
        $model->paciente_id = Yii::$app->user->identity->roleId;

        $trans = Yii::$app->db->beginTransaction();
        try {
            if (Yii::$app->request->post()) {
                $model->load(Yii::$app->request->post());
                $model->save(false);
                Yii::$app->session->setFlash('success', "Pré-agendamento efetuado com sucesso.");
                $trans->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } catch (\Exception $e) {
            $trans->rollBack();
            $msg = $e->getMessage();
            if (strpos($e->getMessage(), 'ORA-20011') !== false) {
                $msg = "Ops! Provavelmente alguém conseguiu agendar antes de você, tente outro horáŕio, por favor";
            }
            $model->rollBackCheckupInfo();
            Yii::$app->session->setFlash('error', $msg);
            return $this->redirect(['novo']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AgendamentoConsulta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAtualizar($id)
    {
        $model = $this->findModel($id);
        if ($model->isCancelado()) {
            throw new MethodNotAllowedHttpException('Pré-agendamento cancelado não pode ser alterado.');
        }
        $model->setScenario(AgendamentoConsulta::SCENARIO_UPDATE);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Pré-agendamento atualizado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AgendamentoConsulta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCancelar($id)
    {
        $patient = Paciente::find(['id' => Yii::$app->user->identity->id])->one();
        $action =  'index';
        $message = "Pré-agendamento cancelado!";

        $trans = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findModel($id);
            $model->setScenario(AgendamentoConsulta::SCENARIO_CANCEL);
            $model->getLog()->acao = LogAcaoEnum::CANCEL;
            $model->status = StatusEnum::CANCELADO;
            $model->save(false);
            $model->cancelCheckup();

            if ($patient->revocate()) {
                $patient->deactivate();
                $message =  "Você efetuou 2 cancelamentos neste mês, seu acesso foi desativado. Entre em contato com a administração.";
                Yii::$app->user->logout();
                $action = 'default/login';
            }

            Yii::$app->session->setFlash('error', $message);
            $trans->commit();
            return $this->redirect([$action]);
        } catch (\Exception $e) {
            $trans->rollBack();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Finds the AgendamentoConsulta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AgendamentoConsulta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AgendamentoConsulta::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionValidatepostdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = ['success' => 0, 'errors' => null, 'id' => null];
        $model = new AgendamentoConsulta();
        $model->setScenario(AgendamentoConsulta::SCENARIO_DEFAULT);
        $model->loadFields();

        try {
            $model->load(Yii::$app->request->post());

            if ($model->validate()) {
                $model->sendInfoCheckup();
                $model->save(false);
                $response['success'] = 1;
                $response['id'] = $model->id;
                Yii::$app->session->setFlash('success', "Pré-agendamento efetuado com sucesso.");
            } else {
                $response['errors'] =  $model->errors;
            }
        } catch (\Exception $e) {
            $response['success'] = 0;
            $model->rollBackCheckupInfo();
            $msg = $e->getMessage();
            $inicio = "ORA-";
            $fim = "#";
            if (preg_match("/{$inicio}([^#]+){$fim}/", $msg, $match) == 1) {
                $msg = $match[1];
            }
            if ($msg == '') {
                $msg = "Não foi possível marcar neste horário, por favor, selecione outro.";
            }
            $response['errors'] = ['msg' => [$msg]];
        }
        return $response;
    }
}
