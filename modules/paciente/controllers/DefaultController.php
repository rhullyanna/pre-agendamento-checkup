<?php

namespace app\modules\paciente\controllers;

use app\models\Log;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\PermissaoEnum;
use app\models\Usuario;

/**
 * Default controller for the `paciente` module
 */
class DefaultController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model->role = PermissaoEnum::PACIENTE;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $model->loginLog();
            $user = Usuario::findOne(Yii::$app->user->identity->id);
            if ($user->isFirstLogin()) {
                return $this->redirect(['cadastro/alterarsenha']);
            }
            return $this->goHome();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionPartiallogin()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = ['success' => 0, 'errors' => null];

        $model = new LoginForm();
        $model->role = PermissaoEnum::PACIENTE;
        $model->load(Yii::$app->request->post());

        if ($model->validate()) {
            $model->login();
            $response['success'] = 1;
        } else {
            foreach ($model->getErrors() as $value) {
                $response['errors'][] = $value;
            }
        }

        $model->password = '';
        return $response;
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
