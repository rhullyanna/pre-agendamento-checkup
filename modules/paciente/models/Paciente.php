<?php

namespace app\modules\paciente\models;

use app\components\LogActiveRecord;
use app\models\Log;
use app\models\LogAcaoEnum;
use app\models\SexoEnum;
use app\models\StatusEnum;
use app\models\TipoImagemEnum;
use app\models\Usuario;
use yiibr\brvalidator\CpfValidator;
use yii\web\UploadedFile;

use Yii;

/**
 * This is the model class for table "paciente".
 *
 * @property int $id
 * @property string $nome
 * @property string $cpf
 * @property string $rg
 * @property string $email
 * @property string $nome_mae
 * @property int $usuario_id
 * @property int $convenio_id
 * @property int $cd_pessoa_fisica
 *
 * @property Usuario $usuario
 */
class Paciente extends LogActiveRecord
{

    const SCENARIO_DEFAULT     = 'default';
    const SCENARIO_UPDATE      = 'update';
    const SCENARIO_EXAM      = 'exam';
    public $is_convenio;
    public $doc_identificacao;
    public $doc_convenio;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paciente';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['data_nascimento'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['data_nascimento'],
                ],
                'value' => function ($event) {
                    $date = \DateTime::createFromFormat('d/m/Y', $this->data_nascimento);
                    if (!$date) {
                        $date = \DateTime::createFromFormat('Y-m-d', $this->data_nascimento);
                    }
                    return $date->format('Y-m-d');
                },
            ],
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['validade_convenio'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['validade_convenio'],
                ],
                'value' => function ($event) {
                    if ($this->validade_convenio) {
                        $date = \DateTime::createFromFormat('d/m/Y', $this->validade_convenio);
                        if (!$date) {
                            $date = \DateTime::createFromFormat('Y-m-d', $this->validade_convenio);
                        }
                        return $date->format('Y-m-d');
                    }
                },
            ],
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_AFTER_FIND => ['data_nascimento'],
                ],
                'value' => function ($event) {
                    if ($this->data_nascimento) {
                        return date('d/m/Y', strtotime($this->data_nascimento));
                    }
                },
            ],
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_AFTER_FIND => ['validade_convenio'],
                ],
                'value' => function ($event) {
                    if ($this->validade_convenio) {
                        return date('d/m/Y', strtotime($this->validade_convenio));
                    }
                },
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEFAULT] =
            [
                'nome', 'rg', 'cpf', 'email', 'data_nascimento', 'sexo', 'telefone', 'nome_mae', 'doc_identificacao', 'cd_pessoa_fisica'
            ];
        $scenarios[self::SCENARIO_EXAM] =
            [
                'nome', 'rg', 'cpf', 'data_nascimento', 'sexo'
            ];
        $scenarios[self::SCENARIO_UPDATE] =
            [
                'nome', 'rg', 'cpf', 'email', 'data_nascimento', 'sexo', 'telefone', 'nome_mae', 'doc_identificacao', 'cd_pessoa_fisica'
            ];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'cpf', 'data_nascimento', 'rg', 'email', 'sexo', 'nome_mae', 'telefone', 'doc_identificacao'], 'required', 'on' => [self::SCENARIO_DEFAULT]],
            [['nome', 'cpf', 'data_nascimento', 'rg', 'email', 'sexo', 'nome_mae', 'telefone', 'doc_identificacao'], 'required', 'on' => [self::SCENARIO_UPDATE]],
            [['nome', 'cpf', 'data_nascimento', 'rg',  'sexo'], 'required', 'on' => [self::SCENARIO_EXAM]],
            [['nome', 'nome_mae'], 'trim'],
            [['nome', 'cpf'], 'unique', 'on' => [self::SCENARIO_DEFAULT]],
            [['nome', 'cpf'], 'myUniqueValidator', 'on' => [self::SCENARIO_UPDATE]],
            [['cpf'], CpfValidator::className()],
            [['data_nascimento'], 'string', 'max' => 10],
            [['data_nascimento'], 'date', 'format' => 'd/m/Y'],
            [['nome'], 'string', 'max' => 200],
            [['nome'], 'string', 'min' => 6],
            [['cpf'], 'string', 'max' => 14],
            [['rg'], 'string', 'max' => 20],
            [['email', 'senha'], 'string', 'max' => 100],
            [['nome_mae'], 'string', 'max' => 120],
            [['telefone'], 'string', 'max' => 15],
            [['sexo'], 'string', 'max' => 1],
            [['data_nascimento', 'doc_identificacao'], 'safe'],
            [['doc_identificacao'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome Completo',
            'cpf' => 'CPF',
            'rg' => 'RG',
            'email' => 'Email',
            'nome_mae' => 'Nome Completo da Mãe',
            'data_nascimento' => 'Data de Nascimento',
            'telefone' => 'Telefone',
            'sexo' => 'Sexo',
            'usuario_id' => 'Usuário',
            'convenio_id' => 'Convênio',
            'nome_convenio' => 'Convênio',
            'cd_pessoa_fisica' => 'Paciente ID Tasy',
            'doc_identificacao' => 'Documento de Identificação',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    public function getDocumentos()
    {
        return $this->hasMany(PacienteDocumento::className(), ['paciente_id' => 'id']);
    }


    public function myUniqueValidator($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (self::find()->where([$attribute => $this->$attribute])->andWhere(['<>', 'id', Yii::$app->user->identity->id])) {
                return false;
            }
        }
        return false;
    }


    public function afterFind()
    {

        foreach ($this->documentos as $key => $value) {
            if ($value->tipo == TipoImagemEnum::IDENTIFICACAO) {
                $this->doc_identificacao = $value->imagem;
            }
        }
        return parent::afterFind();
    }

    public function beforeValidate()
    {
        $this->cpf = preg_replace('/[^0-9]/', '', $this->cpf);
        $this->telefone = preg_replace('/[^0-9]/', '', $this->telefone);
        $this->rg = preg_replace('/[^0-9]/', '', $this->rg);
        return parent::beforeValidate();
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->setFiles();
        return parent::afterSave($insert, $changedAttributes);
    }

    public function setFiles()
    {
        $newDocId = PacienteDocumento::findOne(['paciente_id' => $this->id, 'tipo' => TipoImagemEnum::IDENTIFICACAO]);

        if (!$newDocId) {
            $documento = new PacienteDocumento();
            $documento->imagem = $this->doc_identificacao;
            $documento->tipo = TipoImagemEnum::IDENTIFICACAO;
            $documento->paciente_id = $this->id;
            $documento->save(false);
        } else {
            $newDocId->imagem = $this->doc_identificacao;
            $newDocId->update(false);
        }
    }

    public function getSexText()
    {
        $options = SexoEnum::list();
        return $options[$this->sexo];
    }

    public function isAtivo()
    {
        return $this->usuario->status == StatusEnum::ATIVO;
    }

    public function deactivate()
    {
        $this->usuario->status = StatusEnum::INATIVO;
        $this->usuario->save(false);
    }

    public function activate()
    {
        $this->usuario->status = StatusEnum::ATIVO;
        $this->usuario->save(false);
    }

    public function revocate()
    {
        $sql = "SELECT * FROM `log` WHERE 
                usuario_id = " . Yii::$app->user->identity->id . "
                and acao = " . LogAcaoEnum::CANCEL . "
                and month(data) = month(current_date())";
        $logCount = count(Log::findBySql($sql)->all());
        return $logCount % 2 == 0;
    }

    public function sendInfoInsertCheckup()
    {
        $pOut1 = $pOut2 = $pOut3 = '';

        $procedure = " 
            BEGIN 
                TASY.WS_REGISTRAR_PESSOA_FISICA (	
                    :CD_PESSOA_FISICA_EXT_P,
                    :NM_PESSOA_FISICA_P,
                    :DT_NASCIMENTO_P,
                    :NR_CPF_P,
                    :NR_IDENTIDADE_P,
                    :IE_SEXO_P,
                    :NR_DDD_CELULAR_P,
                    :NR_TELEFONE_CELULAR_P,
                    :NR_SEQ_COR_PELE_P,
                    :CD_NACIONALIDADE_P,
                    :NR_CARTAO_NAC_SUS_P,
                    :NM_USUARIO_P,
                    :CD_ERRO_P,
                    :DS_ERRO_P,
                    :CD_PESSOA_FISICA_P
                );
            END;";


        \Yii::$app->db2->createCommand($procedure)
            //P1
            ->bindValue(':CD_PESSOA_FISICA_EXT_P', '')
            ->bindValue(':NM_PESSOA_FISICA_P', $this->nome)
            ->bindValue(':DT_NASCIMENTO_P', date('d/m/Y', strtotime($this->data_nascimento)))
            ->bindValue(':NR_CPF_P', $this->cpf)
            ->bindValue(':NR_IDENTIDADE_P', $this->rg)
            ->bindValue(':IE_SEXO_P', $this->sexo)
            ->bindValue(':NR_DDD_CELULAR_P', substr($this->telefone, 0, 2))
            ->bindValue(':NR_TELEFONE_CELULAR_P', substr($this->telefone, 3))
            ->bindValue(':NR_SEQ_COR_PELE_P', '')
            ->bindValue(':CD_NACIONALIDADE_P', '')
            ->bindValue(':NR_CARTAO_NAC_SUS_P', 2)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindParam(':CD_ERRO_P', $pOut1, \PDO::PARAM_STR, 600)
            ->bindParam(':DS_ERRO_P', $pOut2, \PDO::PARAM_STR, 600)
            ->bindParam(':CD_PESSOA_FISICA_P', $pOut3, \PDO::PARAM_INT, 10)
            ->execute();

        if ($pOut2 != 0) {
            throw new \Exception("Código erro: {$pOut1} - {$pOut2}", 1);
        } else {
            $this->cd_pessoa_fisica = $pOut3;
            $this->setCdPessoaFisica();
        }
    }

    public function setCdPessoaFisica()
    {
        if ($this->cd_pessoa_fisica == null) {
            $sql = "SELECT CD_PESSOA_FISICA FROM TASY.PESSOA_FISICA WHERE NR_CPF = '{$this->cpf}'";
            $command = \Yii::$app->db2->createCommand($sql);
            $response = $command->queryOne();
            $this->cd_pessoa_fisica = $response['CD_PESSOA_FISICA'];
        }
        $this->update(false);
    }
}
