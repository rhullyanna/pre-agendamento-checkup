<?php

namespace app\modules\paciente\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\paciente\models\AgendamentoConsulta;

/**
 * AgendamentoConsultaSearch represents the model behind the search form of `app\modules\paciente\models\AgendamentoConsulta`.
 */
class AgendamentoConsultaSearch extends AgendamentoConsulta
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'convenio_id', 'especialidade_id'], 'integer'],
            [['data_cadastro'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgendamentoConsulta::find()->orderBy([
            'id' => SORT_DESC,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        $this->load($params);


        $data_cadastro = '';
        if ($this->data_cadastro) {
            $data = \DateTime::createFromFormat('d/m/Y', $this->data_cadastro);
            $data_cadastro = $data->format('Y-m-d');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->status,
            'date(data_cadastro)' => $data_cadastro,
            'especialidade_id' => $this->especialidade_id,
        ]);

        return $dataProvider;
    }
}
