<?php

namespace app\modules\paciente\models;

use app\components\LogActiveRecord;
use app\models\StatusEnum;
use app\models\TipoImagemEnum;
use Yii;


/**
 * This is the model class for table "agendamento_exame".
 *
 * @property int $id
 * @property int $forma
 * @property int $status
 * @property string $data_cadastro
 * @property int $paciente_id
 * @property int $tipo_id
 * @property int $nr_seq_proc_interno
 * @property int $convenio_id
 * @property int $solicitante_id
 */
class AgendamentoExame extends LogActiveRecord
{
    public $is_convenio;
    public $termo_aceite_1;
    public $termo_aceite_2;
    public $doc_identificacao;
    public $doc_convenio;
    public $doc_pedido_medico;
    public $nr_carteirinha;

    const SCENARIO_DEFAULT     = 'default';
    const SCENARIO_UPDATE      = 'update';
    const SCENARIO_CANCEL      = 'cancel';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agendamento_exame';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['validade_convenio'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['validade_convenio'],
                ],
                'value' => function ($event) {
                    if ($this->validade_convenio) {
                        $date = \DateTime::createFromFormat('d/m/Y', $this->validade_convenio);
                        if (!$date) {
                            $date = \DateTime::createFromFormat('Y-m-d', $this->validade_convenio);
                        }
                        return $date->format('Y-m-d');
                    }
                },
            ],
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['horario'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['horario'],
                ],
                'value' => function ($event) {
                    if ($this->horario) {
                        $date = \DateTime::createFromFormat('d/m/Y H:i:s', $this->horario);
                        if (!$date) {
                            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $this->horario);
                        }
                        return $date->format('Y-m-d H:i:s');
                    }
                },
            ],

            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_AFTER_FIND => ['validade_convenio'],
                ],
                'value' => function ($event) {
                    if ($this->validade_convenio) {
                        return date('d/m/Y', strtotime($this->validade_convenio));
                    }
                },
            ],
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_AFTER_FIND => ['horario'],
                ],
                'value' => function ($event) {
                    if ($this->horario) {
                        return date('d/m/Y H:i:s', strtotime($this->horario));
                    }
                },
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEFAULT] =
            [
                'cd_procedimento', 'ie_origem_proced', 'nr_seq_exame_lab', 'nome_medico', 'nr_seq_proc_interno', 'convenio_tasy_nome',
                'doc_pedido_medico', 'convenio_id', 'exame_tasy_descricao', 'numero_convenio', 'doc_identificacao', 'is_convenio',
                'doc_convenio', 'cd_agenda', 'validade_convenio', 'nome_mae_paciente', 'horario', 'termo_aceite_1', 'termo_aceite_2',
                'paciente', 'qt_duracao', 'cd_medico', 'nr_seq_agenda', 'cd_categoria', 'cd_plano', 'nr_carteirinha'
            ];
        $scenarios[self::SCENARIO_UPDATE] =
            [
                'horario'
            ];
        $scenarios[self::SCENARIO_CANCEL] = ['status'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[
                'paciente_id', 'nr_seq_proc_interno', 'doc_pedido_medico', 'is_convenio', 'horario'
            ], 'required', 'on' => [self::SCENARIO_DEFAULT]],
            [['horario'], 'required', 'on' => [self::SCENARIO_UPDATE]],
            [['status', 'paciente_id', 'nr_seq_proc_interno', 'convenio_id'], 'integer'],
            [[
                'data_cadastro', 'dt_nasc_paciente', 'is_convenio', 'horario', 'termo_aceite_1', 'exame_tasy_descricao',
                'convenio_tasy_nome', 'nome_medico', 'cd_procedimento', 'ie_origem_proced', 'nr_seq_exame_lab', 'cd_agenda',
                'qt_duracao', 'doc_convenio', 'cd_categoria', 'cd_plano'
            ], 'safe'],
            [['termo_aceite_1', 'termo_aceite_2'], 'required', 'requiredValue' => 1, 'message' => 'É necessário aceitar o {attribute}', 'on' => [self::SCENARIO_DEFAULT]],
            [['convenio_id', 'numero_convenio', 'validade_convenio', 'doc_convenio'], 'required', 'on' => [self::SCENARIO_DEFAULT], 'message' => '{attribute} é obrigatório caso utilize convênio', 'when' => function ($model) {
                return $model->is_convenio == 1;
            }],
            [['numero_convenio'], 'validarNumero', 'on' => [self::SCENARIO_DEFAULT]],
            [['validade_convenio'], 'validarValidade', 'on' => [self::SCENARIO_DEFAULT]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'data_cadastro' => 'Data de Cadastro',
            'paciente_id' => 'Paciente',
            'exame_tasy_descricao' => 'Exame',
            'nr_seq_proc_interno' => 'Exame',
            'cd_agenda' => 'Médico(a)',
            'nome_medico' => 'Nome do médico',
            'nome_exame' => 'Exame',
            'convenio_tasy_nome' => 'Nome do Convênio',
            'convenio_id' => 'Convênio',
            'cd_plano' => 'Plano',
            'cd_categoria' => 'Categoria',
            'is_convenio' => 'Tipo de Convênio',
            'doc_pedido_medico' => 'Pedido Médico',
            'doc_identificacao' => 'Documento de Identificação',
            'doc_convenio' => 'Carteirinha de Convênio',
            'numero_convenio' => 'Número do Convênio',
            'validade_convenio' => 'Validade do Convênio',
            'horario' => 'Horário Selecionado',
            'termo_aceite_1' => 'Entendo que este pré-agendamento não garante o atendimento',
            'termo_aceite_2' => 'Li, aceito e concordo'
        ];
    }

    public function getDocumentos()
    {
        return $this->hasMany(ExameDocumento::className(), ['exame_id' => 'id']);
    }

    public function getPaciente()
    {
        return $this->hasOne(Paciente::className(), ['id' => 'paciente_id']);
    }

    public function validarNumero()
    {
        if (empty($this->nr_carteirinha)) return true;
        if ($this->convenio_id && $this->convenio_id != 50) {
            if (strlen($this->numero_convenio) < $this->nr_carteirinha) {
                $this->addError($this->numero_convenio, "Número do convênio deve conter {$this->nr_carteirinha} dígitos");
                return false;
            }
        }
    }

    public function validarValidade()
    {
        if (empty($this->validade_convenio)) return true;
        $hoje = date('Y-m-d');
        $vc = \DateTime::createFromFormat('d/m/Y', $this->validade_convenio);
        $data = $vc->format('Y-m-d');
        if (strtotime($hoje) > strtotime($data)) {
            $this->addError($this->validade_convenio, "A data de validade de convênio expirou");
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if ($this->getScenario() == self::SCENARIO_DEFAULT) {
            $this->status = StatusEnum::ATIVO;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->setFiles();

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind()
    {
        foreach ($this->documentos as $key => $value) {
            if ($value->tipo == TipoImagemEnum::PEDIDO_MEDICO) {
                $this->doc_pedido_medico = $value->imagem;
            }
            if ($value->tipo == TipoImagemEnum::IDENTIFICACAO) {
                $this->doc_identificacao = $value->imagem;
            }
            if ($value->tipo == TipoImagemEnum::CONVENIO) {
                $this->doc_convenio = $value->imagem;
            }
        }
        return parent::afterFind();
    }


    public function setFiles()
    {
        $newDocPM = ExameDocumento::findOne(['exame_id' => $this->id, 'tipo' => TipoImagemEnum::PEDIDO_MEDICO]);

        if (!$newDocPM) {
            $documento = new ExameDocumento();
            $documento->imagem = $this->doc_pedido_medico;
            $documento->tipo = TipoImagemEnum::PEDIDO_MEDICO;
            $documento->exame_id = $this->id;
            $documento->save(false);
        } else {
            $newDocPM->imagem = $this->doc_pedido_medico;
            $newDocPM->update(false);
        }

        $newDocId = ExameDocumento::findOne(['exame_id' => $this->id, 'tipo' => TipoImagemEnum::IDENTIFICACAO]);
        if ($newDocId) {
            $newDocId->delete();
        }
        if ($this->doc_identificacao) {
            $documento = new ExameDocumento();
            $documento->imagem = $this->doc_identificacao;
            $documento->tipo = TipoImagemEnum::IDENTIFICACAO;
            $documento->exame_id = $this->id;
            $documento->save(false);
        }


        $newDocConv = ExameDocumento::findOne(['exame_id' => $this->id, 'tipo' => TipoImagemEnum::CONVENIO]);
        if ($newDocConv) {
            $newDocConv->delete();
        }

        if ($this->doc_convenio) {
            $documento = new ExameDocumento();
            $documento->imagem = $this->doc_convenio;
            $documento->tipo = TipoImagemEnum::CONVENIO;
            $documento->exame_id = $this->id;
            $documento->save(false);
        }
    }


    public function isCancelado()
    {
        return $this->status == StatusEnum::CANCELADO;
    }

    public function getStatusText()
    {
        $options = StatusEnum::list();
        return $options[$this->status];
    }

    public function loadFields()
    {
        $paciente = Paciente::findOne(Yii::$app->user->identity->roleId);
        $this->cd_pessoa_fisica = $paciente->cd_pessoa_fisica;
        $this->paciente_id = Yii::$app->user->identity->roleId;
    }

    public function loadFieldsForCheckup()
    {
        if (empty($this->convenio_id)) {
            $this->convenio_id = Yii::$app->params['checkupparticular'];
        }
        $this->setFormattedTime();
    }

    private function setFormattedTime()
    {
        $rh = \DateTime::createFromFormat('d/m/Y H:i:s', $this->horario);

        if (!$rh) {
            $horario = date('d/m/Y H:i:s', strtotime($this->horario));
        } else {
            $horario = $this->horario;
        }

        $this->horario = $horario;
    }

    public function sendInfoCheckup()
    {

        $this->loadFieldsForCheckup();
        // try {
        /******************************************************************** */
        $p1 = " 
            BEGIN
                TASY.GERAR_ENCAIXE_ATEND_AGENDA(
                    :DT_AGENDA_P, 
                    :QT_DURACAO_P, 
                    :CD_PESSOA_FISICA_P, 
                    :NM_PACIENTE_P,
                    :NR_ATENDIMENTO_P, 
                    :CD_MEDICO_P, 
                    :NR_SEQ_PROC_INTERNO_P, 
                    :CD_PROCEDIMENTO_P,
                    :IE_ORIGEM_PROCED_P, 
                    :IE_LADO_P, 
                    :CD_AGENDA_P,
                    :CD_ESTABELECIMENTO_P, 
                    :NM_USUARIO_P,
                    :DS_OBSERVACAO_P, 
                    :NM_PESSOA_CONTATO_P,
                    :NR_SEQ_AGENDA_P, 
                    :IE_CARATER_CIRURGIA_P,
                    :CD_PROCEDENCIA_P, 
                    :DS_ERRO_2_P, 
                    :NR_SEQ_CLASSIF_AGENDA_P
                );
            END;";
        $p1Out1 = $p1Out2 = '';
        \Yii::$app->db2->createCommand($p1)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':QT_DURACAO_P', (int) $this->qt_duracao)
            ->bindValue(':CD_PESSOA_FISICA_P', (int) $this->cd_pessoa_fisica)
            ->bindValue(':NM_PACIENTE_P', $this->paciente->nome)
            ->bindValue(':NR_ATENDIMENTO_P', 0)
            ->bindValue(':CD_MEDICO_P', (int) $this->cd_medico)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':CD_PROCEDIMENTO_P', (int) $this->cd_procedimento)
            ->bindValue(':IE_ORIGEM_PROCED_P', $this->ie_origem_proced)
            ->bindValue(':IE_LADO_P', '')
            ->bindValue(':CD_AGENDA_P', (int) $this->cd_agenda)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':DS_OBSERVACAO_P', '')
            ->bindValue(':NM_PESSOA_CONTATO_P', '')
            ->bindParam(':NR_SEQ_AGENDA_P', $p1Out1, \PDO::PARAM_INT, 10)
            ->bindValue(':IE_CARATER_CIRURGIA_P', '')
            ->bindValue(':CD_PROCEDENCIA_P', '')
            ->bindParam(':DS_ERRO_2_P', $p1Out2, \PDO::PARAM_STR, 1000)
            ->bindValue(':NR_SEQ_CLASSIF_AGENDA_P', 12)
            ->execute();


        if ($p1Out1 == '') {
            throw new \Exception("GERAR_ENCAIXE_ATEND_AGENDA: {$p1Out1}:{$p1Out2}");
        }

        $this->nr_seq_agenda = $p1Out1;

        /******************************************************************** */

        $partialSql = "";
        if ($this->convenio_id != Yii::$app->params['checkupparticular']) {
            $partialSql = "
                    CD_CATEGORIA = {$this->convenio_id},
                    CD_PLANO = {$this->cd_plano},
                    ";
        }

        $sql18 = " 
                DECLARE 
                
                vTelefone VARCHAR(20);
                vTelefoneDDD VARCHAR(5);
                
                BEGIN

                SELECT NR_TELEFONE_CELULAR, NR_DDD_CELULAR INTO vTelefone, vTelefoneDDD FROM TASY.PESSOA_FISICA WHERE CD_PESSOA_FISICA = :CD_PESSOA_FISICA_P;

                UPDATE TASY.AGENDA_PACIENTE SET 
                    NR_TELEFONE = 'M: (' || vTelefoneDDD || ')' || vTelefone, 
                    CD_CONVENIO = :CD_CONVENIO, 
                    {$partialSql}
                    CD_TIPO_ACOMODACAO = :CD_TIPO_ACOMODACAO,
                    CD_USUARIO_CONVENIO = :CD_USUARIO_CONVENIO,
                    DT_VALIDADE_CARTEIRA = :DT_VALIDADE_CARTEIRA,
                    IE_FORMA_AGENDAMENTO = :IE_FORMA_AGENDAMENTO,
                    IE_STATUS_AGENDA = :IE_STATUS_AGENDA
                WHERE CD_PESSOA_FISICA = :CD_PESSOA_FISICA_P 
                    AND (
                        HR_INICIO = TO_DATE(:HR_INICIO_P,'DD/MM/RRRR HH24:MI:SS') OR TO_CHAR(HR_INICIO, 'DD/MM/RRRR HH24:MI:SS') = :HR_INICIO_P
                    )
                    AND NR_SEQUENCIA = :NR_SEQUENCIA;

                END;";

        \Yii::$app->db2->createCommand($sql18)
            ->bindValue(':CD_CONVENIO', (int) $this->convenio_id)
            ->bindValue(':CD_TIPO_ACOMODACAO', 1)
            ->bindValue(':CD_USUARIO_CONVENIO', $this->cd_pessoa_fisica)
            ->bindValue(':DT_VALIDADE_CARTEIRA', $this->validade_convenio)
            ->bindValue(':IE_FORMA_AGENDAMENTO', Yii::$app->params['checkupformaagend'])
            ->bindValue(':IE_STATUS_AGENDA', 'PA')
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':HR_INICIO_P', $this->horario)
            ->bindValue(':NR_SEQUENCIA', $this->nr_seq_agenda)
            ->execute();

        /**************************** */

        $procedimento = $origem = '';
        $sql = "BEGIN
                TASY.OBTER_PROC_TAB_INTER_AGENDA	(
                    :NR_SEQ_INTERNO_P,
                    :NR_SEQ_AGENDA_P,
                    :CD_CONVENIO_P,
                    '',
                    '',
                    :CD_ESTABELECIMENTO_P,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    :CD_PROCEDIMENTO_P,
                    :IE_ORIGEM_PROCED_P
                );
                END;";

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':NR_SEQ_AGENDA_P', 0)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindParam(':CD_PROCEDIMENTO_P', $procedimento, \PDO::PARAM_STR, 20)
            ->bindParam(':IE_ORIGEM_PROCED_P', $origem, \PDO::PARAM_STR, 20)
            ->execute();

        if ($procedimento == '') {
            throw new \Exception("OBTER_PROC_TAB_INTER_AGENDA: Não foi possível encontrar o convênio para este exame");
        } else {
            $this->cd_procedimento = $procedimento;
            $this->ie_origem_proced = $origem;
        }

        /**************************** */

        $sql2 = " 
            BEGIN
                TASY.CONSISTIR_PAC_ATEND_GRUPO(
                    :CD_PESSOA_FISICA_P,
                    :DS_RETORNO_P,
                    :NM_USUARIO_P
                );
            END;
            ";
        $pOut2 = '';

        \Yii::$app->db2->createCommand($sql2)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindParam(':DS_RETORNO_P', $pOut2, \PDO::PARAM_STR, 600)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->execute();

        /******************************************************************** */

        $sql3 = " 
            BEGIN 
                TASY.CONSISTIR_QTD_CONV_REGRA(
                    :NR_SEQ_AGENDA_P, 
                    :CD_CONVENIO_P,
                    :DT_AGENDA_P,
                    :CD_AGENDA_P,
                    :CD_PESSOA_FISICA_P,
                    :CD_CATEGORIA_P,
                    :CD_PLANO_P,
                    :CD_ESTABELECIMENTO_P,
                    :NM_USUARIO_P, 
                    :CD_MEDICO_P,
                    :NR_SEQ_PROC_INTERNO_P,
                    :IE_TIPO_ATENDIMENTO_P,
                    :QT_AGENDAMENTO_P,
                    :QT_PERM_REGRA_P,
                    :DS_MENSAGEM_P
                );
            END;";

        $p2Out1 = $p2Out2 = $p2Out3 = '';

        \Yii::$app->db2->createCommand($sql3)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':CD_CATEGORIA_P', $this->cd_categoria)
            ->bindValue(':CD_PLANO_P', $this->cd_plano)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_MEDICO_P', $this->cd_medico)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':IE_TIPO_ATENDIMENTO_P', '')
            ->bindParam(':QT_AGENDAMENTO_P', $p2Out1, \PDO::PARAM_STR, 600)
            ->bindParam(':QT_PERM_REGRA_P', $p2Out2, \PDO::PARAM_STR, 600)
            ->bindParam(':DS_MENSAGEM_P', $p2Out3, \PDO::PARAM_STR, 600)
            ->execute();

        if ($p2Out3 != '') {
            throw new \Exception("CONSISTIR_QTD_CONV_REGRA: {$p2Out3}");
        }

        /******************************************************************** */

        $sql4 = " 
            BEGIN 
                TASY.CONSISTIR_PROC_INATIVO(:NR_SEQ_PROC_INTERNO_P, :NM_USUARIO_P, :NM_ESTABELECIMENTO_P);
            END;";

        \Yii::$app->db2->createCommand($sql4)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', $this->nr_seq_proc_interno)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':NM_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->execute();

        /**************************** */
        $sql5 = "
                BEGIN
                    TASY.CONSISTIR_PROC_CONV_AGENDA(
                        :CD_ESTABELECIMENTO_P,
                        :CD_PESSOA_FISICA_P,
                        :DT_REFERENCIA_P,
                        :CD_AGENDA_P,
                        :CD_CONVENIO_P,
                        :CD_CATEGORIA_P,
                        :CD_PROCEDIMENTO_P,
                        :IE_ORIGEM_PROCED_P,
                        :NR_SEQ_PROC_INTERNO_P,
                        :CD_MEDICO_P,
                        :IE_MEDICO_P,
                        :CD_PLANO_P,
                        :IE_CONSISTENCIA_P,
                        :IE_AGENDA_P,
                        :NR_SEQUENCIA_P,
                        :NR_SEQ_REGRA_P,
                        :IE_CONSIST_JS_P,
                        :NR_SEQ_COBERTURA_P,
                        :NR_MINUTO_DURACAO_P,
                        :CD_EMPRESA_REF_P,
                        :IE_ANESTESIA_P,
                        :IE_PROC_ADIC_P,
                        :NR_SEQ_ITEM_P
                    );
                END;";

        $p20Out1 = $p20Out2 = $p20Out3 = $p20Out4 = '';

        \Yii::$app->db2->createCommand($sql5)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':DT_REFERENCIA_P', date('d/m/Y', strtotime($this->horario)))
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':CD_CONVENIO_P', (int) $this->convenio_id)
            ->bindValue(':CD_CATEGORIA_P', $this->cd_categoria)
            ->bindValue(':CD_PROCEDIMENTO_P', $this->cd_procedimento)
            ->bindValue(':IE_ORIGEM_PROCED_P', $this->ie_origem_proced)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':CD_MEDICO_P', $this->cd_medico)
            ->bindValue(':IE_MEDICO_P', 'E')
            ->bindValue(':CD_PLANO_P', $this->cd_plano)
            ->bindParam(':IE_CONSISTENCIA_P', $p20Out1, \PDO::PARAM_STR, 900)
            ->bindParam(':IE_AGENDA_P', $p20Out2, \PDO::PARAM_STR, 600)
            ->bindValue(':NR_SEQUENCIA_P', (int) $this->nr_seq_agenda)
            ->bindParam(':NR_SEQ_REGRA_P', $p20Out3, \PDO::PARAM_STR, 600)
            ->bindParam(':IE_CONSIST_JS_P', $p20Out4, \PDO::PARAM_STR, 600)
            ->bindValue(':NR_SEQ_COBERTURA_P', '')
            ->bindValue(':NR_MINUTO_DURACAO_P', $this->qt_duracao)
            ->bindValue(':CD_EMPRESA_REF_P', 0)
            ->bindValue(':IE_ANESTESIA_P', 'N')
            ->bindValue(':IE_PROC_ADIC_P', null)
            ->bindValue(':NR_SEQ_ITEM_P', null)
            ->execute();


        if ($p20Out1 != '') {
            throw new \Exception("CONSISTIR_PROC_CONV_AGENDA 1: {$p20Out1}");
        }

        /**************************** */

        $sql5 = "
                BEGIN
                    TASY.CONSISTIR_PROC_CONV_AGENDA(
                        :CD_ESTABELECIMENTO_P,
                        :CD_PESSOA_FISICA_P,
                        :DT_REFERENCIA_P,
                        :CD_AGENDA_P,
                        :CD_CONVENIO_P,
                        :CD_CATEGORIA_P,
                        :CD_PROCEDIMENTO_P,
                        :IE_ORIGEM_PROCED_P,
                        :NR_SEQ_PROC_INTERNO_P,
                        :CD_MEDICO_P,
                        :IE_MEDICO_P,
                        :CD_PLANO_P,
                        :IE_CONSISTENCIA_P,
                        :IE_AGENDA_P,
                        :NR_SEQUENCIA_P,
                        :NR_SEQ_REGRA_P,
                        :IE_CONSIST_JS_P,
                        :NR_SEQ_COBERTURA_P,
                        :NR_MINUTO_DURACAO_P,
                        :CD_EMPRESA_REF_P,
                        :IE_ANESTESIA_P,
                        :IE_PROC_ADIC_P,
                        :NR_SEQ_ITEM_P
                    );
                END;";

        $p20Out1 = $p20Out2 = $p20Out3 = $p20Out4 = '';

        \Yii::$app->db2->createCommand($sql5)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':DT_REFERENCIA_P', date('d/m/Y', strtotime($this->horario)))
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':CD_CATEGORIA_P', $this->cd_categoria)
            ->bindValue(':CD_PROCEDIMENTO_P', $this->cd_procedimento)
            ->bindValue(':IE_ORIGEM_PROCED_P', $this->ie_origem_proced)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':CD_MEDICO_P', $this->cd_medico)
            ->bindValue(':IE_MEDICO_P', 'R')
            ->bindValue(':CD_PLANO_P', $this->cd_plano)
            ->bindParam(':IE_CONSISTENCIA_P', $p20Out1, \PDO::PARAM_STR, 900)
            ->bindParam(':IE_AGENDA_P', $p20Out2, \PDO::PARAM_STR, 600)
            ->bindValue(':NR_SEQUENCIA_P', (int) $this->nr_seq_agenda)
            ->bindParam(':NR_SEQ_REGRA_P', $p20Out3, \PDO::PARAM_STR, 600)
            ->bindParam(':IE_CONSIST_JS_P', $p20Out4, \PDO::PARAM_STR, 600)
            ->bindValue(':NR_SEQ_COBERTURA_P', '')
            ->bindValue(':NR_MINUTO_DURACAO_P', $this->qt_duracao)
            ->bindValue(':CD_EMPRESA_REF_P', 0)
            ->bindValue(':IE_ANESTESIA_P', 'N')
            ->bindValue(':IE_PROC_ADIC_P', null)
            ->bindValue(':NR_SEQ_ITEM_P', null)
            ->execute();


        if ($p20Out1 != '') {
            throw new \Exception("CONSISTIR_PROC_CONV_AGENDA 2: {$p20Out1}");
        }

        /***************************************************** */


        $sql6 = " 
            BEGIN 
                TASY.CONSISTIR_CAMPO_OBRIG_AGENDA(
                    :CD_AGENDA_P,
                    :QT_PESO,
                    :QT_ALTURA_CM_P,
                    :CD_MEDICO_REQ_P,
                    :IE_ANESTESIA_P,
                    :NR_SEQ_MOTIVO_ANEST_P,
                    :NR_SEQ_UNID_SOLIC_EXT_P,
                    :CD_ESTABELECIMENTO_P,
                    :NM_USUARIO_P,
                    :DS_ERRO_P
                );
            END;";
        $p15out1 = '';

        \Yii::$app->db2->createCommand($sql6)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':QT_PESO', 0)
            ->bindValue(':QT_ALTURA_CM_P', 0)
            ->bindValue(':CD_MEDICO_REQ_P', '')
            ->bindValue(':IE_ANESTESIA_P', '')
            ->bindValue(':NR_SEQ_MOTIVO_ANEST_P', 'N')
            ->bindValue(':NR_SEQ_UNID_SOLIC_EXT_P', 0)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindParam(':DS_ERRO_P', $p15out1, \PDO::PARAM_STR, 300)
            ->execute();

        if ($p15out1 != '') {
            throw new \Exception("CONSISTIR_CAMPO_OBRIG_AGENDA: {$p15out1}");
        }

        /**************************** */

        // $sql7 = " 
        //     BEGIN 
        //         TASY.CONSISTE_PLANO_CONVENIO(
        //             :NR_ATENDIMENTO_P, :CD_CONVENIO_P, :CD_PROCEDIMENTO_P, :IE_ORIGEM_PROCED_P,
        //             :DT_PROCEDIMENTO_P, :QT_PROCEDIMENTO_P, :IE_TIPO_ATENDIMENTO_P,
        //             :CD_PLANO_P, :CD_AUTORIZACAO_P, :DS_ERRO_P, :CD_SETOR_ATENDIMENTO_P, :NR_SEQ_EXAME_P, :IE_REGRA_P,
        //             :NR_SEQ_AGENDA_P, :NR_SEQ_REGRA_P, :NR_SEQ_PROC_INTERNO_P, :CD_CATEGORIA_P,
        //             :CD_ESTABELECIMENTO_P, :CD_SETOR_ENTREGA_PRESCR_P, :CD_MEDICO_P, :CD_PESSOA_FISICA_P,
        //             :IE_GLOSA_P, :NR_SEQ_REGRA_AJUSTE_P, :NR_PRESCRICAO_P, :NR_SEQ_PRESCRICAO_P, :CD_TIPO_ACOMODACAO_P,
        //             :CD_USUARIO_CONVENIO_P, :IE_CARATER_CIRURGIA_P
        //         ); 
        //     END;";

        // $p13Out1 = $p13Out2 = $p13Out3 = $p13Out13 = $p13Out5 = '';
        // \Yii::$app->db2->createCommand($sql7)
        //     //P13
        //     ->bindValue(':NR_ATENDIMENTO_P', 0)
        //     ->bindValue(':CD_CONVENIO_P', (int) $this->convenio_id)
        //     ->bindValue(':CD_PROCEDIMENTO_P', (int) $this->cd_procedimento)
        //     ->bindValue(':IE_ORIGEM_PROCED_P', $this->ie_origem_proced)
        //     ->bindValue(':DT_PROCEDIMENTO_P', $this->horario)
        //     ->bindValue(':QT_PROCEDIMENTO_P', 1)
        //     ->bindValue(':IE_TIPO_ATENDIMENTO_P', null)
        //     ->bindValue(':CD_PLANO_P', (int) $this->cd_plano)
        //     ->bindValue(':CD_AUTORIZACAO_P', '')
        //     ->bindParam(':DS_ERRO_P', $p13Out1, \PDO::PARAM_STR, 600)
        //     ->bindValue(':CD_SETOR_ATENDIMENTO_P', '')
        //     // ->bindValue(':NR_SEQ_EXAME_P', $this->nr_seq_exame_lab)
        //     ->bindValue(':NR_SEQ_EXAME_P', 0)
        //     ->bindParam(':IE_REGRA_P', $p13Out2, \PDO::PARAM_STR, 600)
        //     ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
        //     ->bindParam(':NR_SEQ_REGRA_P', $p13Out3, \PDO::PARAM_STR, 600)
        //     ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int) $this->nr_seq_proc_interno)
        //     ->bindValue(':CD_CATEGORIA_P', (int) $this->cd_categoria)
        //     ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
        //     ->bindValue(':CD_SETOR_ENTREGA_PRESCR_P', null)
        //     ->bindValue(':CD_MEDICO_P', $this->cd_medico)
        //     // ->bindValue(':CD_MEDICO_P', '')
        //     ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
        //     ->bindParam(':IE_GLOSA_P', $p13Out13, \PDO::PARAM_STR, 150)
        //     ->bindParam(':NR_SEQ_REGRA_AJUSTE_P', $p13Out5, \PDO::PARAM_STR, 600)
        //     ->bindValue(':NR_PRESCRICAO_P',  '')
        //     ->bindValue(':NR_SEQ_PRESCRICAO_P',  '')
        //     ->bindValue(':CD_TIPO_ACOMODACAO_P',  '')
        //     ->bindValue(':CD_USUARIO_CONVENIO_P',  '')
        //     ->bindValue(':IE_CARATER_CIRURGIA_P',  '')
        //     ->execute();

        // // erro na proc ageint_consiste_autor_proc ou consiste_autorizacao_proc
        // // regra = 6
        // if ($p13Out1 != '' || $p13Out1 != null) {
        //     throw new \Exception("CONSISTE_PLANO_CONVENIO: {$p13Out1}");
        // }


        /**************************** */


        $sql8 = " 
            BEGIN 
                TASY.REGRA_AGENDA_LIBER_CIDADE(:CD_PESSOA_FISICA_P, :CD_AGENDA_P, :DS_ERRO_P);
            END;";
        $p8Out1 = '';

        \Yii::$app->db2->createCommand($sql8)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindParam(':DS_ERRO_P', $p8Out1, \PDO::PARAM_STR, 600)
            ->execute();

        if ($p8Out1 != '') {
            throw new \Exception("REGRA_AGENDA_LIBER_CIDADE: {$p8Out1}");
        }

        /**************************** */

        $sql9 = "
                BEGIN
                    TASY.CONSISTIR_REGRA_AGENDA_GRUPO(
                        :CD_AGENDA_P,
                        :DT_AGENDA_P,
                        :NR_SEQ_PROC_INTERNO_P,
                        :NR_SEQ_AGENDA_P,
                        :NM_USUARIO_P,
                        :CD_ESTABELECIMENTO_P,
                        :NR_SEQ_ADICIONAL_P
                    );
                END;";

        \Yii::$app->db2->createCommand($sql9)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':NR_SEQ_ADICIONAL_P', 0)
            ->execute();

        /******************************************************************** */

        $sql10 = " 
            BEGIN 
                TASY.CONSISTIR_PESO_PROC_AGENDA(
                    :CD_PROCEDIMENTO_P, 
                    :IE_ORIGEM_PROCED_P, 
                    :QT_PESO_P,
                    :DS_ERRO_P
                );
            END;";
        $p19out1 = '';

        \Yii::$app->db2->createCommand($sql10)
            ->bindValue(':CD_PROCEDIMENTO_P', $this->cd_procedimento)
            ->bindValue(':IE_ORIGEM_PROCED_P', $this->ie_origem_proced)
            ->bindValue(':QT_PESO_P', 0)
            ->bindParam(':DS_ERRO_P', $p19out1, \PDO::PARAM_STR, 150)
            ->execute();

        if ($p19out1 != '') {
            throw new \Exception("CONSISTIR_PESO_PROC_AGENDA: {$p19out1}");
        }


        /**************************** */


        $sql11 = " 
            BEGIN 
                TASY.CONSISTIR_PESO_PACIENTE_EXAME(:CD_AGENDA_P, :QT_PESO_PACIENTE_P, :IE_PESO_P);
            END;";
        $p18out1 = '';

        \Yii::$app->db2->createCommand($sql11)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':QT_PESO_PACIENTE_P', 0)
            ->bindParam(':IE_PESO_P', $p18out1, \PDO::PARAM_STR, 150)
            ->execute();

        /**************************** */

        $sql12 = " 
            BEGIN 
                TASY.CONSISTIR_EQUIP_AGENDA_PARADO(:NR_SEQ_AGENDA_P, :NM_USUARIO_P, :CD_ESTABELECIMENTO_P);
            END;";

        \Yii::$app->db2->createCommand($sql12)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->execute();

        /**************************** */


        $sql13 = " 
            BEGIN 
                TASY.CONSISTIR_ALTURA_PAC_EXAME(:CD_AGENDA_P, :QT_ALTURA_PACIENTE_P, :IE_ALTURA_P);
            END;";
        $p14out1 = '';

        \Yii::$app->db2->createCommand($sql13)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':QT_ALTURA_PACIENTE_P', 0)
            ->bindParam(':IE_ALTURA_P', $p14out1, \PDO::PARAM_STR, 150)
            ->execute();

        /**************************** */


        $sql14 = " 
            BEGIN
                TASY.GERAR_AUTOR_REGRA(
                    :NR_ATENDIMENTO_P, 
                    :NR_SEQ_MATERIAL_P, 
                    :NR_SEQ_PROCED_P, 
                    :NR_PRESCRICAO_P,
                    :NR_SEQ_MAT_PRESCR_P, 
                    :NR_SEQ_PROC_PRESCR_P,
                    :IE_EVENTO_P, 
                    :NM_USUARIO_P,
                    :NR_SEQ_AGENDA_P, 
                    :NR_SEQ_PROC_INTERNO_P,
                    :NR_SEQ_GESTAO_P, 
                    :NR_SEQ_AGENDA_CONSULTA_P, 
                    :NR_SEQ_AGENDA_PAC_OPME_P,
                    :NR_SEQ_AGENDA_PROC_ADIC_P, 
                    :NR_SEQ_PROC_ORCAMENTO_P,
                    :NR_SEQ_MAT_ORCAMENTO_P, 
                    :NR_SEQ_PARAMETRO3_P
                );
            END;";

        \Yii::$app->db2->createCommand($sql14)
            ->bindValue(':NR_ATENDIMENTO_P', null)
            ->bindValue(':NR_SEQ_MATERIAL_P', null)
            ->bindValue(':NR_SEQ_PROCED_P', null)
            ->bindValue(':NR_PRESCRICAO_P', null)
            ->bindValue(':NR_SEQ_MAT_PRESCR_P', null)
            ->bindValue(':NR_SEQ_PROC_PRESCR_P', null)
            ->bindValue(':IE_EVENTO_P', 'AP')
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':NR_SEQ_GESTAO_P', null)
            ->bindValue(':NR_SEQ_AGENDA_CONSULTA_P', null)
            ->bindValue(':NR_SEQ_AGENDA_PAC_OPME_P', null)
            ->bindValue(':NR_SEQ_AGENDA_PROC_ADIC_P', null)
            ->bindValue(':NR_SEQ_PROC_ORCAMENTO_P', '')
            ->bindValue(':NR_SEQ_MAT_ORCAMENTO_P', '')
            ->bindValue(':NR_SEQ_PARAMETRO3_P', '')
            ->execute();

        /**************************** */


        $sql15 = " 
                BEGIN 
                    TASY.CONSISTIR_GERAR_AUTOR_AGRUP(:NR_SEQ_AGENDA_P, :IE_EVENTO_P, :NM_USUARIO_P, :DS_RETORNO_P);
                END;";
        $p16out1 = '';

        \Yii::$app->db2->createCommand($sql15)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':IE_EVENTO_P', 'AP')
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindParam(':DS_RETORNO_P', $p16out1, \PDO::PARAM_STR, 600)
            ->execute();

        if ($p16out1 != '') {
            throw new \Exception("CONSISTIR_GERAR_AUTOR_AGRUP: {$p16out1}");
        }

        $sql16 = " 
            BEGIN
                TASY.CALCULAR_MIN_DUR_AGENDA_PROC(
                    :CD_ESTABELECIMENTO_P, 
                    :CD_PERFIL_P, 
                    :NM_USUARIO_P, 
                    :NR_SEQ_AGENDA_P, 
                    :IE_AJUSTAR_MIN_P, 
                    :DS_AJUSTAR_MIN_P
                );
            END;
            ";
        $p12Out1 = $p12Out12 = '';

        \Yii::$app->db2->createCommand($sql16)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':CD_PERFIL_P', 1936)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindParam(':IE_AJUSTAR_MIN_P', $p12Out1, \PDO::PARAM_STR, 600)
            ->bindParam(':DS_AJUSTAR_MIN_P', $p12Out12, \PDO::PARAM_STR, 600)
            ->execute();

        /**************************** */

        $sql17 = " 
                BEGIN
                    TASY.ALTERAR_STATUS_AGENDA_REGRA(:CD_ESTABELECIMENTO_P, :IE_FONTE_P);
                END;";

        \Yii::$app->db2->createCommand($sql17)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':IE_FONTE_P', 'A')
            ->execute();


        /**************************** */
        $p2 = " 
            BEGIN
                TASY.GERAR_HORARIO_AGENDA_EXAME(
                    :CD_ESTABELECIMENTO_P,
                    :CD_AGENDA_P,
                    :DT_AGENDA_P,
                    :NM_USUARIO_P
                );
            END;";
        $commandP1 = \Yii::$app->db2->createCommand($p2)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal']);

        $commandP1->execute();

        /**************************** */
        // } catch (\Exception $e) {
        //     $this->rollBackCheckupInfo();
        //     throw new \Exception($e->getMessage(), 1);
        // }
    }

    public function cancelCheckup()
    {
        $this->setFormattedTime();

        \Yii::$app->db2->createCommand(
            "BEGIN 
                TASY.ALTERAR_STATUS_AGENDA(
                    :CD_AGENDA_P,
                    :NR_SEQ_AGENDA_P,
                    :IE_STATUS_P,
                    :CD_MOTIVO_P,
                    :DS_MOTIVO_P,
                    :IE_AGENDA_DIA_P,
                    :NM_USUARIO_P
                );
            END;"
        )
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_seq_agenda)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':IE_STATUS_P', 'C')
            ->bindValue(':CD_MOTIVO_P', 107)
            ->bindValue(':DS_MOTIVO_P', '')
            ->bindValue(':IE_AGENDA_DIA_P', 'N')
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->execute();


        /**************************** */
        $p2 = " 
            BEGIN
                TASY.GERAR_HORARIO_AGENDA_EXAME(
                    :CD_ESTABELECIMENTO_P,
                    :CD_AGENDA_P,
                    :DT_AGENDA_P,
                    :NM_USUARIO_P
                );
            END;";
        $commandP1 = \Yii::$app->db2->createCommand($p2)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal']);

        $commandP1->execute();
    }

    public function rollBackCheckupInfo()
    {
        $this->setFormattedTime();

        \Yii::$app->db2->createCommand(
            "BEGIN 
                TASY.LIBERAR_HORARIO_AGENDA_EXAME(:NR_SEQ_AGENDA_P, :NM_USUARIO_P);
            END;"
        )
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_seq_agenda)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->execute();

        \Yii::$app->db2->createCommand(
            "DELETE FROM TASY.AGENDA_PACIENTE WHERE CD_PESSOA_FISICA = :CD_PESSOA_FISICA_P 
                AND (
                    HR_INICIO = TO_DATE(:HR_INICIO_P,'DD/MM/RRRR HH24:MI:SS') OR TO_CHAR(HR_INICIO, 'DD/MM/RRRR HH24:MI:SS') = :HR_INICIO_P
                )
                AND NR_SEQUENCIA = :NR_SEQUENCIA
                "
        )
            ->bindValue(':CD_PESSOA_FISICA_P', (int) $this->cd_pessoa_fisica)
            ->bindValue(':HR_INICIO_P', $this->horario)
            ->bindValue(':NR_SEQUENCIA', (int) $this->nr_seq_agenda)
            ->execute();
    }
}
