<?php

namespace app\modules\paciente\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\paciente\models\AgendamentoExame;

/**
 * AgendamentoExameSearch represents the model behind the search form of `app\modules\paciente\models\AgendamentoExame`.
 */
class AgendamentoExameSearch extends AgendamentoExame
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['data_cadastro'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgendamentoExame::find()->orderBy([
            'id' => SORT_DESC,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        $this->load($params);

        // grid filtering conditions
        $data_cadastro = '';
        if ($this->data_cadastro) {
            $data = \DateTime::createFromFormat('d/m/Y', $this->data_cadastro);
            $data_cadastro = $data->format('Y-m-d');
        }
        $query->andFilterWhere([
            'status' => $this->status,
            'date(data_cadastro)' => $data_cadastro,
        ]);

        return $dataProvider;
    }
}
