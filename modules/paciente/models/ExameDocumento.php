<?php

namespace app\modules\paciente\models;

use app\models\TipoImagemEnum;
use Yii;

/**
 * This is the model class for table "paciente_documento".
 *
 * @property int $id
 * @property resource $imagem
 * @property int $descricao
 * @property int $paciente_id
 *
 * @property Paciente $paciente
 */
class ExameDocumento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exame_documento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['imagem', 'exame_id'], 'required'],
            [['imagem'], 'string'],
            [['paciente_id', 'tipo'], 'integer'],
            // [['paciente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paciente::className(), 'targetAttribute' => ['paciente_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imagem' => 'Imagem',
            'descricao' => 'Descrição',
            'paciente_id' => 'Paciente ID',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendamentoExame()
    {
        return $this->hasOne(AgendamentoExame::className(), ['id' => 'exame_id']);
    }

    public function getType()
    {
        return TipoImagemEnum::list()[$this->tipo];
    }
}
