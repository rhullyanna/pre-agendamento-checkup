<?php

namespace app\modules\paciente\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\paciente\models\Paciente;

/**
 * PacienteSearch represents the model behind the search form of `app\modules\paciente\models\Paciente`.
 */
class PacienteSearch extends Paciente
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'usuario_id', 'convenio_id', 'paciente_id'], 'integer'],
            [['nome', 'cpf', 'rg', 'email', 'nome_mae'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Paciente::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'usuario_id' => $this->usuario_id,
            'convenio_id' => $this->convenio_id,
            // 'paciente_id' => $this->paciente_id,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'cpf', $this->cpf])
            ->andFilterWhere(['like', 'rg', $this->rg])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'nome_mae', $this->nome_mae]);

        return $dataProvider;
    }
}
