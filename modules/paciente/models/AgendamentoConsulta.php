<?php

namespace app\modules\paciente\models;

use app\components\LogActiveRecord;
use app\models\StatusEnum;
use app\models\TipoImagemEnum;
use app\modules\admin\models\Medico;
use Yii;

/**
 * This is the model class for table "agendamento_consulta".
 *
 * @property int $id
 * @property int $forma
 * @property int $status
 * @property string $data_cadastro
 * @property int $paciente_id
 * @property int $tipo_id
 * @property int $convenio_id
 * @property int $especialidade_id
 * @property int $cd_medico
 * @property int $solicitante_id
 */
class AgendamentoConsulta extends LogActiveRecord
{

    public $is_convenio;
    public $termo_aceite_1;
    public $termo_aceite_2;
    public $doc_convenio;
    public $doc_identificacao;
    public $ie_classif_agenda;
    public $nr_carteirinha;


    const SCENARIO_DEFAULT     = 'default';
    const SCENARIO_UPDATE      = 'update';
    const SCENARIO_CANCEL      = 'cancel';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agendamento_consulta';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['validade_convenio'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['validade_convenio'],
                ],
                'value' => function ($event) {
                    if ($this->validade_convenio) {
                        $date = \DateTime::createFromFormat('d/m/Y', $this->validade_convenio);
                        if (!$date) {
                            $date = \DateTime::createFromFormat('Y-m-d', $this->validade_convenio);
                        }
                        return $date->format('Y-m-d');
                    }
                },
            ],
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['horario'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['horario'],
                ],
                'value' => function ($event) {
                    $date = \DateTime::createFromFormat('d/m/Y H:i:s', $this->horario);
                    if (!$date) {
                        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $this->horario);
                    }
                    return $date->format('Y-m-d H:i:s');
                },
            ],
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_AFTER_FIND => ['validade_convenio'],
                ],
                'value' => function ($event) {
                    if ($this->validade_convenio) {
                        return date('d/m/Y', strtotime($this->validade_convenio));
                    }
                },
            ],
            [
                'class'      => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_AFTER_FIND => ['horario'],
                ],
                'value' => function ($event) {
                    if ($this->horario) {
                        return date('d/m/Y H:i:s', strtotime($this->horario));
                    }
                },
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEFAULT] =
            [
                'especialidade_id', 'cd_medico', 'convenio_id', 'convenio_tasy_nome', 'exame_tasy_descricao', 'nome_medico', 'cd_agenda',
                'numero_convenio', 'validade_convenio', 'is_convenio', 'doc_convenio', 'especialidade_descricao', 'nr_carteirinha',
                'horario', 'termo_aceite_1', 'termo_aceite_2', 'nr_sequencia',  'qt_duracao', 'cd_medico', 'nr_seq_turno', 'ie_classif_agenda'
            ];
        $scenarios[self::SCENARIO_UPDATE] =
            [
                'horario'
            ];
        $scenarios[self::SCENARIO_CANCEL] = ['status'];

        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[
                'data_cadastro', 'paciente_id', 'especialidade_id', 'cd_medico', 'is_convenio', 'horario',
            ], 'required', 'on' => [self::SCENARIO_DEFAULT]],
            [['horario'], 'required', 'on' => [self::SCENARIO_UPDATE]],
            [['termo_aceite_1', 'termo_aceite_2'], 'required', 'requiredValue' => 1, 'message' => 'É necessário aceitar o {attribute}', 'on' => [self::SCENARIO_DEFAULT]],
            [['status', 'paciente_id', 'tipo_id', 'convenio_id', 'especialidade_id', 'cd_medico'], 'integer'],
            [['convenio_id', 'convenio_tasy_nome', 'numero_convenio', 'validade_convenio', 'doc_convenio'], 'required', 'message' => '{attribute} é obrigatório caso utilize convênio', 'when' => function ($model) {
                return $model->is_convenio == 1;
            }],
            [['numero_convenio'], 'validarNumero', 'on' => [self::SCENARIO_DEFAULT]],
            [['data_cadastro', 'is_convenio', 'validade_convenio', 'nr_sequencia', 'nr_seq_turno', 'especialidade_descricao'], 'safe'],
            [['validade_convenio'], 'date', 'format' => 'php:d/m/Y'],
            [['validade_convenio'], 'validarValidade', 'on' => [self::SCENARIO_DEFAULT]],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'data_cadastro' => 'Data deCadastro',
            'paciente_id' => 'Paciente',
            'tipo_id' => 'Tipo',
            'convenio_id' => 'Convênio',
            'convenio_tasy_nome' => 'Convênio',
            'is_convenio' => 'Tipo de Convênio',
            'especialidade_id' => 'Especialidade',
            'especialidade_descricao' => 'Especialidade',
            'cd_medico' => 'Médico',
            'cd_agenda' => 'Médico(a)',
            'doc_identificacao' => 'Documento de Identificação',
            'doc_convenio' => 'Carteirinha de Convênio',
            'numero_convenio' => 'Número do Convênio',
            'validade_convenio' => 'Validade do Convênio',
            'horario' => 'Horário Selecionado',
            'termo_aceite_1' => 'Termo 1',
            'termo_aceite_2' => 'Termo 2'
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedico()
    {
        return $this->hasOne(Medico::className(), ['id' => 'cd_medico']);
    }

    public function getDocumentos()
    {
        return $this->hasMany(ConsultaDocumento::className(), ['consulta_id' => 'id']);
    }

    public function getPaciente()
    {
        return $this->hasOne(Paciente::className(), ['id' => 'paciente_id']);
    }

    public function validarNumero()
    {
        if (empty($this->nr_carteirinha)) return true;
        if ($this->convenio_id && $this->convenio_id != 50) {
            if (strlen($this->numero_convenio) < $this->nr_carteirinha) {
                $this->addError($this->numero_convenio, "Número do convênio deve conter {$this->nr_carteirinha} dígitos");
                return false;
            }
        }
    }

    public function validarValidade()
    {
        if (empty($this->validade_convenio)) return true;
        $hoje = date('Y-m-d');
        $vc = \DateTime::createFromFormat('d/m/Y', $this->validade_convenio);
        $data = $vc->format('Y-m-d');
        if (strtotime($hoje) > strtotime($data)) {
            $this->addError($this->validade_convenio, "A data de validade de convênio expirou");
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if ($this->getScenario() == self::SCENARIO_DEFAULT) {
            $this->status = StatusEnum::ATIVO;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->setFiles();

        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind()
    {
        foreach ($this->documentos as $key => $value) {

            if ($value->tipo == TipoImagemEnum::IDENTIFICACAO) {
                $this->doc_identificacao = $value->imagem;
            }
            if ($value->tipo == TipoImagemEnum::CONVENIO) {
                $this->doc_convenio = $value->imagem;
            }
        }
        return parent::afterFind();
    }

    public function setFiles()
    {
        $newDocId = ConsultaDocumento::findOne(['consulta_id' => $this->id, 'tipo' => TipoImagemEnum::IDENTIFICACAO]);

        if ($newDocId) {
            $newDocId->delete();
        }

        if ($this->doc_identificacao) {
            $documento = new ConsultaDocumento();
            $documento->imagem = $this->doc_identificacao;
            $documento->tipo = TipoImagemEnum::IDENTIFICACAO;
            $documento->exame_id = $this->id;
            $documento->save(false);
        }

        $newDocConv = ConsultaDocumento::findOne(['consulta_id' => $this->id, 'tipo' => TipoImagemEnum::CONVENIO]);
        if ($newDocConv) {
            $newDocConv->delete();
        }

        if ($this->doc_convenio) {
            $documento = new ConsultaDocumento();
            $documento->imagem = $this->doc_convenio;
            $documento->tipo = TipoImagemEnum::CONVENIO;
            $documento->consulta_id = $this->id;
            $documento->save(false);
        }
    }

    public function isCancelado()
    {
        return $this->status == StatusEnum::CANCELADO;
    }

    public function getStatusText()
    {
        $options = StatusEnum::list();
        return $options[$this->status];
    }

    public function getTipoText()
    {
        $options = StatusEnum::list();
        return $options[$this->status];
    }

    public function getEspecialidadeText()
    {
        $options = StatusEnum::list();
        return $options[$this->status];
    }

    public function loadFields()
    {
        $paciente = Paciente::findOne(Yii::$app->user->identity->roleId);
        $this->cd_pessoa_fisica = $paciente->cd_pessoa_fisica;
        $this->paciente_id = Yii::$app->user->identity->roleId;
        if (empty($this->convenio_id)) {
            $this->convenio_id = Yii::$app->params['checkupparticular'];
        }
    }

    public function loadFieldsForCheckup()
    {
        if (empty($this->convenio_id)) {
            $this->convenio_id = Yii::$app->params['checkupparticular'];
        }
        $this->setFormattedTime();
    }

    private function setFormattedTime()
    {
        $rh = \DateTime::createFromFormat('d/m/Y H:i:s', $this->horario);

        if (!$rh) {
            $horario = date('d/m/Y H:i:s', strtotime($this->horario));
        } else {
            $horario = $this->horario;
        }

        $this->horario = $horario;
    }

    public function sendInfoCheckup()
    {

        $this->loadFieldsForCheckup();

        // try {

        $sql =
            "BEGIN
                TASY.GERAR_ENCAIXE_AGECONS	(
                    :CD_ESTABELECIMENTO_P,
                    :CD_AGENDA_P,
                    :DT_AGENDA_P,
                    :HR_ENCAIXE_P,
                    :QT_DURACAO_P,
                    :CD_PESSOA_FISICA_P,
                    :NM_PESSOA_FISICA_P,
                    :CD_CONVENIO_P,
                    :CD_MEDICO_REQ_P,
                    :DS_OBSERVACAO_P,
                    :NR_SEQ_AGENDA_RXT_P,
                    :NR_SEQ_MOTIVO_ENCAIXE_P,
                    :IE_CLASSIF_AGENDA_P,
                    :NM_USUARIO_P,
                    :NR_SEQ_AGECIR_P,
                    :NR_SEQ_ENCAIXE_P,
                    :CD_CATEGORIA_P,
                    :CD_PLANO_P
                );
            END;";

        $p1Out1 = '';
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':CD_AGENDA_P', (int) $this->cd_agenda)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':HR_ENCAIXE_P', $this->horario)
            ->bindValue(':QT_DURACAO_P', $this->qt_duracao)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':NM_PESSOA_FISICA_P', $this->paciente->nome)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':CD_MEDICO_REQ_P', '')
            ->bindValue(':DS_OBSERVACAO_P', '')
            ->bindValue(':NR_SEQ_AGENDA_RXT_P', '')
            ->bindValue(':NR_SEQ_MOTIVO_ENCAIXE_P', '')
            ->bindValue(':IE_CLASSIF_AGENDA_P', $this->ie_classif_agenda)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':NR_SEQ_AGECIR_P', 0)
            ->bindParam(':NR_SEQ_ENCAIXE_P', $p1Out1, \PDO::PARAM_INT, 150)
            ->bindValue(':CD_CATEGORIA_P', '')
            ->bindValue(':CD_PLANO_P', '')
            ->execute();

        if ($p1Out1 == '') {
            throw new \Exception("Não foi possível gerar consulta", 1);
        }

        $this->nr_sequencia = $p1Out1;

        /**************************** */

        $sql18 = " 
            DECLARE 
            
            vTelefone VARCHAR(20);
            vTelefoneDDD VARCHAR(5);
            
            BEGIN

            SELECT NR_TELEFONE_CELULAR, NR_DDD_CELULAR INTO vTelefone, vTelefoneDDD FROM TASY.PESSOA_FISICA WHERE CD_PESSOA_FISICA = :CD_PESSOA_FISICA_P;

            UPDATE TASY.AGENDA_CONSULTA SET 
                NR_TELEFONE = 'M: (' || vTelefoneDDD || ')' || vTelefone, 
                CD_CONVENIO = :CD_CONVENIO, 
                CD_CATEGORIA = :CD_CATEGORIA,
                CD_PLANO = :CD_PLANO,
                IE_CLASSIF_AGENDA = :IE_CLASSIF_AGENDA,
                CD_TIPO_ACOMODACAO = :CD_TIPO_ACOMODACAO,
                CD_USUARIO_CONVENIO = :CD_USUARIO_CONVENIO,
                DT_VALIDADE_CARTEIRA = :DT_VALIDADE_CARTEIRA,
                IE_FORMA_AGENDAMENTO = :IE_FORMA_AGENDAMENTO,
                IE_STATUS_AGENDA = :IE_STATUS_AGENDA
            WHERE CD_PESSOA_FISICA = :CD_PESSOA_FISICA_P 
                AND DT_AGENDA = :DT_AGENDA_P
                AND NR_SEQUENCIA = :NR_SEQUENCIA;

            END;";

        \Yii::$app->db2->createCommand($sql18)
            ->bindValue(':CD_CONVENIO', (int) $this->convenio_id)
            ->bindValue(':CD_PLANO', $this->cd_plano)
            ->bindValue(':CD_CATEGORIA', $this->cd_categoria)
            ->bindValue(':CD_TIPO_ACOMODACAO', $this->cd_acomodacao)
            ->bindValue(':IE_CLASSIF_AGENDA', 'P2')
            ->bindValue(':CD_USUARIO_CONVENIO', $this->cd_pessoa_fisica)
            ->bindValue(':DT_VALIDADE_CARTEIRA', $this->validade_convenio)
            ->bindValue(':IE_FORMA_AGENDAMENTO', Yii::$app->params['checkupformaagend'])
            ->bindValue(':IE_STATUS_AGENDA', 'PA')
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':NR_SEQUENCIA', $this->nr_sequencia)
            ->execute();

        /**************************** */


        $p4 = " 
            BEGIN
                TASY.CONSISTIR_PAC_ATEND_GRUPO(
                    :CD_PESSOA_FISICA_P,
                    :DS_RETORNO_P,
                    :NM_USUARIO_P
                );
            END;
            ";
        $p4Out1 = '';

        \Yii::$app->db2->createCommand($p4)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindParam(':DS_RETORNO_P', $p4Out1, \PDO::PARAM_STR, 600)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->execute();

        if ($p4Out1 != '') {
            throw new \Exception("CONSISTIR_PAC_ATEND_GRUPO: {$p4Out1}", 1);
        }


        /**************************** */

        $sql = "BEGIN
                TASY.CONSISTIR_CLASSIF_AGECON(
                    :CD_ESTABELECIMENTO_P,
                    :CD_PESSOA_FISICA_P,
                    :DT_REFERENCIA_P,
                    :CD_AGENDA_P,
                    :CD_CONVENIO_P,
                    :CD_PROCEDIMENTO_P,
                    :IE_ORIGEM_PROCED_P,
                    :NR_SEQ_PROC_INTERNO_P,
                    :IE_CLASSIF_AGENDA_P,
                    :NR_SEQUENCIA_P,
                    :IE_CONSISTENCIA_P,
                    :IE_AGENDA_P
                );
            END;";

        $p4Out1 = $p4Out2 = '';

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':DT_REFERENCIA_P', $this->horario)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':CD_PROCEDIMENTO_P', '')
            ->bindValue(':IE_ORIGEM_PROCED_P', '')
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', '')
            ->bindValue(':IE_CLASSIF_AGENDA_P', $this->ie_classif_agenda)
            ->bindValue(':NR_SEQUENCIA_P', $this->nr_sequencia)
            ->bindParam(':IE_CONSISTENCIA_P', $p4Out1, \PDO::PARAM_STR, 600)
            ->bindParam(':IE_AGENDA_P', $p4Out2, \PDO::PARAM_STR, 600)
            ->execute();

        /**************************** */

        $sql8 = " 
            BEGIN 
                TASY.REGRA_AGENDA_LIBER_CIDADE(:CD_PESSOA_FISICA_P, :CD_AGENDA_P, :DS_ERRO_P);
            END;";
        $p8Out1 = '';

        \Yii::$app->db2->createCommand($sql8)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindParam(':DS_ERRO_P', $p8Out1, \PDO::PARAM_STR, 600)
            ->execute();

        if ($p8Out1 != '') {
            throw new \Exception("REGRA_AGENDA_LIBER_CIDADE: {$p8Out1}", 1);
        }

        /**************************** */

        $p12 = " 
            BEGIN
                TASY.CONSISTIR_TURNO_ESP_CONVENIO(
                    :CD_AGENDA_P,
                    :DT_AGENDA_P,
                    :NR_SEQ_TURNO_ESP_P,
                    :CD_CONVENIO_P,
                    :NR_SEQ_AGENDA_P,
                    :DS_CONSISTENCIA_P,
                    :CD_CATEGORIA_P,
                    :IE_ACAO_P
                );
            END;
            ";
        $p12Out1 = '';

        \Yii::$app->db2->createCommand($p12)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':NR_SEQ_TURNO_ESP_P', $this->nr_seq_turno)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_sequencia)
            ->bindParam(':DS_CONSISTENCIA_P', $p12Out1, \PDO::PARAM_STR, 600)
            ->bindValue(':CD_CATEGORIA_P', 1)
            ->bindValue(':IE_ACAO_P', null)
            ->execute();

        if ($p12Out1 != '') {
            throw new \Exception("CONSISTIR_TURNO_ESP_CONVENIO: {$p12Out1}", 1);
        }

        /**************************** */

        $p11 = " 
            BEGIN
                TASY.CONSISTIR_TURNO_CONVENIO(
                    :NR_SEQ_AGENDA_P,
                    :CD_AGENDA_P,
                    :DT_AGENDA_P,
                    :NR_SEQ_TURNO_P,
                    :CD_CONVENIO_P,
                    :CD_CATEGORIA_P,
                    :DS_CONSISTENCIA_P,
                    :NM_USUARIO_P,
                    :CD_ESTABELECIMENTO_P,
                    :IE_ACAO_P
                );
            END;
            ";
        $p11Out1 = '';
        \Yii::$app->db2->createCommand($p11)
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_sequencia)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':NR_SEQ_TURNO_P', $this->nr_seq_turno)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':CD_CATEGORIA_P', 1)
            ->bindParam(':DS_CONSISTENCIA_P', $p11Out1, \PDO::PARAM_STR, 600)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':IE_ACAO_P', null)
            ->execute();

        if ($p11Out1 != '') {
            throw new \Exception("CONSISTIR_TURNO_CONVENIO: {$p11Out1}", 1);
        }

        /**************************** */

        $p9 = " 
            BEGIN
                TASY.CONSISTIR_REGRA_SETOR_AGENDA(
                    :CD_PESSOA_FISICA_P,
                    :CD_AGENDA_P, 
                    :DT_AGENDA_P, 
                    :CD_ESTABELECIMENTO_P, 
                    :NM_USUARIO_P,
                    :IE_CONSISTENCIA_P
                );
            END;
            ";
        $p9Out1 = '';

        \Yii::$app->db2->createCommand($p9)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindParam(':IE_CONSISTENCIA_P', $p9Out1, \PDO::PARAM_STR, 600)
            ->execute();

        if ($p9Out1 != '') {
            throw new \Exception("CONSISTIR_REGRA_SETOR_AGENDA: {$p9Out1}", 1);
        }

        /**************************** */

        $p6 = " 
            BEGIN
                TASY.CONSISTIR_QTD_CONV_REGRA_CONS(
                    :NR_SEQ_AGENDA_P,
                    :CD_CONVENIO_P,
                    :DT_AGENDA_P,
                    :CD_AGENDA_P,
                    :CD_PESSOA_FISICA_P,
                    :CD_CATEGORIA_P,
                    :CD_PLANO_P,
                    :CD_ESTABELECIMENTO_P,
                    :NM_USUARIO_P,
                    :CD_MEDICO_P,
                    :NR_SEQ_PROC_INTERNO_P,
                    :IE_TIPO_ATENDIMENTO_P,
                    :QT_AGENDAMENTO_P,
                    :QT_PERM_REGRA_P,
                    :DS_MENSAGEM_P
                );
            END;
            ";
        $p6Out1 = $p6Out2 = $p6Out3 = '';

        \Yii::$app->db2->createCommand($p6)
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_sequencia)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':CD_CATEGORIA_P', '')
            ->bindValue(':CD_PLANO_P', '')
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_MEDICO_P', $this->cd_medico)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', '')
            ->bindValue(':IE_TIPO_ATENDIMENTO_P', 0)
            ->bindParam(':QT_AGENDAMENTO_P', $p6Out1, \PDO::PARAM_STR, 600)
            ->bindParam(':QT_PERM_REGRA_P', $p6Out2, \PDO::PARAM_STR, 600)
            ->bindParam(':DS_MENSAGEM_P', $p6Out3, \PDO::PARAM_STR, 600)
            ->execute();

        if ($p6Out3 != '') {
            throw new \Exception("CONSISTIR_QTD_CONV_REGRA_CONS: {$p6Out3}", 1);
        }


        /**************************** */

        $p5 = " 
            BEGIN
                TASY.CONSISTIR_PRAZO_CONVENIO_AGEND(
                    :CD_PESSOA_FISICA_P,
                    :DT_AGENDA_P,
                    :CD_CONVENIO_P,
                    :NM_USUARIO_P,
                    :CD_ESTABELECIMENTO_P,
                    :DS_MENSAGEM_P
                );
            END;
            ";
        $p5Out1 = '';
        \Yii::$app->db2->createCommand($p5)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindParam(':DS_MENSAGEM_P', $p5Out1, \PDO::PARAM_STR, 600)
            ->execute();

        if ($p5Out1 != '') {
            throw new \Exception("CONSISTIR_PRAZO_CONVENIO_AGEND: {$p5Out1}", 1);
        }

        /**************************** */


        $p3 = " 
            BEGIN
                TASY.CONSISTIR_HORARIO_MED_AGENDA(
                    :NR_SEQ_AGENDA_P,
                    :CD_MEDICO_P,
                    :NR_MINUTO_DURACAO_P,
                    :CD_TIPO_AGENDA_P,
                    :DS_ERRO_P,
                    :DT_AGENDA_P
                );
            END;
            ";
        $p3Out1 = '';

        \Yii::$app->db2->createCommand($p3)
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_sequencia)
            ->bindValue(':CD_MEDICO_P', $this->cd_medico)
            ->bindValue(':NR_MINUTO_DURACAO_P', $this->qt_duracao)
            ->bindValue(':CD_TIPO_AGENDA_P', 3)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindParam(':DS_ERRO_P', $p3Out1, \PDO::PARAM_STR, 600)
            ->execute();


        if ($p3Out1 != '') {
            throw new \Exception("CONSISTIR_HORARIO_MED_AGENDA: {$p3Out1}", 1);
        }

        /**************************** */

        $p2 = " 
            BEGIN
                TASY.CONSISTIR_DURACAO_AGE_CONS(
                    :NR_MINUTO_DURACAO_P,
                    :DT_AGENDA_P, 
                    :CD_AGENDA_P, 
                    :NM_USUARIO_P, 
                    :CD_ESTABELECIMENTO_P, 
                    :NR_SEQ_AGENDA_P
                );
            END;
            ";
        \Yii::$app->db2->createCommand($p2)
            ->bindValue(':NR_MINUTO_DURACAO_P', (int) $this->qt_duracao)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_AGENDA_P', (int) $this->cd_agenda)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_sequencia)
            ->execute();

        /**************************** */

        $p1 = " 
            BEGIN
                TASY.CONSISTIR_CLASSIF_AGECONS(
                    :CD_PESSOA_FISICA_P, 
                    :CD_AGENDA_P, 
                    :IE_CLASSIF_AGENDA_P, 
                    :DT_AGENDAMENTO_P, 
                    :DS_ERRO_P
                );
            END;
            ";
        $p1Out1 = '';
        $commandP1 = \Yii::$app->db2->createCommand($p1)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':IE_CLASSIF_AGENDA_P', $this->ie_classif_agenda)
            ->bindValue(':DT_AGENDAMENTO_P', $this->horario)
            ->bindParam(':DS_ERRO_P', $p1Out1, \PDO::PARAM_STR, 600);
        $commandP1->execute();

        if ($p1Out1 != '') {
            throw new \Exception("CONSISTIR_CLASSIF_AGECONS 2: {$p1Out1}", 1);
        }

        /**************************** */


        $p18 = " 
            BEGIN
                TASY.CONSISTIR_AGENDA_CONV_ESPEC(                
                    :CD_AGENDA_P,
                    :CD_CONVENIO_P,
                    :NM_USUARIO_P,
                    :CD_ESTABELECIMENTO_P,
                    :DS_ERRO_P,
                    :IE_CONSISTENCIA_P
                );
            END;
            ";

        $p18Out1 = $p18Out2 = '';

        \Yii::$app->db2->createCommand($p18)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindParam(':DS_ERRO_P', $p18Out1, \PDO::PARAM_STR, 600)
            ->bindParam(':IE_CONSISTENCIA_P', $p18Out2, \PDO::PARAM_STR, 600)
            ->execute();


        if ($p18Out1 != '') {
            throw new \Exception("CONSISTIR_AGENDA_CONV_ESPEC: {$p18Out1}", 1);
        }


        /**************************** */

        $p17 = " 
            BEGIN
                TASY.CONSISTIR_AGENDA_CONSULTA(                
                    :CD_AGENDA_P,
                    :NR_SEQUENCIA_P,
                    :DT_AGENDA_P,
                    :CD_PESSOA_FISICA_P,
                    :NM_USUARIO_P,
                    :CD_ESTABELECIMENTO_P,
                    :DS_ERRO_P
                );
            END;
            ";
        $p17Out1 = '';

        \Yii::$app->db2->createCommand($p17)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':NR_SEQUENCIA_P', $this->nr_sequencia)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindParam(':DS_ERRO_P', $p17Out1, \PDO::PARAM_STR, 600)
            ->execute();

        if ($p17Out1 != '') {
            throw new \Exception("CONSISTIR_AGENDA_CONSULTA: {$p17Out1}", 1);
        }

        /**************************** */


        $p14 = " 
            BEGIN
                TASY.CONSISTE_REGRA_AGECONS_CONV(                
                    :CD_CONVENIO_P,
                    :CD_CATEGORIA_P,
                    :CD_AGENDA_P,
                    :CD_SETOR_ATENDIMENTO_P,
                    :CD_PLANO_CONVENIO_P,
                    :CD_PESSOA_FISICA_P,
                    :DT_AGENDA_P,
                    :CD_ESTABELECIMENTO_P,
                    :CD_EMPRESA_REF_P,
                    :NR_SEQUENCIA_P
                );
            END;
            ";

        \Yii::$app->db2->createCommand($p14)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':CD_CATEGORIA_P', 1)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':CD_SETOR_ATENDIMENTO_P', 0)
            ->bindValue(':CD_PLANO_CONVENIO_P', '')
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':CD_EMPRESA_REF_P', null)
            ->bindValue(':NR_SEQUENCIA_P', $this->nr_sequencia)
            ->execute();

        /**************************** */

        $p13 = " 
            BEGIN
                TASY.CONSISTE_IDADE_PAC_AGECONS(                
                    :CD_AGENDA_P,
                    :DT_AGENDA_P,
                    :QT_IDADE_P,
                    :CD_PESSOA_FISICA_P,
                    :DS_ERRO_P,
                    :NM_USUARIO_P
                );
            END;
            ";
        $p13Out1 = '';

        \Yii::$app->db2->createCommand($p13)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':QT_IDADE_P', 0)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindParam(':DS_ERRO_P', $p13Out1, \PDO::PARAM_STR, 600)
            ->bindvalue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->execute();


        if ($p13Out1 != '') {
            throw new \Exception("CONSISTE_IDADE_PAC_AGECONS: {$p13Out1}");
        }

        /**************************** */

        $p7 = " 
                BEGIN
                    TASY.CONSISTE_CONVENIO_AGECONS(
                        :NR_SEQ_DESTINO_P,
                        :CD_AGENDA_P,
                        :CD_CONVENIO_P,
                        :DT_AGENDA_P,
                        :NM_USUARIO_P
                    );
                END;
                ";
        $p7Out1 = '';

        \Yii::$app->db2->createCommand($p7)
            ->bindValue(':NR_SEQ_DESTINO_P', '')
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->execute();

        /**************************** */

        $p16 = " 
            BEGIN
                TASY.CONSISTIR_AGEND_ESPECIALIDADE(                
                    :CD_PESSOA_FISICA_P,
                    :CD_AGENDA_P,
                    :DT_AGENDA_P,
                    :NR_MIN_DURACAO_P,
                    :IE_PERMITE_P
                );
            END;
            ";
        $p16Out1 = '';

        \Yii::$app->db2->createCommand($p16)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':NR_MIN_DURACAO_P', $this->qt_duracao)
            ->bindParam(':IE_PERMITE_P', $p16Out1, \PDO::PARAM_STR, 600)
            ->execute();

        /**************************** */


        $sql14 = " 
            BEGIN
                TASY.GERAR_AUTOR_REGRA(
                    :NR_ATENDIMENTO_P, 
                    :NR_SEQ_MATERIAL_P, 
                    :NR_SEQ_PROCED_P, 
                    :NR_PRESCRICAO_P,
                    :NR_SEQ_MAT_PRESCR_P, 
                    :NR_SEQ_PROC_PRESCR_P,
                    :IE_EVENTO_P, 
                    :NM_USUARIO_P,
                    :NR_SEQ_AGENDA_P, 
                    :NR_SEQ_PROC_INTERNO_P,
                    :NR_SEQ_GESTAO_P, 
                    :NR_SEQ_AGENDA_CONSULTA_P, 
                    :NR_SEQ_AGENDA_PAC_OPME_P,
                    :NR_SEQ_AGENDA_PROC_ADIC_P, 
                    :NR_SEQ_PROC_ORCAMENTO_P,
                    :NR_SEQ_MAT_ORCAMENTO_P, 
                    :NR_SEQ_PARAMETRO3_P
                );
            END;";

        \Yii::$app->db2->createCommand($sql14)
            ->bindValue(':NR_ATENDIMENTO_P', null)
            ->bindValue(':NR_SEQ_MATERIAL_P', null)
            ->bindValue(':NR_SEQ_PROCED_P', null)
            ->bindValue(':NR_PRESCRICAO_P', null)
            ->bindValue(':NR_SEQ_MAT_PRESCR_P', null)
            ->bindValue(':NR_SEQ_PROC_PRESCR_P', null)
            ->bindValue(':IE_EVENTO_P', 'AP')
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_sequencia)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', null)
            ->bindValue(':NR_SEQ_GESTAO_P', null)
            ->bindValue(':NR_SEQ_AGENDA_CONSULTA_P', null)
            ->bindValue(':NR_SEQ_AGENDA_PAC_OPME_P', null)
            ->bindValue(':NR_SEQ_AGENDA_PROC_ADIC_P', null)
            ->bindValue(':NR_SEQ_PROC_ORCAMENTO_P', '')
            ->bindValue(':NR_SEQ_MAT_ORCAMENTO_P', '')
            ->bindValue(':NR_SEQ_PARAMETRO3_P', '')
            ->execute();

        /**************************** */

        // \Yii::$app->db2->createCommand(
        //     "BEGIN 
        //         TASY.W_AGECONS_LIMPAR_AGENDAS_LIB(:NM_USUARIO_P);
        //     END;"
        // )
        //     ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
        //     ->execute();

        /**************************** */
        $p19 = " 
                BEGIN
                    TASY.HORARIO_LIVRE_CONSULTA(
                        :CD_ESTABELECIMENTO_P,
                        :CD_AGENDA_P,
                        :IE_FERIADO_P,
                        :DT_AGENDA_P,
                        :NM_USUARIO_P,
                        :IE_GRAVAR_P,
                        :IE_SOBRA_HORARIO_P,
                        :IE_CONSULTA_P,
                        :QT_HORARIOS_P,
                        :DS_RETORNO_P
                    );
                END;
                ";
        $p19Out1 = '';

        \Yii::$app->db2->createCommand($p19)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':DT_AGENDA_P',  $this->horario)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':IE_FERIADO_P', 'S')
            ->bindValue(':NM_USUARIO_P',  Yii::$app->params['checkupuser'])
            ->bindValue(':IE_GRAVAR_P', 'S')
            ->bindValue(':IE_SOBRA_HORARIO_P', 'N')
            ->bindValue(':IE_CONSULTA_P', 'N')
            ->bindValue(':QT_HORARIOS_P', 0)
            ->bindParam(':DS_RETORNO_P', $p19Out1, \PDO::PARAM_STR, 600)
            ->execute();

        if ($p19Out1 != '') {
            throw new \Exception("HORARIO_LIVRE_CONSULTA: {$p19Out1}");
        }

        /**************************** */

        $p20 = " 
            BEGIN
                TASY.ALTERAR_STATUS_AGENDA_REGRA(:CD_ESTABELECIMENTO_P, :IE_FONTE_P);
            END;";
        \Yii::$app->db2->createCommand($p20)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':IE_FONTE_P', 'A')
            ->execute();


        /**************************** */

        $p20 = " 
            BEGIN
                TASY.GERAR_AUTOR_AFTERPOST_AGECONS(:NR_SEQ_AGENDA_P, :CD_ESTABELECIMENTO_P, :NM_USUARIO_P);
            END;";

        \Yii::$app->db2->createCommand($p20)
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_sequencia)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->execute();

        /**************************** */


        // $this->update(false);

        /**************************** */
        // } catch (\Exception $e) {
        //     $this->rollBackCheckupInfo();
        //     throw new \Exception($e->getMessage());
        // }
    }

    public function cancelCheckup()
    {
        $this->loadFieldsForCheckup();

        \Yii::$app->db2->createCommand(
            "BEGIN 
                TASY.ALTERAR_STATUS_AGECONS(
                    :CD_AGENDA_P,
                    :NR_SEQ_AGENDA_P,
                    :IE_STATUS_P,
                    :CD_MOTIVO_P,
                    :DS_MOTIVO_P,
                    :IE_AGENDA_DIA_P,
                    :NM_USUARIO_P,
                    :NR_SEQ_FORMA_CONF_P
                );
            END;"
        )
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_sequencia)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':IE_STATUS_P', 'C')
            ->bindValue(':CD_MOTIVO_P', 107)
            ->bindValue(':DS_MOTIVO_P', '')
            ->bindValue(':IE_AGENDA_DIA_P', 'N')
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':NR_SEQ_FORMA_CONF_P', '')
            ->execute();


        /**************************** */
        $p19 = " 
                BEGIN
                    TASY.HORARIO_LIVRE_CONSULTA(
                        :CD_ESTABELECIMENTO_P,
                        :CD_AGENDA_P,
                        :IE_FERIADO_P,
                        :DT_AGENDA_P,
                        :NM_USUARIO_P,
                        :IE_GRAVAR_P,
                        :IE_SOBRA_HORARIO_P,
                        :IE_CONSULTA_P,
                        :QT_HORARIOS_P,
                        :DS_RETORNO_P
                    );
                END;
                ";
        $p19Out1 = '';

        \Yii::$app->db2->createCommand($p19)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':DT_AGENDA_P', date('d/m/Y', strtotime($this->horario)))
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':IE_FERIADO_P', 'S')
            ->bindValue(':NM_USUARIO_P',  Yii::$app->params['checkupuser'])
            ->bindValue(':IE_GRAVAR_P', 'S')
            ->bindValue(':IE_SOBRA_HORARIO_P', 'N')
            ->bindValue(':IE_CONSULTA_P', 'N')
            ->bindValue(':QT_HORARIOS_P', 0)
            ->bindParam(':DS_RETORNO_P', $p19Out1, \PDO::PARAM_STR, 600)
            ->execute();
    }

    public function rollBackCheckupInfo()
    {
        $this->loadFieldsForCheckup();

        \Yii::$app->db2->createCommand(
            "BEGIN 
                TASY.LIBERAR_HORARIO_AGECONS(:NR_SEQ_AGENDA_P, :NM_USUARIO_P, :NR_SEQ_TURNO_P);
            END;"
        )
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_sequencia)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':NR_SEQ_TURNO_P', $this->nr_seq_turno)
            ->execute();

        \Yii::$app->db2->createCommand(
            "DELETE FROM TASY.AGENDA_CONSULTA WHERE CD_PESSOA_FISICA = :CD_PESSOA_FISICA_P 
                AND DT_AGENDA = :DT_AGENDA_P
                AND NR_SEQUENCIA = :NR_SEQUENCIA
                "
        )
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':NR_SEQUENCIA', $this->nr_sequencia)
            ->execute();
    }
}
