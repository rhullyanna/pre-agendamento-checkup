<?php

namespace app\modules\paciente;

use Yii;
use yii\helpers\Url;
use yii\web\ErrorHandler;

/**
 * paciente module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\paciente\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->layout = '@app/modules/paciente/views/layouts/main';
        Yii::$app->user->loginUrl = ['/paciente/default/login'];
        Yii::$app->setHomeUrl(Url::to(['/paciente/default/index']));

        Yii::configure($this, [
            'components' => [
                'errorHandler' => [
                    'class' => ErrorHandler::className(),
                    'errorAction' => '/paciente/default/error',
                ]
            ],
        ]);

        $handler = $this->get('errorHandler');
        Yii::$app->set('errorHandler', $handler);
        $handler->register();
    }
}
