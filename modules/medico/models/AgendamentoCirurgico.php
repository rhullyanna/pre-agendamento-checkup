<?php

namespace app\modules\medico\models;

use app\components\LogActiveRecord;
use yii\helpers\Url;
use app\modules\admin\models\Medico;
use app\models\StatusEnum;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "agendamento_cirurgico".
 *
 * @property int $id
 * @property int $cd_medico
 * @property string $nome_convenio
 * @property string $numero_convenio
 * @property int $procedimento_id
 * @property int $carater_id
 * @property int $cid_id
 */
class AgendamentoCirurgico extends LogActiveRecord
{
    const SCENARIO_DEFAULT     = 'default';
    const SCENARIO_UPDATE      = 'update';
    const SCENARIO_CANCEL      = 'cancel';


    public $documentos;
    public $nr_carteirinha;
    public $procedimentos;
    public $cd_perfil = 1190;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agendamento_cirurgico';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => \yii\behaviors\AttributeBehavior::class,
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['horario'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['horario'],
                ],
                'value' => function ($event) {
                    $date = \DateTime::createFromFormat('d/m/Y H:i:s', $this->horario);
                    if (!$date) {
                        $date = \DateTime::createFromFormat('Y-m-d H:i:s', $this->horario);
                    }
                    return $date->format('Y-m-d H:i:s');
                },
            ],
            [
                'class'      => \yii\behaviors\AttributeBehavior::class,
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_AFTER_FIND => ['horario'],
                ],
                'value' => function ($event) {
                    if ($this->horario) {
                        return date('d/m/Y H:i:s', strtotime($this->horario));
                    }
                },
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEFAULT] =
            [
                'cd_medico', 'nome_paciente', 'cd_pessoa_fisica', 'convenio_id',
                'nome_convenio', 'documentos', 'nr_carteirinha', 'horario', 'cd_agenda', 'nome_medico', 'cd_plano',
                'nr_seq_proc_interno',  'nome_procedimento', 'carater_id',
                'nome_carater', 'cd_plano', 'cd_acomodacao', 'nome_plano', 'procedimentos', 'observacao',
            ];
        $scenarios[self::SCENARIO_UPDATE] =
            [
                'cd_medico', 'nome_paciente', 'nr_seq_proc_interno', 'carater_id', 'nome_convenio', 'documentos'
            ];
        $scenarios[self::SCENARIO_CANCEL] = ['status'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[
                'cd_medico',  'convenio_id', 'cd_plano', 'carater_id', 'nr_seq_proc_interno', 'observacao'
            ], 'required', 'on' => [self::SCENARIO_DEFAULT]],
            [['cd_medico', 'nome_paciente', 'nr_seq_proc_interno', 'carater_id'], 'required', 'on' => [self::SCENARIO_UPDATE]],
            [['cd_medico', 'nr_seq_proc_interno'], 'integer'],
            [['nome_convenio'], 'string', 'max' => 80],
            [['documentos'], 'safe'],
            [['documentos'], 'file', 'extensions' => 'png, jpg, pdf, jpeg, doc'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cd_medico' => 'Medico ID',
            'convenio_id' => 'Convênio',
            'cd_pessoa_fisica' => 'Nome',
            'nome_paciente' => 'Nome',
            'nome_acomodacao' => 'Acomodação',
            'nome_convenio' => 'Nome do Convênio',
            'nome_carater' => 'Caráter',
            'carater_id' => 'Caráter',
            'cd_plano' => 'Plano',
            'documentos' => 'Documentos',
            'nome_procedimento' => 'Procedimento',
            'nr_seq_proc_interno' => 'Procedimento',
            'observacao' => 'Observações',
            'nome_procedimento_adicional' => 'Procedimento Adicional',
            'cd_acomodacao' => 'Acomodação',
            'documentos' => 'Documentos'
        ];
    }


    public function getMedico()
    {
        return $this->hasOne(Medico::class, ['id' => 'cd_medico']);
    }

    public function getDocumentoscirurgico()
    {
        return $this->hasMany(CirurgicoDocumento::class, ['cirurgico_id' => 'id']);
    }

    public function getStatusText()
    {
        $options = StatusEnum::list();
        return $options[$this->status];
    }



    public function init()
    {
        if ($this->getScenario() == self::SCENARIO_DEFAULT) {
            $this->status = StatusEnum::ATIVO;
        }
    }

    public function verifyPathUpload()
    {
        if (!is_dir(Yii::getAlias('@uploads/cirurgico/') . $this->id)) {
            mkdir(Yii::getAlias('@uploads/cirurgico/') . $this->id, 0777, true);
        }
    }

    public function verifyTempPathUpload()
    {
        if (!is_dir(Yii::getAlias('@uploads/cirurgico/'))) {
            mkdir(Yii::getAlias('@uploads/cirurgico/'), 0777, true);
        }
    }

    private function setFormattedTime()
    {
        $rh = \DateTime::createFromFormat('d/m/Y H:i:s', $this->horario);

        if (!$rh) {
            $horario = date('d/m/Y H:i:s', strtotime($this->horario));
        } else {
            $horario = $this->horario;
        }

        $this->horario = $horario;
    }

    public function sendInfoCheckup()
    {
        $this->cd_medico = $this->medico->cd_pessoa_fisica;
        $this->cd_agenda = Yii::$app->params['checkupagendc'];

        // $hoje = new \DateTime();
        // $hoje->add(new \DateInterval('P1D'));
        // $this->horario = $hoje->format('d/m/Y');

        $resultado = \Yii::$app->db2->createCommand("
                SELECT *
                FROM  
                (
                    SELECT
                    A.HR_INICIO,
                    A.DT_AGENDA,
                    A.NR_SEQUENCIA,
                    A.NR_MINUTO_DURACAO
                    FROM AGENDA_PACIENTE A
                    WHERE 1 = 1
                    AND IE_STATUS_AGENDA = 'L' 
                    AND TRUNC(DT_AGENDA) > TRUNC(SYSDATE)
                    AND CD_AGENDA = :CD_AGENDA
                    ORDER BY HR_INICIO ASC
                )
                WHERE ROWNUM = 1
            ")
            ->bindValue(':CD_AGENDA', $this->cd_agenda)
            ->queryOne();

        $this->nr_seq_agenda = $resultado['NR_SEQUENCIA'];
        $this->horario = $resultado['HR_INICIO'];
        $this->qt_duracao = $resultado['NR_MINUTO_DURACAO'];

        $this->setFormattedTime();

        /**************************** */
        $erro = "";
        $sql = "BEGIN
                    TASY.CONSISTE_QTD_AGEND_PERMITIDOS	(
                        :NR_SEQ_AGENDA_P,
                        :DS_ERRO_P
                    );
                END;";

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_seq_agenda)
            ->bindParam(':DS_ERRO_P', $erro, \PDO::PARAM_STR, 200);


        if ($erro != '') {
            throw new \Exception("CONSISTE_QTD_AGEND_PERMITIDOS: {$erro}");
        }

        //GERAR_CONVENIO_AGENDAMENTO
        /**************************** */

        $p1 = " 
            BEGIN
                TASY.GERAR_ENCAIXE_ATEND_AGENDA(
                    :DT_AGENDA_P, 
                    :QT_DURACAO_P, 
                    :CD_PESSOA_FISICA_P, 
                    :NM_PACIENTE_P,
                    :NR_ATENDIMENTO_P, 
                    :CD_MEDICO_P, 
                    :NR_SEQ_PROC_INTERNO_P, 
                    :CD_PROCEDIMENTO_P,
                    :IE_ORIGEM_PROCED_P, 
                    :IE_LADO_P, 
                    :CD_AGENDA_P,
                    :CD_ESTABELECIMENTO_P, 
                    :NM_USUARIO_P,
                    :DS_OBSERVACAO_P, 
                    :NM_PESSOA_CONTATO_P,
                    :NR_SEQ_AGENDA_P, 
                    :IE_CARATER_CIRURGIA_P,
                    :CD_PROCEDENCIA_P, 
                    :DS_ERRO_2_P, 
                    :NR_SEQ_CLASSIF_AGENDA_P
                );
            END;";
        $p1Out1 = $p1Out2 = '';
        \Yii::$app->db2->createCommand($p1)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':QT_DURACAO_P', (int) $this->qt_duracao)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':NM_PACIENTE_P', $this->nome_paciente)
            ->bindValue(':NR_ATENDIMENTO_P', 0)
            ->bindValue(':CD_MEDICO_P', (int) $this->cd_medico)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':CD_PROCEDIMENTO_P', (int) $this->procedimento_id)
            ->bindValue(':IE_ORIGEM_PROCED_P', $this->ie_origem_proced)
            ->bindValue(':IE_LADO_P', '')
            ->bindValue(':CD_AGENDA_P', (int) $this->cd_agenda)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':DS_OBSERVACAO_P', '')
            ->bindValue(':NM_PESSOA_CONTATO_P', '')
            ->bindParam(':NR_SEQ_AGENDA_P', $p1Out1, \PDO::PARAM_INT, 10)
            ->bindValue(':IE_CARATER_CIRURGIA_P', $this->carater_id)
            ->bindValue(':CD_PROCEDENCIA_P', '')
            ->bindParam(':DS_ERRO_2_P', $p1Out2, \PDO::PARAM_STR, 1000)
            ->bindValue(':NR_SEQ_CLASSIF_AGENDA_P', 12)
            ->execute();


        if ($p1Out1 == '') {
            throw new \Exception("GERAR_ENCAIXE_ATEND_AGENDA: {$p1Out1}:{$p1Out2}");
        }

        $this->nr_seq_agenda = $p1Out1;

        /**************************** */


        $sql18 = " 
                BEGIN

                UPDATE TASY.AGENDA_PACIENTE SET 
                    CD_CONVENIO = :CD_CONVENIO, 
                    CD_PLANO = :CD_PLANO,
                    CD_TIPO_ACOMODACAO = :CD_TIPO_ACOMODACAO,
                    CD_USUARIO_CONVENIO = :CD_USUARIO_CONVENIO,
                    IE_FORMA_AGENDAMENTO = :IE_FORMA_AGENDAMENTO,
                    IE_STATUS_AGENDA = :IE_STATUS_AGENDA,
                    DS_OBSERVACAO = :DS_OBSERVACAO,
                    NM_PACIENTE = :NM_PACIENTE
                    
                WHERE 1=1
                    AND HR_INICIO = :HR_INICIO
                    AND NR_SEQUENCIA = :NR_SEQUENCIA;
                END;";

        $command = \Yii::$app->db2->createCommand($sql18)
            ->bindValue(':CD_CONVENIO', $this->convenio_id)
            ->bindValue(':CD_PLANO', $this->cd_plano)
            ->bindValue(':CD_TIPO_ACOMODACAO', 1)
            ->bindValue(':CD_USUARIO_CONVENIO', $this->cd_pessoa_fisica)
            ->bindValue(':IE_FORMA_AGENDAMENTO', Yii::$app->params['checkupformaagend'])
            ->bindValue(':IE_STATUS_AGENDA', 'PA')
            ->bindValue(':DS_OBSERVACAO', $this->observacao)
            ->bindValue(':NM_PACIENTE', $this->nome_paciente)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':HR_INICIO', $this->horario)
            ->bindValue(':NR_SEQUENCIA', $this->nr_seq_agenda)

            ->execute();

        if (!$command) {
            throw new \Exception("UPDATE TASY.AGENDA_CONSULTA");
        }

        /**************************** */

        $sql = "BEGIN
                 TASY.OBTER_PROC_TAB_INTER_AGENDA	(
                     :NR_SEQ_INTERNO_P,
                     :NR_SEQ_AGENDA_P,
                     :CD_CONVENIO_P,
                     '',
                     '',
                     :CD_ESTABELECIMENTO_P,
                     '',
                     '',
                     '',
                     '',
                     '',
                     '',
                     '',
                     '',
                     '',
                     :CD_PROCEDIMENTO_P,
                     :IE_ORIGEM_PROCED_P
                 );
                 END;";

        $procedimento = $origem = '';

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':NR_SEQ_AGENDA_P', (int)$this->nr_seq_agenda)
            ->bindValue(':CD_CONVENIO_P', (int)$this->convenio_id)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindParam(':CD_PROCEDIMENTO_P', $procedimento, \PDO::PARAM_STR, 20)
            ->bindParam(':IE_ORIGEM_PROCED_P', $origem, \PDO::PARAM_STR, 20)
            ->execute();

        if ($procedimento == '') {
            throw new \Exception("OBTER_PROC_TAB_INTER_AGENDA: Não foi possível encontrar o convênio para este exame");
        } else {
            $this->procedimento_id = $procedimento;
            $this->ie_origem_proced = $origem;
        }

        /**************************** */

        $sql = "BEGIN
                    TASY.OBTER_SE_MEDICO_DISPONIVEL	(
                        :NR_SEQ_AGENDA_P,
                        :CD_MEDICO_P
                    );
                END;";

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':CD_MEDICO_P', (int) $this->cd_medico)
            ->execute();



        /**************************** */

        $sql5 = "
                BEGIN
                    TASY.CONSISTIR_PROC_CONV_AGENDA(
                        :CD_ESTABELECIMENTO_P,
                        :CD_PESSOA_FISICA_P,
                        :DT_REFERENCIA_P,
                        :CD_AGENDA_P,
                        :CD_CONVENIO_P,
                        :CD_CATEGORIA_P,
                        :CD_PROCEDIMENTO_P,
                        :IE_ORIGEM_PROCED_P,
                        :NR_SEQ_PROC_INTERNO_P,
                        :CD_MEDICO_P,
                        :IE_MEDICO_P,
                        :CD_PLANO_P,
                        :IE_CONSISTENCIA_P,
                        :IE_AGENDA_P,
                        :NR_SEQUENCIA_P,
                        :NR_SEQ_REGRA_P,
                        :IE_CONSIST_JS_P,
                        :NR_SEQ_COBERTURA_P,
                        :NR_MINUTO_DURACAO_P,
                        :CD_EMPRESA_REF_P,
                        :IE_ANESTESIA_P,
                        :IE_PROC_ADIC_P,
                        :NR_SEQ_ITEM_P
                    );
                END;";

        $p20Out1 = $p20Out2 = $p20Out3 = $p20Out4 = '';

        \Yii::$app->db2->createCommand($sql5)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':CD_PESSOA_FISICA_P', (int)$this->cd_pessoa_fisica)
            ->bindValue(':DT_REFERENCIA_P', $this->horario)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':CD_CONVENIO_P', (int) $this->convenio_id)
            ->bindValue(':CD_CATEGORIA_P', '')
            ->bindValue(':CD_PROCEDIMENTO_P', (int)$this->procedimento_id)
            ->bindValue(':IE_ORIGEM_PROCED_P', $this->ie_origem_proced)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':CD_MEDICO_P', $this->cd_medico)
            ->bindValue(':IE_MEDICO_P', 'E')
            ->bindValue(':CD_PLANO_P', $this->cd_plano)
            ->bindParam(':IE_CONSISTENCIA_P', $p20Out1, \PDO::PARAM_STR, 900)
            ->bindParam(':IE_AGENDA_P', $p20Out2, \PDO::PARAM_STR, 600)
            ->bindValue(':NR_SEQUENCIA_P', (int) $this->nr_seq_agenda)
            ->bindParam(':NR_SEQ_REGRA_P', $p20Out3, \PDO::PARAM_STR, 600)
            ->bindParam(':IE_CONSIST_JS_P', $p20Out4, \PDO::PARAM_STR, 600)
            ->bindValue(':NR_SEQ_COBERTURA_P', '')
            ->bindValue(':NR_MINUTO_DURACAO_P', $this->qt_duracao)
            ->bindValue(':CD_EMPRESA_REF_P', 0)
            ->bindValue(':IE_ANESTESIA_P', 'N')
            ->bindValue(':IE_PROC_ADIC_P', null)
            ->bindValue(':NR_SEQ_ITEM_P', null)
            ->execute();


        if ($p20Out1 != '') {
            throw new \Exception("CONSISTIR_PROC_CONV_AGENDA 1: {$p20Out1}");
        }

        /**************************** */
        // TODO o que é esse cd_perfil
        $sql = "BEGIN
                    TASY.CONSISTIR_LIMITE_PROCED_AGENDA	(
                        :CD_PROCEDIMENTO_P,
                        :CD_PERFIL_P,
                        :IE_ORIGEM_PROCED_P,
                        :NR_SEQ_PROC_INTERNO_P,
                        :CD_TIPO_AGENDA_P,
                        :DT_AGENDA_P,
                        :CD_CONVENIO_P,
                        :CD_MEDICO_P,
                        :IE_ACAO_REGRA_P
                    );
                END;";

        $regra = '';

        $command = \Yii::$app->db2->createCommand($sql)
            ->bindValue(':CD_PROCEDIMENTO_P', $this->procedimento_id)
            ->bindValue(':CD_PERFIL_P', $this->cd_perfil)
            ->bindValue(':IE_ORIGEM_PROCED_P', $this->ie_origem_proced)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', $this->nr_seq_proc_interno)
            ->bindValue(':CD_TIPO_AGENDA_P', 1)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_CONVENIO_P', $this->convenio_id)
            ->bindValue(':CD_MEDICO_P', $this->cd_medico)
            ->bindParam(':IE_ACAO_REGRA_P', $regra, \PDO::PARAM_STR, 100)
            ->execute();

        if (!$command) {
            throw new \Exception("CONSISTIR_LIMITE_PROCED_AGENDA");
        }

        /**************************** */

        $sql = "BEGIN
                    TASY.CONSISTIR_DURACAO_AGENDA_PAC	(
                        :CD_AGENDA_P,
                        :DT_AGENDA_P,
                        :HR_INICIO_P,
                        :NR_MINUTO_DURACAO_P,
                        :NR_SEQ_AGENDA_P,
                        :NR_SEQ_ORIGEM_P,
                        :DS_ERRO_P
                    );
                END;";

        $erro = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':CD_AGENDA_P', (int) $this->cd_agenda)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':HR_INICIO_P', $this->horario)
            ->bindValue(':NR_MINUTO_DURACAO_P', $this->qt_duracao)
            ->bindValue(':NR_SEQ_AGENDA_P', (int)$this->nr_seq_agenda)
            ->bindValue(':NR_SEQ_ORIGEM_P', '')
            ->bindParam(':DS_ERRO_P', $erro, \PDO::PARAM_STR, 500)
            ->execute();

        if ($erro != '') {
            throw new \Exception("{$erro}");
        }

        /**************************** */


        $sql = "BEGIN
                    TASY.CONSISTE_SE_AGENDA_UNICA	(
                        :NR_SEQ_AGENDA_P,
                        :CD_AGENDA_P,
                        :DT_AGENDA_P,
                        :HR_INICIO_P,
                        :IE_STATUS_AGENDA_P,
                        :NM_USUARIO_P,
                        :CD_ESTABELECIMENTO_P
                    );
                END;";

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':CD_AGENDA_P', (int) $this->cd_agenda)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':HR_INICIO_P', $this->horario)
            ->bindValue(':IE_STATUS_AGENDA_P', 'N')
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->execute();


        /**************************** */

        $sql = "BEGIN
                    TASY.CONSISTE_REGRA_QTD_AGENDAMENTO	(
                        :NR_SEQUENCIA_P,
                        :CD_MEDICO_P, 
                        :CD_CONVENIO_P,
                        :DT_AGENDA_P,
                        :CD_AGENDA_P,
                        :CD_TURNO_P,
                        :IE_CARATER_CIRURGIA_P,
                        :IE_TRANSFERENCIA_P,
                        :IE_COPIA_P,
                        :DS_ERRO_P,
                        :IE_SOMENTE_AVISO_P
                    );
                END;";

        $erro = $aviso = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQUENCIA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':CD_MEDICO_P', (int) $this->cd_medico)
            ->bindValue(':CD_CONVENIO_P', (int) $this->convenio_id)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':CD_AGENDA_P', (int) $this->cd_agenda)
            ->bindValue(':CD_TURNO_P', 0)
            ->bindValue(':IE_CARATER_CIRURGIA_P', 'A')
            ->bindValue(':IE_TRANSFERENCIA_P', 'N')
            ->bindValue(':IE_COPIA_P', 'N')
            ->bindParam(':DS_ERRO_P',  $erro, \PDO::PARAM_STR, 500)
            ->bindParam(':IE_SOMENTE_AVISO_P', $aviso, \PDO::PARAM_STR, 500)
            ->execute();

        if ($erro != '') {
            throw new \Exception("{$erro} : {$aviso}");
        }


        /**************************** */

        $sql = "BEGIN
                    TASY.GERAR_DADOS_PRE_AGENDA	(
                        :CD_PROCEDIMENTO_P,
                        :IE_ORIGEM_PROCED_P,
                        :NR_SEQUENCIA_P,
                        :NR_SEQ_PROC_INTERNO_P,
                        :CD_MEDICO_P,
                        :CD_PESSOA_FISICA_P,
                        :NM_USUARIO_P,
                        :CD_CONVENIO_P,
                        :CD_PLANO_AGENDA_P,
                        :DS_ORIENTACAO_P,
                        :CD_ESTABELECIMENTO_P,
                        :IE_GERAR_EQUIP_P,
                        :IE_GERAR_CME_P,
                        :IE_GERAR_OPME_P,
                        :IE_GERAR_SERVICO_P,
                        :IE_GERAR_CAIXA_OPME_P,
                        :DS_ERRO_P,
                        :DS_ERRO_2_P
                    );
                END;";


        $erro1 = $erro2 = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':CD_PROCEDIMENTO_P', (int) $this->procedimento_id)
            ->bindValue(':IE_ORIGEM_PROCED_P', (int) $this->ie_origem_proced)
            ->bindValue(':NR_SEQUENCIA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int) $this->nr_seq_proc_interno)
            ->bindValue(':CD_MEDICO_P', (int) $this->cd_medico)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_CONVENIO_P', (int)$this->convenio_id)
            ->bindValue(':CD_PLANO_AGENDA_P', '')
            ->bindValue(':DS_ORIENTACAO_P', '')
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':IE_GERAR_EQUIP_P', 'N')
            ->bindValue(':IE_GERAR_CME_P', 'N')
            ->bindValue(':IE_GERAR_OPME_P', 'N')
            ->bindValue(':IE_GERAR_SERVICO_P', 'N')
            ->bindValue(':IE_GERAR_CAIXA_OPME_P', 'N')
            ->bindParam(':DS_ERRO_P', $erro1, \PDO::PARAM_STR, 500)
            ->bindParam(':DS_ERRO_2_P', $erro2, \PDO::PARAM_STR, 500)
            ->execute();

        if ($erro1 != '' || $erro2 != '') {
            throw new \Exception("{$erro1} : {$erro2}");
        }
        /**************************** */

        $sql14 = " 
        BEGIN
            TASY.GERAR_AUTOR_REGRA(
                :NR_ATENDIMENTO_P, 
                :NR_SEQ_MATERIAL_P, 
                :NR_SEQ_PROCED_P, 
                :NR_PRESCRICAO_P,
                :NR_SEQ_MAT_PRESCR_P, 
                :NR_SEQ_PROC_PRESCR_P,
                :IE_EVENTO_P, 
                :NM_USUARIO_P,
                :NR_SEQ_AGENDA_P, 
                :NR_SEQ_PROC_INTERNO_P,
                :NR_SEQ_GESTAO_P, 
                :NR_SEQ_AGENDA_CONSULTA_P, 
                :NR_SEQ_AGENDA_PAC_OPME_P,
                :NR_SEQ_AGENDA_PROC_ADIC_P, 
                :NR_SEQ_PROC_ORCAMENTO_P,
                :NR_SEQ_MAT_ORCAMENTO_P, 
                :NR_SEQ_PARAMETRO3_P
            );
        END;";

        \Yii::$app->db2->createCommand($sql14)
            ->bindValue(':NR_ATENDIMENTO_P', null)
            ->bindValue(':NR_SEQ_MATERIAL_P', null)
            ->bindValue(':NR_SEQ_PROCED_P', null)
            ->bindValue(':NR_PRESCRICAO_P', null)
            ->bindValue(':NR_SEQ_MAT_PRESCR_P', null)
            ->bindValue(':NR_SEQ_PROC_PRESCR_P', null)
            ->bindValue(':IE_EVENTO_P', 'AP')
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int)$this->nr_seq_proc_interno)
            ->bindValue(':NR_SEQ_GESTAO_P', null)
            ->bindValue(':NR_SEQ_AGENDA_CONSULTA_P', null)
            ->bindValue(':NR_SEQ_AGENDA_PAC_OPME_P', null)
            ->bindValue(':NR_SEQ_AGENDA_PROC_ADIC_P', null)
            ->bindValue(':NR_SEQ_PROC_ORCAMENTO_P', '')
            ->bindValue(':NR_SEQ_MAT_ORCAMENTO_P', '')
            ->bindValue(':NR_SEQ_PARAMETRO3_P', '')
            ->execute();

        /**************************** */

        $sql = "BEGIN
                TASY.CONSISTIR_RESERVA_LEITO (
                    :CD_AGENDA_P,
                    :DT_AGENDA_P,
                    :IE_RESERVA_LEITO_P,
                    :QT_DIARIA_PREV_P,
                    :NR_SEQUENCIA_P,
                    :DS_ERRO_P
                );
            END;";

        $erro1 = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':CD_AGENDA_P', (int) $this->cd_agenda)
            ->bindValue(':DT_AGENDA_P', $this->horario)
            ->bindValue(':IE_RESERVA_LEITO_P', 'O')
            ->bindValue(':QT_DIARIA_PREV_P', 0)
            ->bindValue(':NR_SEQUENCIA_P', (int) $this->nr_seq_agenda)
            ->bindParam(':DS_ERRO_P', $erro1, \PDO::PARAM_STR, 500)
            ->execute();

        if ($erro1 != '') {
            throw new \Exception("CONSISTIR_RESERVA_LEITO: {$erro1}");
        }

        /**************************** */

        $sql = "BEGIN
                TASY.CONSISTIR_DIAS_AGEND_DUPLIC (
                    :NR_SEQ_AGEND_P,
                    :CD_PESSOA_FISICA_P,
                    :CD_PROCEDIMENTO_P,
                    :CD_ESTABELECIMENTO_P
                );
            END;";

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGEND_P', (int) $this->nr_seq_agenda)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':CD_PROCEDIMENTO_P', $this->procedimento_id)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->execute();


        /**************************** */

        $sql = "BEGIN
                    TASY.CONSISTE_REGRA_AGENDA (
                        :IE_MOMENTO_P,
                        :CD_AGENDA_P,
                        :CD_PESSOA_FISICA_P,
                        :CD_PROCEDIMENTO_P,
                        :IE_ORIGEM_PROCED_P,
                        :CD_CONVENIO_P,
                        :CD_PERFIL_P,
                        :CD_ESTABELECIMENTO_P,
                        :NM_USUARIO_P,
                        :CD_MEDICO_CIRURGIAO_P,
                        :NR_SEQ_AGENDA_P
                    );
                END;";

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':IE_MOMENTO_P', 'AS')
            ->bindValue(':CD_AGENDA_P', (int) $this->cd_agenda)
            ->bindValue(':CD_PESSOA_FISICA_P', (int)$this->cd_pessoa_fisica)
            ->bindValue(':CD_PROCEDIMENTO_P', (int)$this->procedimento_id)
            ->bindValue(':IE_ORIGEM_PROCED_P', $this->ie_origem_proced)
            ->bindValue(':CD_CONVENIO_P', (int)$this->convenio_id)
            ->bindValue(':CD_PERFIL_P', (int)$this->cd_perfil)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_MEDICO_CIRURGIAO_P', '')
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_seq_agenda)
            ->execute();

        /**************************** */

        $sql = "BEGIN
                    TASY.CONS_DIAS_AGEND_DUPLIC_PF (
                        :NR_SEQ_AGEND_P,
                        :CD_PESSOA_FISICA_P,
                        :CD_ESTABELECIMENTO_P,
                        :QT_REGRA_P,
                        :DS_MENSAGEM_P
                    );
                END;";


        $msg = $regra = "";

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGEND_P', (int)$this->nr_seq_agenda)
            ->bindValue(':CD_PESSOA_FISICA_P', $this->cd_pessoa_fisica)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindParam(':QT_REGRA_P', $regra, \PDO::PARAM_STR, 500)
            ->bindParam(':DS_MENSAGEM_P', $msg, \PDO::PARAM_STR, 500)
            ->execute();

        if ($msg != '') {
            throw new \Exception("{$msg}: {$regra}");
        }

        /**************************** */

        $sql = "BEGIN
                    TASY.ATUALIZAR_EVENTO_AUTOR_CONV (
                        :IE_EVENTO_P,
                        :NR_SEQ_AGENDA_P,
                        :NR_SEQUENCIA_AUTOR_P,
                        :NR_SEQ_GESTAO_VAGA_P,
                        :NM_USUARIO_P
                    );
                END;";

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':IE_EVENTO_P', 1)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':NR_SEQUENCIA_AUTOR_P', '')
            ->bindValue(':NR_SEQ_GESTAO_VAGA_P', '')
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->execute();

        /**************************** */


        $sql = "BEGIN
                    TASY.GERAR_ESCALA_PARTIC_CIR (
                        :NR_SEQ_AGENDA_P,
                        :NM_USUARIO_P,
                        :CD_ESTABELECIMENTO_P
                    );
                END;";

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->execute();

        /**************************** */

        $sql = "BEGIN
                    TASY.GERAR_AGENDA_PROCED_KIT (
                        :NR_SEQ_AGENDA_P,
                        :CD_PESSOA_FISICA_P,
                        :NM_USUARIO_P,
                        :DS_ERRO_P
                    );
                END;";

        $erro = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':CD_PESSOA_FISICA_P', (int)$this->cd_pessoa_fisica)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindParam(':DS_ERRO_P', $erro, \PDO::PARAM_STR, 500)
            ->execute();

        // if ($erro != '') {
        //     throw new \Exception("GERAR_AGENDA_PROCED_KIT: {$erro}");
        // }

        /**************************** */

        $sql = "BEGIN
                    TASY.CONSISTIR_AGENDA_CIRURGICA (
                        :NR_SEQ_AGENDA_P,
                        :IE_EQUIP_CLASSIF_P,
                        :CD_ESTABELECIMENTO_P,
                        :NM_USUARIO_P			 
                    );
                END;";

        $erro = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':IE_EQUIP_CLASSIF_P', 'E')
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->execute();

        /**************************** */


        $sql = "BEGIN
                    TASY.ATUALIZAR_AGENDA_BOLSAS_SANGUE (
                        :NR_SEQ_AGENDA_P,
                        :NR_SEQ_PROC_INTERNO_P,
                        :NM_USUARIO_P			 
                    );
                END;";

        $erro = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':NR_SEQ_PROC_INTERNO_P', (int)$this->nr_seq_proc_interno)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->execute();

        /**************************** */


        $sql = "BEGIN
                    TASY.ENVIAR_EMAIL_REGRA (
                        :NR_SEQ_AGENDA_P,
                        :IE_MOMENTO_P,
                        :NM_USUARIO_P,
                        :CD_ESTABELECIMENTO_P
                    );
                END;";

        $erro = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':IE_MOMENTO_P', 'SA')
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->execute();

        /**************************** */

        $sql = "BEGIN
                    TASY.CONSISTIR_QTDADE_AGENDAMENTO (
                        :NR_SEQ_AGENDA_P,
                        :NM_USUARIO_P,
                        :DS_CONSISTENCIA_P
                    );
                END;";

        $ds = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':NR_SEQ_AGENDA_P', (int) $this->nr_seq_agenda)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindParam(':DS_CONSISTENCIA_P', $ds, \PDO::PARAM_STR, 500)
            ->execute();

        /**************************** */

        $sql = "BEGIN
                    TASY.LIB_HOR_INCORRETO_AGENDA_PAC (
                        :DT_INICIAL_P,
                        :DT_FINAL_P,
                        :NM_USUARIO_P,
                        :CD_ESTABELECIMENTO_P
                    );
                END;";

        $ds = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':DT_INICIAL_P', $this->horario)
            ->bindValue(':DT_FINAL_P',   substr($this->horario, 0, 10) . " 23:59:59")
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->execute();

        /**************************** */

        // W_AGENDA_PROC_ADIC
    }

    public function cancelCheckup()
    {

        $this->setFormattedTime();

        \Yii::$app->db2->createCommand(
            "BEGIN 
                TASY.ALTERAR_STATUS_AGECONS(
                    :CD_AGENDA_P,
                    :NR_SEQ_AGENDA_P,
                    :IE_STATUS_P,
                    :CD_MOTIVO_P,
                    :DS_MOTIVO_P,
                    :IE_AGENDA_DIA_P,
                    :NM_USUARIO_P,
                    :NR_SEQ_FORMA_CONF_P
                );
            END;"
        )
            ->bindValue(':NR_SEQ_AGENDA_P', $this->nr_sequencia)
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':IE_STATUS_P', 'C')
            ->bindValue(':CD_MOTIVO_P', 107)
            ->bindValue(':DS_MOTIVO_P', '')
            ->bindValue(':IE_AGENDA_DIA_P', 'N')
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':NR_SEQ_FORMA_CONF_P', '')
            ->execute();


        /**************************** */
        $p19 = " 
                BEGIN
                    TASY.HORARIO_LIVRE_CONSULTA(
                        :CD_ESTABELECIMENTO_P,
                        :CD_AGENDA_P,
                        :IE_FERIADO_P,
                        :DT_AGENDA_P,
                        :NM_USUARIO_P,
                        :IE_GRAVAR_P,
                        :IE_SOBRA_HORARIO_P,
                        :IE_CONSULTA_P,
                        :QT_HORARIOS_P,
                        :DS_RETORNO_P
                    );
                END;
                ";
        $p19Out1 = '';

        \Yii::$app->db2->createCommand($p19)
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->bindValue(':DT_AGENDA_P', date('d/m/Y', strtotime($this->horario)))
            ->bindValue(':CD_AGENDA_P', $this->cd_agenda)
            ->bindValue(':IE_FERIADO_P', 'S')
            ->bindValue(':NM_USUARIO_P',  Yii::$app->params['checkupuser'])
            ->bindValue(':IE_GRAVAR_P', 'S')
            ->bindValue(':IE_SOBRA_HORARIO_P', 'N')
            ->bindValue(':IE_CONSULTA_P', 'N')
            ->bindValue(':QT_HORARIOS_P', 0)
            ->bindParam(':DS_RETORNO_P', $p19Out1, \PDO::PARAM_STR, 600)
            ->execute();
    }

    public function rollBackCheckupInfo()
    {
        $this->setFormattedTime();


        \Yii::$app->db2->createCommand(
            "DELETE FROM TASY.AGENDA_PACIENTE 
                WHERE 1=1
                AND HR_INICIO = :HR_INICIO
                AND NR_SEQUENCIA = :NR_SEQUENCIA
                "
        )
            ->bindValue(':HR_INICIO', $this->horario)
            ->bindValue(':NR_SEQUENCIA', $this->nr_seq_agenda)
            ->execute();


        $sql = "BEGIN
            TASY.LIB_HOR_INCORRETO_AGENDA_PAC (
                :DT_INICIAL_P,
                :DT_FINAL_P,
                :NM_USUARIO_P,
                :CD_ESTABELECIMENTO_P
                );
            END;";

        $ds = "";
        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':DT_INICIAL_P', $this->horario)
            ->bindValue(':DT_FINAL_P', substr($this->horario, 0, 10) . " 23:59:59")
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_ESTABELECIMENTO_P', Yii::$app->params['checkuplocal'])
            ->execute();
    }

    public function sendAddProcedure($pa)
    {
        $sql = "INSERT INTO W_AGENDA_PROC_ADIC 
        ( IE_LADO , NM_USUARIO , QT_PROCEDIMENTO , CD_PROCEDIMENTO , QT_MIN_PROCED , DT_ATUALIZACAO_NREC , CD_CONVENIO , CD_PESSOA_FISICA , CD_PLANO , 
        IE_ORIGEM_PROCED , NR_SEQUENCIA , NM_USUARIO_NREC , CD_MEDICO , NR_SEQ_PROC_INTERNO , DT_ATUALIZACAO )
        VALUES ( 
            :IE_LADO , 
            :NM_USUARIO_P , 
            :QT_PROCEDIMENTO , 
            :CD_PROCEDIMENTO , 
            :QT_MIN_PROCED , 
            sysdate , 
            :CD_CONVENIO , 
            :CD_PESSOA_FISICA , 
            :CD_PLANO , 
            :IE_ORIGEM_PROCED , 
            :NR_SEQUENCIA , 
            :NM_USUARIO_NREC , 
            :CD_MEDICO , 
            :NR_SEQ_PROC_INTERNO , 
            sysdate
            )
        ";

        \Yii::$app->db2->createCommand($sql)
            ->bindValue(':IE_LADO', $pa->ie_lado)
            ->bindValue(':NM_USUARIO_P', Yii::$app->params['checkupuser'])
            ->bindValue(':QT_PROCEDIMENTO', $pa->quantidade)
            ->bindValue(':CD_PROCEDIMENTO', $pa->procedimento_id)
            ->bindValue(':QT_MIN_PROCED', $pa->min_proced)
            ->bindValue(':CD_CONVENIO', $pa->convenio_id)
            ->bindValue(':CD_PESSOA_FISICA', $pa->cd_pessoa_fisica)
            ->bindValue(':CD_PLANO', $pa->plano_id)
            ->bindValue(':IE_ORIGEM_PROCED', '')
            ->bindValue(':NR_SEQUENCIA', $pa->nr_sequencia)
            ->bindValue(':NM_USUARIO_NREC', Yii::$app->params['checkupuser'])
            ->bindValue(':CD_MEDICO', $pa->medico_id)
            ->bindValue(':NR_SEQ_PROC_INTERNO', $pa->nr_seq_proc_interno)
            ->execute();
    }
}
