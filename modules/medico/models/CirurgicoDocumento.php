<?php

namespace app\modules\medico\models;

use Yii;

/**
 * This is the model class for table "cirurgico_documento".
 *
 * @property int $int
 * @property int $nome
 * @property int $url
 * @property int $cirurgico_id
 */
class CirurgicoDocumento extends \yii\db\ActiveRecord
{
    public $arquivo;
    public $excluir;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cirurgico_documento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['arquivo'], 'required'],
            [['excluir'], 'safe'],
            [['arquivo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, pdf, jpeg, doc', 'maxSize' => 3145728],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nome' => 'Nome',
            'url' => 'Url',
            'cirurgico_id' => 'Cirurgico ID',
        ];
    }

    public function verifyTempPathUpload()
    {
        if (!is_dir(Yii::getAlias('@uploads/cirurgico/'))) {
            mkdir(Yii::getAlias('@uploads/cirurgico/'), 0777, true);
        }
    }

    public function getPath()
    {
        return Yii::$app->request->baseUrl . '/images/uploads/cirurgico/' . $this->cirurgico_id . '/' . $this->url;
    }
}
