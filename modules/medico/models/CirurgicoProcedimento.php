<?php

namespace app\modules\medico\models;

use Yii;

/**
 * This is the model class for table "cirurgico_procedimento".
 *
 * @property int $id
 * @property int $procedimento_id
 * @property string $nome_procedimento
 * @property int $quantidade
 * @property int $convenio_id
 * @property int $plano_id
 * @property string $nome_convenio
 * @property int $cirurgico_id
 *
 * @property AgendamentoCirurgico $cirurgico
 */
class CirurgicoProcedimento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cirurgico_procedimento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['procedimento_id', 'nome_procedimento', 'convenio_id', 'nome_convenio', 'cirurgico_id'], 'required'],
            [[
                'procedimento_id', 'quantidade', 'convenio_id', 'plano_id', 'cirurgico_id', 'categoria_id', 'min_proced',
                'medico_id', 'nome_medico', 'ie_lado', 'nome_lado', 'nr_sequencia', 'nr_seq_proc_interno', 'nome_plano', 'nome_categoria', 'nome_convenio',
                'nome_lado'
            ], 'safe'],
            [['nome_procedimento'], 'string', 'max' => 120],
            [['nome_convenio'], 'string', 'max' => 100],
            // [['cirurgico_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgendamentoCirurgico::className(), 'targetAttribute' => ['cirurgico_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'procedimento_id' => 'Procedimento ID',
            'nome_procedimento' => 'Nome Procedimento',
            'quantidade' => 'Quantidade',
            'convenio_id' => 'Convenio ID',
            'plano_id' => 'Plano ID',
            'nome_convenio' => 'Nome Convenio',
            'cirurgico_id' => 'Cirurgico ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCirurgico()
    {
        return $this->hasOne(AgendamentoCirurgico::class, ['id' => 'cirurgico_id']);
    }
}
