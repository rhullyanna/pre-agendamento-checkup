<?php

namespace app\modules\medico\models;

use yii\data\ActiveDataProvider;
use yii\db\Query;
use Yii;
use yii\data\SqlDataProvider;

/**
 * This is the model class for table "PESSOA_FISICA".
 *
 * @property string $CD_PESSOA_FISICA
 * @property int $IE_TIPO_PESSOA
 * @property string $NM_PESSOA_FISICA
 * @property string $DT_ATUALIZACAO
 * @property string $NM_USUARIO
 * @property string $DT_NASCIMENTO
 * @property string $IE_SEXO
 * @property string $IE_ESTADO_CIVIL
 * @property string $NR_CPF
 * @property string $NR_IDENTIDADE
 * @property string $NR_TELEFONE_CELULAR
 * @property int $IE_GRAU_INSTRUCAO
 * @property string $NR_CEP_CIDADE_NASC
 * @property int $NR_PRONTUARIO
 * @property int $CD_RELIGIAO
 * @property string $NR_PIS_PASEP
 * @property string $CD_NACIONALIDADE
 * @property string $IE_DEPENDENCIA_SUS
 * @property string $QT_ALTURA_CM
 * @property string $IE_TIPO_SANGUE
 * @property string $IE_FATOR_RH
 * @property string $DT_OBITO
 * @property string $NR_ISS
 * @property string $NR_INSS
 * @property string $NR_CERT_NASC
 * @property int $CD_CARGO
 * @property string $DS_CODIGO_PROF
 * @property string $IE_FUNCIONARIO
 * @property int $NR_SEQ_COR_PELE
 * @property string $DS_ORGAO_EMISSOR_CI
 * @property int $NR_CARTAO_NAC_SUS_ANT
 * @property int $CD_CBO_SUS
 * @property int $CD_ATIVIDADE_SUS
 * @property int $IE_VINCULO_SUS
 * @property int $CD_ESTABELECIMENTO
 * @property string $CD_SISTEMA_ANT
 * @property string $CD_FUNCIONARIO
 * @property string $NR_PAGER_BIP
 * @property string $NR_TRANSACAO_SUS
 * @property string $CD_MEDICO
 * @property string $NM_PESSOA_PESQUISA
 * @property string $DT_EMISSAO_CI
 * @property int $NR_SEQ_CONSELHO
 * @property string $DT_ADMISSAO_HOSP
 * @property string $IE_FLUENCIA_PORTUGUES
 * @property string $NR_TITULO_ELEITOR
 * @property string $NR_ZONA
 * @property string $NR_SECAO
 * @property string $NR_CARTAO_ESTRANGEIRO
 * @property string $NR_REG_GERAL_ESTRANG
 * @property string $DT_CHEGADA_BRASIL
 * @property string $NM_USUARIO_ORIGINAL
 * @property string $DS_HISTORICO
 * @property string $DT_CADASTRO_ORIGINAL
 * @property string $DT_ATUALIZACAO_NREC
 * @property string $NM_USUARIO_NREC
 * @property int $IE_TIPO_PRONTUARIO
 * @property string $DS_OBSERVACAO
 * @property int $QT_DEPENDENTE
 * @property int $NR_TRANSPLANTE
 * @property int $CD_EMPRESA
 * @property string $SG_EMISSORA_CI
 * @property string $DT_NATURALIZACAO_PF
 * @property int $NR_CTPS
 * @property string $NR_SERIE_CTPS
 * @property string $UF_EMISSORA_CTPS
 * @property string $DT_EMISSAO_CTPS
 * @property string $NM_PESSOA_FISICA_SEM_ACENTO
 * @property string $DT_REVISAO
 * @property string $NM_USUARIO_REVISAO
 * @property string $DT_DEMISSAO_HOSP
 * @property string $NR_CONTRA_REF_SUS
 * @property int $CD_PUERICULTURA
 * @property string $NR_PORTARIA_NAT
 * @property string $DS_SENHA
 * @property string $DS_FONETICA
 * @property string $IE_REVISAR
 * @property string $CD_CNES
 * @property int $NR_SEQ_PERFIL
 * @property int $NR_SAME
 * @property int $NR_SEQ_CBO_SAUDE
 * @property string $DT_INTEGRACAO_EXTERNA
 * @property string $DS_FONETICA_CNS
 * @property string $DT_GERACAO_PRONT
 * @property string $DS_APELIDO
 * @property string $CD_MUNICIPIO_IBGE
 * @property int $NR_SEQ_PAIS
 * @property string $NR_CERT_CASAMENTO
 * @property int $NR_SEQ_CARTORIO_NASC
 * @property int $NR_SEQ_CARTORIO_CASAMENTO
 * @property string $DT_EMISSAO_CERT_NASC
 * @property string $DT_EMISSAO_CERT_CASAMENTO
 * @property int $NR_LIVRO_CERT_NASC
 * @property int $NR_LIVRO_CERT_CASAMENTO
 * @property int $NR_FOLHA_CERT_NASC
 * @property int $NR_FOLHA_CERT_CASAMENTO
 * @property int $NR_SEQ_NUT_PERFIL
 * @property string $NR_REGISTRO_PLS
 * @property string $DT_VALIDADE_RG
 * @property string $QT_PESO_NASC
 * @property string $UF_CONSELHO
 * @property string $IE_STATUS_EXPORTAR
 * @property int $IE_ENDERECO_CORRESPONDENCIA
 * @property string $NR_PRONT_DV
 * @property string $NM_ABREVIADO
 * @property string $NR_DDD_CELULAR
 * @property string $NR_DDI_CELULAR
 * @property string $IE_FREQUENTA_ESCOLA
 * @property int $NR_CCM
 * @property string $DT_FIM_EXPERIENCIA
 * @property string $DT_VALIDADE_CONSELHO
 * @property string $IE_NF_CORREIO
 * @property string $DS_PROFISSAO
 * @property string $DS_EMPRESA_PF
 * @property string $NR_PASSAPORTE
 * @property int $CD_TIPO_PJ
 * @property string $NR_CNH
 * @property string $NR_CERT_MILITAR
 * @property int $CD_PERFIL_ATIVO
 * @property string $NR_PRONT_EXT
 * @property string $DT_INICIO_OCUP_ATUAL
 * @property string $IE_ESCOLARIDADE_CNS
 * @property string $IE_SITUACAO_CONJ_CNS
 * @property int $NR_FOLHA_CERT_DIV
 * @property string $NR_CERT_DIVORCIO
 * @property int $NR_SEQ_CARTORIO_DIVORCIO
 * @property string $DT_EMISSAO_CERT_DIVORCIO
 * @property int $NR_LIVRO_CERT_DIVORCIO
 * @property string $DT_ALTA_INSTITUCIONAL
 * @property string $DT_TRANSPLANTE
 * @property int $CD_FAMILIA
 * @property string $NR_MATRICULA_NASC
 * @property string $IE_DOADOR
 * @property string $DT_VENCIMENTO_CNH
 * @property string $DS_CATEGORIA_CNH
 * @property string $CD_PESSOA_MAE
 * @property string $DT_PRIMEIRA_ADMISSAO
 * @property string $DT_FIM_PRORROGACAO
 * @property string $NR_CERTIDAO_OBITO
 * @property int $NR_SEQ_ETNIA
 * @property string $NR_CARTAO_NAC_SUS
 * @property string $QT_PESO
 * @property string $IE_EMANCIPADO
 * @property int $IE_VINCULO_PROFISSIONAL
 * @property string $CD_NIT
 * @property string $IE_DEPENDENTE
 * @property string $DS_EMAIL_CCIH
 * @property string $CD_CGC_ORIG_TRANSPL
 * @property string $DT_AFASTAMENTO
 * @property int $NR_SEQ_TIPO_BENEFICIO
 * @property int $NR_SEQ_TIPO_INCAPACIDADE
 * @property string $IE_TRATAMENTO_PSIQUIATRICO
 * @property int $NR_SEQ_AGENCIA_INSS
 * @property int $QT_DIAS_IG
 * @property int $QT_SEMANAS_IG
 * @property string $IE_REGRA_IG
 * @property string $DT_NASCIMENTO_IG
 * @property string $IE_STATUS_USUARIO_EVENT
 * @property string $CD_CID_DIRETA
 * @property string $IE_COREN
 * @property string $DT_VALIDADE_COREN
 * @property string $NM_SOCIAL
 * @property int $NR_SEQ_COR_OLHO
 * @property int $NR_SEQ_COR_CABELO
 * @property string $DT_CAD_SISTEMA_ANT
 * @property string $IE_PERM_SMS_EMAIL
 * @property int $NR_SEQ_CLASSIF_PAC_AGE
 * @property string $NR_INSCRICAO_ESTADUAL
 * @property string $IE_SOCIO
 * @property int $CD_ULT_PROFISSAO
 * @property string $IE_VEGETARIANO
 * @property string $DS_ORIENTACAO_COBRANCA
 * @property string $IE_CONSELHEIRO
 * @property string $IE_FUMANTE
 * @property string $NR_CODIGO_SERV_PREST
 * @property string $DT_ADOCAO
 * @property int $NR_CELULAR_NUMEROS
 * @property string $DS_LAUDO_ANAT_PATOL
 * @property string $DT_LAUDO_ANAT_PATOL
 * @property string $NR_RIC
 * @property string $IE_CONSISTE_NR_SERIE_NF
 * @property string $NR_RGA
 * @property int $CD_PESSOA_CROSS
 * @property string $CD_BARRAS_PESSOA
 * @property int $NR_SEQ_FUNCAO_PF
 * @property string $NM_USUARIO_PRINC_CI
 * @property int $NR_SEQ_TURNO_TRABALHO
 * @property int $NR_SEQ_CHEFIA
 * @property string $CD_DECLARACAO_NASC_VIVO
 * @property string $NR_TERMO_CERT_NASC
 * @property string $CD_CURP
 * @property string $CD_RFC
 * @property string $SG_ESTADO_NASC
 * @property string $IE_FORNECEDOR
 * @property string $CD_IFE
 * @property string $NM_PRIMEIRO_NOME
 * @property string $NM_SOBRENOME_PAI
 * @property string $NM_SOBRENOME_MAE
 * @property string $IE_RH_FRACO
 * @property string $DS_MUNICIPIO_NASC_ESTRANGEIRO
 * @property string $IE_GEMELAR
 * @property int $IE_SUBTIPO_SANGUINEO
 * @property int $NR_SEQ_PERSON_NAME
 * @property int $NR_SEQ_FORMA_TRAT
 * @property string $IE_TIPO_DEFINITIVO_PROVISORIO
 * @property string $NR_INSCRICAO_MUNICIPAL
 * @property string $NR_SPSS
 * @property int $NR_SEQ_LINGUA_INDIGENA
 * @property int $NR_SEQ_NOME_SOLTEIRO
 * @property string $IE_CONSIDERA_INDIO
 * @property string $IE_NASC_ESTIMADO
 * @property string $IE_OCUPACAO_HABITUAL
 * @property string $IE_UNID_MED_PESO
 * @property string $QT_PESO_UM
 */
class PessoaFisicaTasy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PESSOA_FISICA';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CD_PESSOA_FISICA' => 'Cd Pessoa Fisica',
            'NM_PESSOA_FISICA' => 'Nome',
            'NR_CPF' => 'CPF',
            'NR_IDENTIDADE' => 'Nr Identidade',
        ];
    }

    public function searchPatient($params)
    {
        if (empty($params) || $params['NM_PESSOA_FISICA'] == '') {
            $query = new Query();
            return new ActiveDataProvider([
                'db' => \Yii::$app->db2,
                'query' => $query,
                'totalCount' => 0
            ]);
        }

        $count = Yii::$app->db2->createCommand(
            'SELECT COUNT(*) FROM PESSOA_FISICA WHERE 
                IE_TIPO_PESSOA = :tipo 
                AND NM_PESSOA_FISICA LIKE lower(:nm_pessoa_fisica)',
            [':tipo' => 2, ':nm_pessoa_fisica' => "%{$params['NM_PESSOA_FISICA']}%"]
        )->queryScalar();

        return new SqlDataProvider([
            'db' => \Yii::$app->db2,
            'sql' => "SELECT CD_PESSOA_FISICA, NM_PESSOA_FISICA, NR_CPF FROM PESSOA_FISICA 
                        WHERE IE_TIPO_PESSOA = :tipo 
                        AND lower(NM_PESSOA_FISICA) LIKE lower(:nm_pessoa_fisica)
                        ORDER BY NM_PESSOA_FISICA",
            'params' => [':tipo' => 2, ':nm_pessoa_fisica' => "%{$params['NM_PESSOA_FISICA']}%"],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    public function searchAnesthesist($params)
    {
        if (empty($params) || $params['NM_PESSOA_FISICA'] == '') {
            $query = new Query();
            return new ActiveDataProvider([
                'db' => \Yii::$app->db2,
                'query' => $query,
                'totalCount' => 0
            ]);
        }

        $count = Yii::$app->db2->createCommand(
            "SELECT COUNT(*) 
                FROM PESSOA_FISICA A,
                        MEDICO B
                        WHERE 1=1
                        AND A.CD_PESSOA_FISICA = B.CD_PESSOA_FISICA
                        AND	B.IE_SITUACAO = 'A'
                        AND IE_CORPO_CLINICO <> ' X'
                        AND IE_CORPO_ASSIST <> ' X'
                        AND IE_TIPO_PESSOA = :tipo
                        AND lower(NM_PESSOA_FISICA) LIKE lower(:nm_pessoa_fisica)",
            [':tipo' => 2, ':nm_pessoa_fisica' => "%{$params['NM_PESSOA_FISICA']}%"]
        )->queryScalar();

        return new SqlDataProvider([
            'db' => \Yii::$app->db2,
            'sql' => "SELECT B.NR_CRM, A.CD_PESSOA_FISICA, A.NM_PESSOA_FISICA
                        FROM PESSOA_FISICA A,
                        MEDICO B
                        WHERE 1=1
                        AND A.CD_PESSOA_FISICA = B.CD_PESSOA_FISICA
                        AND	B.IE_SITUACAO = 'A'
                        AND IE_CORPO_CLINICO <> ' X'
                        AND IE_CORPO_ASSIST <> ' X'
                        AND IE_TIPO_PESSOA = :tipo
                        AND lower(NM_PESSOA_FISICA) LIKE lower(:nm_pessoa_fisica)
                        ORDER BY NM_PESSOA_FISICA",
            'params' => [':tipo' => 2, ':nm_pessoa_fisica' => "%{$params['NM_PESSOA_FISICA']}%"],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }

    public function searchDoctor($params)
    {
        if (empty($params) || $params['NM_PESSOA_FISICA'] == '') {
            $query = new Query();
            return new ActiveDataProvider([
                'db' => \Yii::$app->db2,
                'query' => $query,
                'totalCount' => 0
            ]);
        }

        $count = Yii::$app->db2->createCommand(
            "SELECT COUNT(*) 
                FROM PESSOA_FISICA A,
                        MEDICO B
                        WHERE 1=1
                        AND A.CD_PESSOA_FISICA = B.CD_PESSOA_FISICA
                        AND	B.IE_SITUACAO = 'A'
                        AND IE_CORPO_CLINICO <> ' X'
                        AND IE_CORPO_ASSIST <> ' X'
                        AND IE_TIPO_PESSOA = :tipo
                        AND lower(NM_PESSOA_FISICA) LIKE lower(:nm_pessoa_fisica)",
            [':tipo' => 2, ':nm_pessoa_fisica' => "%{$params['NM_PESSOA_FISICA']}%"]
        )->queryScalar();

        return new SqlDataProvider([
            'db' => \Yii::$app->db2,
            'sql' => "SELECT B.NR_CRM, A.CD_PESSOA_FISICA, A.NM_PESSOA_FISICA, A.NR_CPF
                        FROM PESSOA_FISICA A,
                        MEDICO B
                        WHERE 1=1
                        AND A.CD_PESSOA_FISICA = B.CD_PESSOA_FISICA
                        AND	B.IE_SITUACAO = 'A'
                        AND IE_CORPO_CLINICO <> ' X'
                        AND IE_CORPO_ASSIST <> ' X'
                        AND IE_TIPO_PESSOA = :tipo
                        AND lower(NM_PESSOA_FISICA) LIKE lower(:nm_pessoa_fisica)
                        ORDER BY NM_PESSOA_FISICA",
            'params' => [':tipo' => 2, ':nm_pessoa_fisica' => "%{$params['NM_PESSOA_FISICA']}%"],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }
}
