<?php

namespace app\modules\medico\models;

use yii\data\ActiveDataProvider;
use yii\db\Query;
use Yii;
use yii\data\SqlDataProvider;

class ProcedimentoTasy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PROC_INTERNO';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    public function search($params)
    {
        if (empty($params) || $params['DS_PROC_EXAME'] == '') {
            $query = new Query();
            return new ActiveDataProvider([
                'db' => \Yii::$app->db2,
                'query' => $query,
                'totalCount' => 0
            ]);
        }

        $count = Yii::$app->db2->createCommand(
            "SELECT COUNT(*)
                FROM PROC_INTERNO 
                WHERE lower(DS_PROC_EXAME) LIKE lower(:procedimento) 
                AND CD_PROCEDIMENTO_TUSS IS NOT NULL",
            [':procedimento' => "%{$params['DS_PROC_EXAME']}%"]
        )->queryScalar();

        return new SqlDataProvider([
            'db' => \Yii::$app->db2,
            'sql' => "SELECT CD_PROCEDIMENTO_TUSS, NR_SEQUENCIA, DS_PROC_EXAME 
                        FROM PROC_INTERNO 
                        WHERE lower(DS_PROC_EXAME) LIKE lower(:procedimento) 
                        AND CD_PROCEDIMENTO_TUSS IS NOT NULL
                        ORDER BY DS_PROC_EXAME",
            'params' => [':procedimento' => "%{$params['DS_PROC_EXAME']}%"],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }
}
