<?php

namespace app\modules\medico\models;

use Yii;

/**
 * This is the model class for table "cirurgico_material".
 *
 * @property int $id
 * @property int $material_id
 * @property string $descricao
 * @property int $cirurgico_id
 */
class CirurgicoMaterial extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cirurgico_material';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_id', 'descricao', 'cirurgico_id'], 'required'],
            [['material_id', 'cirurgico_id'], 'integer'],
            [['descricao'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'descricao' => 'Material Descricao',
            'cirurgico_id' => 'Cirurgico ID',
        ];
    }
}
