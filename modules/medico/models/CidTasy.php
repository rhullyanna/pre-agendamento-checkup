<?php

namespace app\modules\medico\models;

use yii\data\ActiveDataProvider;
use yii\db\Query;
use Yii;
use yii\data\SqlDataProvider;

class CidTasy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CID_DOENCA';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    public function search($params)
    {
        if (empty($params) || $params['DS_DOENCA_CID'] == '') {
            $query = new Query();
            return new ActiveDataProvider([
                'db' => \Yii::$app->db2,
                'query' => $query,
                'totalCount' => 0
            ]);
        }

        $count = Yii::$app->db2->createCommand(
            "SELECT CD_DOENCA_CID, DS_DOENCA_CID FROM CID_DOENCA
            WHERE lower(DS_DOENCA_CID) LIKE lower(:cid)
            ORDER BY DS_DOENCA_CID",
            [':cid' => "%{$params['DS_DOENCA_CID']}%"]
        )->queryScalar();

        return new SqlDataProvider([
            'db' => \Yii::$app->db2,
            'sql' => "SELECT CD_DOENCA_CID, DS_DOENCA_CID FROM CID_DOENCA
                        WHERE lower(DS_DOENCA_CID) LIKE lower(:cid)
                        ORDER BY DS_DOENCA_CID",
            'params' => [':cid' => "%{$params['DS_DOENCA_CID']}%"],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }
}
