<?php

namespace app\modules\medico\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\medico\models\AgendamentoCirurgico;

/**
 * AgendamentoCirurgicoSearch represents the model behind the search form of `app\modules\medicos\models\AgendamentoCirurgico`.
 */
class AgendamentoCirurgicoSearch extends AgendamentoCirurgico
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'procedimento_id', 'carater_id'], 'integer'],
            [['nome_convenio', 'numero_convenio'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgendamentoCirurgico::find()->orderBy([
            'id' => SORT_DESC,
        ]);;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'procedimento_id' => $this->procedimento_id,
            'carater_id' => $this->carater_id,
        ]);

        $query->andFilterWhere(['like', 'nome_convenio', $this->nome_convenio]);

        return $dataProvider;
    }
}
