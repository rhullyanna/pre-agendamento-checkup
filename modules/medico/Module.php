<?php

namespace app\modules\medico;

use Yii;
use yii\helpers\Url;
use yii\web\ErrorHandler;

/**
 * medico module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\medico\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->layout = '@app/modules/medico/views/layouts/main';
        Yii::$app->user->loginUrl = ['/medico/default/login'];
        Yii::$app->setHomeUrl(Url::to(['/medico/default/index']));

        Yii::configure($this, [
            'components' => [
                'errorHandler' => [
                    'class' => ErrorHandler::className(),
                    'errorAction' => '/medico/default/error',
                ]
            ],
        ]);

        $handler = $this->get('errorHandler');
        Yii::$app->set('errorHandler', $handler);
        $handler->register();
    }
}
