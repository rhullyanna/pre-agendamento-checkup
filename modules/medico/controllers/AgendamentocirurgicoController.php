<?php

namespace app\modules\medico\controllers;

use app\components\URLify;
use app\models\LogAcaoEnum;
use Yii;
use app\modules\medico\models\AgendamentoCirurgico;
use app\modules\medico\models\AgendamentoCirurgicoSearch;
use app\models\PermissaoEnum;
use app\models\StatusEnum;
use app\modules\admin\models\Medico;
use app\modules\medico\models\CidTasy;
use app\modules\medico\models\CirurgicoDocumento;
use app\modules\medico\models\CirurgicoMaterial;
use app\modules\medico\models\CirurgicoProcedimento;
use app\modules\medico\models\PessoaFisicaTasy;
use app\modules\medico\models\ProcedimentoTasy;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * AgendamentoCirurgicoController implements the CRUD actions for AgendamentoCirurgico model.
 */
class AgendamentocirurgicoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'novo', 'view', 'atualizar', 'findmodel', 'index', 'validatepostdata', 'loaddoctordata', 'cancelar',
                            'savefile', 'setfiledeleted', 'getfilelist', 'searchpatient', 'searchpatientbyparams', 'searchprocedure',
                            'searchprocedurebyparams', 'searchprocedureadd', 'searchprocedureaddbyparams', 'searchdoctor', 'searchdoctorbyparams'
                        ],
                        'roles' => [PermissaoEnum::MEDICO],
                    ],

                ],
            ],
        ];
    }

    /**
     * Lists all AgendamentoCirurgico models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgendamentoCirurgicoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AgendamentoCirurgico model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AgendamentoCirurgico model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNovo()
    {
        $model = new AgendamentoCirurgico();

        if ($model->load(Yii::$app->request->post())) {

            $trans = Yii::$app->db->beginTransaction();
            try {
                $model->save();

                CirurgicoMaterial::deleteAll(['cirurgico_id' => $model->id]);

                $materiais = json_decode($model->materiais);
                if ($materiais != null) {
                    foreach ($materiais as $material) {
                        $matObj = json_decode($material, true);
                        $mat = new CirurgicoMaterial();
                        $mat->descricao = $matObj['descricao'];
                        $mat->material_id = $matObj['id'];
                        $mat->cirurgico_id = $model->id;
                        $mat->save(false);
                    }
                }

                $model->verifyTempPathUpload();


                if ($_SESSION['files'] != null) {
                    foreach ($_SESSION['files'] as $doc) {
                        $doc->cirurgico_id = $model->id;
                        $doc->save(false);
                    }
                    rename(Yii::getAlias('@uploads/cirurgico/') . $doc->url, Yii::getAlias('@uploads/cirurgico/') . $model->id . '/' . $doc->url);
                }

                Yii::$app->session->setFlash('success', 'Agendamento cadastro');

                $trans->commit();
                return $this->redirect(['view', 'id' => $model->id]);
            } catch (\Exception $e) {
                $trans->rollBack();
                throw $e;
            }
        } else {
            unset($_SESSION['files']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AgendamentoCirurgico model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAtualizar($id)
    {
        $model = $this->findModel($id);
        $arquivos = $model->arquivos;

        if ($model->load(Yii::$app->request->post())) {

            $trans = Yii::$app->db->beginTransaction();
            try {
                $model->save();

                CirurgicoMaterial::deleteAll(['cirurgico_id' => $model->id]);
                $materiais = json_decode($model->materiais);
                foreach ($materiais as $material) {
                    $matObj = json_decode($material, true);
                    $mat = new CirurgicoMaterial();
                    $mat->descricao = $matObj['descricao'];
                    $mat->material_id = $matObj['id'];
                    $mat->cirurgico_id = $model->id;
                    $mat->save(false);
                }

                $model->verifyPathUpload();

                if ($_SESSION['files'] != null) {
                    foreach ($_SESSION['files'] as $doc) {
                        if ($doc->excluir) {
                            if (is_file(Yii::getAlias('@uploads/cirurgico/') . $model->id . '/' . $doc->url)) {
                                unlink(Yii::getAlias('@uploads/cirurgico/') . $model->id . '/' . $doc->url);
                            }
                            $doc->delete();
                        } else {
                            $doc->cirurgico_id = $model->id;
                            $doc->save(false);
                        }
                    }
                }

                Yii::$app->session->setFlash('success', 'Agendamento atualizado');
                $trans->commit();

                return $this->redirect(['view', 'id' => $model->id]);
            } catch (\Exception $e) {
                $trans->rollBack();
                throw $e;
            }
        } else {
            unset($_SESSION['files']);
            $this->loadFiles($arquivos);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AgendamentoCirurgico model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCancelar($id)
    {
        $trans = Yii::$app->db->beginTransaction();
        try {
            $model = $this->findModel($id);
            $model->setScenario(AgendamentoCirurgico::SCENARIO_CANCEL);
            $model->getLog()->acao = LogAcaoEnum::CANCEL;
            $model->status = StatusEnum::CANCELADO;
            $model->save(false);

            Yii::$app->session->setFlash('error', "Pré-agendamento cancelado!");
            $trans->commit();
            return $this->redirect('index');
        } catch (\Exception $e) {
            $trans->rollBack();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Finds the AgendamentoCirurgico model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AgendamentoCirurgico the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AgendamentoCirurgico::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionLoaddoctordata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $doctor = Medico::findOne(['cpf' => Yii::$app->user->identity->username]);
        $doctorData = [
            'id' => $doctor->id,
            'nome' => $doctor->nome,
            'crm' => $doctor->crm,
        ];
        return $doctorData;
    }

    /**
     * Efetua o upload de arquivos de um pré-agendamento cirúrgico e, para cada um, cria um objeto e armazena em uma sessão 
     *
     * @return mixed
     */
    public function actionSavefile()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $response = ['success' => 0, 'errors' => null];

        $doc = new CirurgicoDocumento();

        $doc->verifyTempPathUpload();

        $doc->load(Yii::$app->request->post());
        $doc->arquivo = UploadedFile::getInstance($doc, 'arquivo');
        if ($doc->validate()) {
            if ($doc->arquivo != null) {
                $arquivo_nome = substr(URLify::filter($doc->arquivo->baseName), 0, -3) .  '_' . strtotime("now") . '_.' . $doc->arquivo->extension;
                // $path = Yii::getAlias('@uploads/cirurgico/') . $arquivo_nome;
                $path = Yii::getAlias('@uploads/cirurgico/') . $arquivo_nome;
                $doc->arquivo->saveAs($path);
                $doc->url = $arquivo_nome;
            }

            $response['success'] = 1;
            $this->updateFileList($doc);
        } else {
            $response['errors'] = $doc->getErrors();
        }

        return $response;
    }


    public function actionSearchpatient()
    {
        $this->layout = 'modal';
        $model = new PessoaFisicaTasy();
        $dataProvider = $model->searchPatient(Yii::$app->request->queryParams);
        return $this->render('_search_patient', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchpatientbyparams()
    {
        $this->layout = 'modal';
        $model = new PessoaFisicaTasy();
        $dataProvider = $model->searchPatient(Yii::$app->request->queryParams);

        return $this->renderAjax('_search_patient', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchprocedure()
    {
        $this->layout = 'modal';
        $model = new ProcedimentoTasy();
        $dataProvider = $model->search(Yii::$app->request->queryParams);
        return $this->render('_search_procedure', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchprocedurebyparams()
    {
        $this->layout = 'modal';
        $model = new ProcedimentoTasy();
        $dataProvider = $model->search(Yii::$app->request->queryParams);
        return $this->render('_search_procedure', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchprocedureadd()
    {
        $this->layout = 'modal';
        $model = new ProcedimentoTasy();
        $dataProvider = $model->search(Yii::$app->request->queryParams);
        return $this->render('_search_procedure_add', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchprocedureaddbyparams()
    {
        $this->layout = 'modal';
        $model = new ProcedimentoTasy();
        $dataProvider = $model->search(Yii::$app->request->queryParams);
        return $this->render('_search_procedure_add', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchdoctor()
    {
        $this->layout = 'modal';
        $model = new PessoaFisicaTasy();
        $dataProvider = $model->searchDoctor(Yii::$app->request->queryParams);
        return $this->render('_search_doctor', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchdoctorbyparams()
    {
        $this->layout = 'modal';
        $model = new PessoaFisicaTasy();
        $dataProvider = $model->searchDoctor(Yii::$app->request->queryParams);

        return $this->renderAjax('_search_doctor', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionValidatepostdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = ['success' => 0, 'errors' => null, 'id' => null];
        $model = new AgendamentoCirurgico();
        $model->setScenario(AgendamentoCirurgico::SCENARIO_DEFAULT);

        $trans = Yii::$app->db->beginTransaction();
        try {
            $model->load(Yii::$app->request->post());

            if ($model->validate()) {
                $model->sendInfoCheckup();
                $model->save(false);

                $procedimentos = json_decode($model->procedimentos, true);
                if ($procedimentos != null) {
                    CirurgicoProcedimento::deleteAll(['cirurgico_id' => $model->id]);
                    foreach ($procedimentos as $proc) {
                        // $procObj = json_decode($proc, true);
                        $cp = new CirurgicoProcedimento();
                        $cp->attributes = $proc;
                        $cp->cirurgico_id = $model->id;
                        $cp->nr_sequencia = $model->nr_seq_agenda;
                        $cp->cd_pessoa_fisica = $model->cd_pessoa_fisica;
                        $model->sendAddProcedure($cp);
                        $cp->save(false);
                    }
                }

                $model->verifyPathUpload();

                $model->documentos = UploadedFile::getInstances($model, 'documentos');

                foreach ($model->documentos as $doc) {
                    $arquivo_nome = substr(URLify::filter($doc->baseName), 0, -3) . '.' . $doc->extension;
                    $path = Yii::getAlias('@uploads/cirurgico/') . $model->id . '/' . $arquivo_nome;
                    $doc->saveAs($path);
                    $document  = new CirurgicoDocumento();
                    $document->url = $arquivo_nome;
                    $document->cirurgico_id = $model->id;
                    $document->save(false);
                }

                $response['success'] = 1;
                $response['id'] = $model->id;
                Yii::$app->session->setFlash('success', "Pré-agendamento efetuado com sucesso.");
            } else {
                $response['errors'] =  $model->errors;
            }
            $trans->commit();
        } catch (\Exception $e) {
            $response['success'] = 0;
            $model->rollBackCheckupInfo();
            $trans->rollBack();
            $msg = $e->getMessage();
            $inicio = "ORA-";
            $fim = "#";
            if (preg_match("/{$inicio}([^#]+){$fim}/", $msg, $match) == 1) {
                $msg = $match[1];
            }
            if ($msg == '') {
                $msg = "Não foi possível marcar neste horário, por favor, selecione outro.";
            }
            $response['errors'] = ['msg' => [$msg]];
        }
        return $response;
    }
}
