<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Administrador */

$this->title = 'Cadastrar Médico';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cadastro-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>