<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Administrador */

$this->title = 'Atualizar Médico #' . $model->id;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cadastro-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('../../../admin/views/medico/_form', [
        'model' => $model,
    ]) ?>

</div>