<?php

/* @var $this yii\web\View */

$this->title = 'Sistema de Pré-agendamento Checkup';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Bem vindo ao Sistema de Pré-agendamento do Hospital Checkup!</h1>

        <p class="lead">Escolha o serviço desejado abaixo.</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="panel panel-index medico">
                    <div class="panel-heading">
                        <h3>Cirúrgico</h3>
                    </div>
                    <div class="panel-body">
                        <p>Agendamento de centro cirúrgico</p>
                        <p><a class="btn btn-default" href="/medico/agendamentocirurgico/index">Agendar &raquo;</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
            </div>
        </div>
    </div>
</div>