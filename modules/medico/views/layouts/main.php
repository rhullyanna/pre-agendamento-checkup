<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => '<div class="pull-left navbar-logo"><img height="100" width="300" src="' . Yii::$app->request->baseUrl . '/images/logo-checkup.png" alt="Check Up"></div>',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'sys-navbar navbar-fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-left'],
            'items' => [
                ['label' => 'Home', 'url' => ['/medico/default/index']],
                ['label' => 'Cirúrgico', 'url' => ['/medico/agendamentocirurgico/index']],
                ['label' => 'Alterar Senha', 'url' => ['/medico/cadastro/alterarsenha'], 'visible' => !Yii::$app->user->isGuest],
                Yii::$app->user->isGuest ? (['label' => 'Login', 'url' => ['/medico/default/login']]) : ('<li>'
                    . Html::beginForm(['/medico/default/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->name . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>')
            ],
        ]);
        NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Copyright – Check Up Hospital - CNPJ: 05460308/0001-33</p>
            <p class="pull-right"><?= Yii::powered() ?> por Rise</p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>