<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\medicos\models\AgendamentoCirurgicoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agendamento-cirurgico-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'medico_id') ?>

    <?= $form->field($model, 'paciente_id') ?>

    <?= $form->field($model, 'nome_convenio') ?>

    <?= $form->field($model, 'numero_convenio') ?>

    <?php // echo $form->field($model, 'procedimento_id') ?>

    <?php // echo $form->field($model, 'carater_id') ?>

    <?php // echo $form->field($model, 'cid_id') ?>

    <?php // echo $form->field($model, 'doc_identificacao') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
