<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\medicos\models\AgendamentoCirurgico */

$this->title = 'Visualizar Pré-Agendamento #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pŕe-Agendamentos Cirúrgicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="agendamento-cirurgico-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <fieldset>
        <legend>Dados Gerais</legend>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'nome_procedimento',
                'nome_carater',
                'horario',
                'nome_paciente',
                'nome_convenio',
                'nome_plano',
                'observacao'
            ],
        ]) ?>
    </fieldset>

    <fieldset>
        <legend>Documentação</legend>
        <?php foreach ($model->documentoscirurgico as $documento) : ?>
            <div class="col-lg-4 col-sm-12">
                <label><?= $documento->url; ?></label>
                <div class="img-document-cover">
                    <img src="<?= $documento->getPath(); ?>" class="img-document" />
                </div>
            </div>

        <?php endforeach; ?>
    </fieldset>

</div>