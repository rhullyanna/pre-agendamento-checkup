<?php

use app\assets\CirurgicoAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use \yii\bootstrap\Modal;
use yii\web\View;

CirurgicoAsset::register($this);

$this->registerJs("

    $(function(){
        $('.modalButtonPatient').click(function (){
                $.get($(this).attr('href'), function(data) {
                $('#modalPatient').modal('show').find('#modalContentPatient').html(data)
            });
            return false;
        });

        $('.modalButtonProcedure').click(function (){
                $.get($(this).attr('href'), function(data) {
                $('#modalProcedure').modal('show').find('#modalContentProcedure').html(data)
            });
            return false;
        });

        $('.modalButtonProcedureAdd').click(function (){
            $.get($(this).attr('href'), function(data) {
            $('#modalProcedureAdd').modal('show').find('#modalContentProcedureAdd').html(data)
            });
            return false;
        });

        $('.modalButtonDoctor').click(function (){
            $.get($(this).attr('href'), function(data) {
            $('#modalDoctor').modal('show').find('#modalContentDoctor').html(data)
            });
            return false;
        });

    }); 
");

$this->registerJs('
    function selectPatient(id, nome){
        $("#cd_pessoa_fisica").val(id);
        $("#nome_paciente").val(nome);
        $("#modalPatient").modal("hide");
    };

    function selectProcedure(proc_tuss_id, proc_interno, nome){
        $("#procedimento_id").val(proc_tuss_id);
        $("#nr_seq_proc_interno").val(proc_interno);
        $("#nome_procedimento").val(nome);
        $("#modalProcedure").modal("hide");
    };

    function selectProcedureAdd(proc_tuss_id, proc_interno, nome){
        $("#procedimento_adicional_id").val(proc_tuss_id)[0].dispatchEvent(new Event("input"));
        $("#nr_seq_proc_adicional").val(proc_interno)[0].dispatchEvent(new Event("input"));
        $("#nome_procedimento_adicional").val(nome)[0].dispatchEvent(new Event("input"));
        $("#modalProcedureAdd").modal("hide");
    };  

    function selectDoctor(id, nome){
        $("#cd_medico_adicional").val(id)[0].dispatchEvent(new Event("input"));
        $("#nome_medico_adicional").val(nome)[0].dispatchEvent(new Event("input"));
        $("#modalDoctor").modal("hide");
    };
', View::POS_END);

?>

<div class="agendamento-cirurgico-form">

    <div id="cirurgico">
        <?php $form = ActiveForm::begin([
            'id' => 'agendamento-cirurgico-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            // 'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

        <div v-cloak id="errorsForm" class="error-summary" v-show="erroForm">
            <p>Por favor, corrija os seguintes erros:</p>
            <ul v-for="e in errorsList">
                <li>{{ e[0] }}</li>
            </ul>
        </div>

        <?= $form->field($model, 'id')->hiddenInput(['id' => 'id'])->label(false) ?>
        <?= $form->field($model, 'cd_medico')->hiddenInput(['value' => Yii::$app->user->identity->roleId])->label(false); ?>


        <fieldset>
            <legend>Dados do Paciente</legend>
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="form-group required">
                        <?= Html::activeLabel($model, 'nome_paciente', ['class' => 'control-label']); ?>
                        <div class="input-group">
                            <input type="text" class="form-control" name="AgendamentoCirurgico[nome_paciente]" id="nome_paciente" aria-describedby="basic-addon1">
                            <span class="input-group-addon" id="basic-addon1">
                                <a class="modalButtonPatient" href="<?= Url::to(['searchpatient']); ?>">
                                    <i class="glyphicon glyphicon-search"></i>
                                </a>
                            </span>
                            <?= $form->field($model, 'cd_pessoa_fisica')->hiddenInput(['id' => 'cd_pessoa_fisica'])->label(false); ?>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-sm-12">
                    <div class="form-group required">
                        <?= Html::activeLabel($model, 'convenio_id', ['class' => 'control-label']); ?>
                        <select class="form-control" id="insuranceField" name="AgendamentoCirurgico[convenio_id]" v-model="insurance">
                            <option value=''>Selecione </option>
                            <option v-for='data in insuranceList' :value='data.id'>{{ data.descricao }}</option>
                        </select>
                        <?= $form->field($model, 'nome_convenio')->hiddenInput(['id' => 'nome_convenio'])->label(false); ?>
                    </div>
                </div>

                <div class="col-lg-6 col-sm-12">
                    <div class="form-group required">
                        <?= Html::activeLabel($model, 'cd_plano', ['class' => 'control-label']); ?>
                        <select class="form-control" name="AgendamentoCirurgico[cd_plano]" v-model="plan">
                            <option value=''>Selecione </option>
                            <option v-for='data in planList' :value='data.id'>{{ data.descricao }}</option>
                        </select>
                    </div>
                    <?= $form->field($model, 'nome_plano')->hiddenInput(['id' => 'nome_plano'])->label(false); ?>
                </div>
            </div>

        </fieldset>

        <fieldset>
            <legend>Procedimento</legend>
            <div class="row">
                <div class="col-lg-4 col-sm-12">
                    <div class="form-group">
                        <label>Médico</label>
                        <input type="text" class="form-control" name="AgendamentoCirurgico[nome_medico]" v-model="doctor.nome" readonly />
                    </div>
                </div>

                <div class="col-lg-4 col-sm-12">
                    <div class="form-group required">
                        <?= Html::activeLabel($model, 'carater_id', ['class' => 'control-label']); ?>
                        <select class="form-control" id="caraterField" name="AgendamentoCirurgico[carater_id]" v-model="carater">
                            <option value=''>Selecione </option>
                            <option v-for='data in caraterList' :value='data.id'>{{ data.descricao }}</option>
                        </select>
                    </div>
                    <?= $form->field($model, 'nome_carater')->hiddenInput(['id' => 'nome_carater'])->label(false); ?>

                </div>

                <div class="col-lg-4 col-sm-12">
                    <div class="form-group">
                        <?= Html::activeLabel($model, 'cd_acomodacao', ['class' => 'control-label']); ?>
                        <select class="form-control" id="acomField" name="AgendamentoCirurgico[cd_acomodacao]" v-model="acomodation">
                            <option value=''>Selecione </option>
                            <option v-for='data in acomodationList' :value='data.id'>{{ data.descricao }}</option>
                        </select>
                    </div>
                    <?= $form->field($model, 'nome_acomodacao')->hiddenInput(['id' => 'nome_acomodacao'])->label(false); ?>

                </div>

                <div class="col-lg-12 col-sm-12">
                    <div class="form-group required">
                        <?= Html::activeLabel($model, 'nome_procedimento', ['class' => 'control-label']); ?>
                        <div class="input-group">
                            <input type="text" readonly class="form-control" name="AgendamentoCirurgico[nome_procedimento]" id="nome_procedimento" aria-describedby="basic-addon3">
                            <span class="input-group-addon" id="basic-addon3">
                                <a class="modalButtonProcedure" href="<?= Url::to(['searchprocedure']); ?>">
                                    <i class="glyphicon glyphicon-search"></i>
                                </a>
                            </span>
                            <?= $form->field($model, 'procedimento_id')->hiddenInput(['id' => 'procedimento_id'])->label(false); ?>
                            <?= $form->field($model, 'nr_seq_proc_interno')->hiddenInput(['id' => 'nr_seq_proc_interno'])->label(false); ?>
                        </div>
                    </div>
                </div>

            </div>

        </fieldset>

        <fieldset>
            <legend>Procedimentos Adicionais</legend>
            <div v-cloak v-show="procError" class="error-summary">
                <span class="text-danger">{{ procErrorMessage }}</span>
            </div>
            <div class="row">

                <div class="col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label>Procedimento</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control" v-model='addedProcedure.nome_procedimento' id="nome_procedimento_adicional" aria-describedby="basic-addon5">
                            <span class="input-group-addon" id="basic-addon5">
                                <a class="modalButtonProcedureAdd" href="<?= Url::to(['searchprocedureadd']); ?>">
                                    <i class="glyphicon glyphicon-search"></i>
                                </a>
                            </span>
                            <input type="hidden" id="procedimento_adicional_id" v-model="addedProcedure.procedimento_id">
                            <input type="hidden" id="nr_seq_proc_adicional" v-model="addedProcedure.nr_seq_proc_interno">
                            <?= $form->field($model, 'procedimentos')->hiddenInput(['id' => 'procedimentos', 'v-model' => 'proceduresjson'])->label(false); ?>

                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label>Médico</label>
                        <div class="input-group">
                            <input type="text" readonly class="form-control" id="nome_medico_adicional" v-model='addedProcedure.nome_medico' aria-describedby="basic-addon7">
                            <span class="input-group-addon" id="basic-addon7">
                                <a class="modalButtonDoctor" href="<?= Url::to(['searchdoctor']); ?>">
                                    <i class="glyphicon glyphicon-search"></i>
                                </a>
                            </span>
                            <input type="hidden" id="cd_medico_adicional" v-model="addedProcedure.medico_id">
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-12">
                    <div class="form-group">
                        <label for="convenio_adicional">Convênio</label>
                        <select class="form-control" id="insuranceAddedField" v-model="addedProcedure.convenio_id" @change="loadAddedDependencies()">
                            <option value=''>Selecione </option>
                            <option v-for='data in addedInsuranceList' :value='data.id'>{{ data.descricao }}</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-12">
                    <div class="form-group">
                        <label for="plano_adicional">Plano</label>
                        <select class="form-control" v-model="addedProcedure.plano_id">
                            <option value=''>Selecione </option>
                            <option v-for='data in addedPlanList' :value='data.id'>{{ data.descricao }}</option>
                        </select>
                    </div>
                </div>


                <div class="col-lg-4 col-sm-12">
                    <div class="form-group">
                        <label for="qtd_adicional">Quantidade</label>
                        <input type="text" id="qtd_adicional" class="form-control" v-model="addedProcedure.quantidade" />
                    </div>
                </div>

                <div class="form-group text-center">
                    <button type="button" class="btn btn-primary" @click.prevent="addProcedure()">Adicionar</button>
                </div>

            </div>
            <table v-cloak class="table table-bordered table-centered" v-show="addedProcedures.length > 0">
                <thead>
                    <tr>
                        <th>Convênio</th>
                        <th>Procedimento</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(a, index) in addedProcedures">
                        <td class="info">{{ a.nome_convenio }}</td>
                        <td class="info">{{ a.nome_procedimento }}</td>
                        <td class="info">
                            <a class="glyphicon glyphicon-trash" href="javascript:;" title="Remover arquivo" v-on:click.prevent="removeProcedure(a.procedimento_id, index);"></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </fieldset>

        <fieldset>
            <legend>Informações Adicionais</legend>

            <?= $form->field($model, 'documentos[]')->fileInput(['multiple' => true, 'accept' => ['image/*', 'application/pdf']]); ?>

            <div class="form-group required">
                <?= $form->field($model, 'observacao')->textArea(['id' => 'observacao']); ?>
            </div>
        </fieldset>

        <div class="form-group text-center">
            <button type="button" class="btn btn-primary" @click.prevent="validate()">Pré-Agendar</button>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php Modal::begin([
    'header' => '<h3>Pesquisar Paciente</h3>',
    'id' => 'modalPatient',
    'size' => 'modal-lg',
]);

echo "<div id='modalContentPatient'></div>";

Modal::end(); ?>

<?php Modal::begin([
    'header' => '<h3>Pesquisar Procedimento</h3>',
    'id' => 'modalProcedure',
    'size' => 'modal-lg',
]);

echo "<div id='modalContentProcedure'></div>";

Modal::end(); ?>

<?php Modal::begin([
    'header' => '<h3>Pesquisar Procedimento Adicional</h3>',
    'id' => 'modalProcedureAdd',
    'size' => 'modal-lg',
]);

echo "<div id='modalContentProcedureAdd'></div>";

Modal::end(); ?>


<?php Modal::begin([
    'header' => '<h3>Pesquisar Médico</h3>',
    'id' => 'modalDoctor',
    'size' => 'modal-lg',
]);

echo "<div id='modalContentDoctor'></div>";

Modal::end(); ?>