<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\medicos\models\AgendamentoCirurgico */

$this->title = 'Atualizar Agendamento Cirúrgico #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gerenciar Agendamento Cirúrgico', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agendamento-cirurgico-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>