<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\modules\medico\models\AgendamentoCirurgico;
use app\models\StatusEnum;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\medicos\models\AgendamentoCirurgicoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pré-Agendamentos Cirúrgicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agendamento-cirurgico-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <p>
        <?= Html::a('Pré-Agendar', ['novo'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'options' => [
                'tag' => 'div',
                'class' => 'list-wrapper',
                'id' => 'list-wrapper',
            ],
            'layout' => "{summary}\n{items}\n{pager}",
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_list_item', ['model' => $model]);
            },
        ]);
    ?>
</div>