<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\web\View;

$this->registerJs(
    '
    $("#btn-search-c").on("click",function(e){ 
        e.preventDefault();

        let nome = $("#DS_DOENCA_CID").val();
        var url = new URL(
            $().getUrl() + "/medico/agendamentocirurgico/searchcidbyparams"
        );
        url.search = new URLSearchParams({
            "DS_DOENCA_CID": nome,
        });
        
        let loading = main.$loading.show();

        $.pjax.reload({
            container:"#c-grid", 
            url:url, 
            replace: false, 
            timeout: 20000, 
        }).always(()=>{
            loading.hide();
        });  
    });

    function updateSearchC(e){
        e.preventDefault();

        let loading = main.$loading.show();

        $.pjax.reload({
            container:"#c-grid", 
            url:e.target.href, 
            replace: false, 
            timeout: 20000, 
        }).always(()=>{
            loading.hide();
        });  
    }
',
    View::POS_END
);


/* @var $this yii\web\View */
/* @var $model app\modules\medicos\models\AgendamentoCirurgicoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cid-search">

    <div class="form-group">
        <label>Descrição</label>
        <input type="text" class="form-control" name="DS_DOENCA_CID" id="DS_DOENCA_CID" autocomplete="off">
    </div>

    <div class="form-group">
        <?= Html::button('Pesquisar', ['class' => 'btn btn-primary', 'id' => 'btn-search-c']) ?>
    </div>

    <?php Pjax::begin(['id' => 'c-grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'linkOptions' => ['data-pjax' => '0', 'onclick' => 'updateSearchC(event);']
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Nome',
                'value' => function ($data) {
                    return $data['DS_DOENCA_CID'];
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Ações',
                'template' => '{select}',
                'buttons'  => [
                    'select' => function ($url, $model) {
                        return Html::button('Selecionar', [
                            'id' => $model['CD_DOENCA_CID'],
                            'class' => 'btn btn-primary',
                            'onclick' => "window.parent.selectCID('" . $model['CD_DOENCA_CID'] . "', '" . $model['DS_DOENCA_CID'] . "');"
                        ]);
                    },
                ],
            ]
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>