<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\web\View;

$this->registerJs(
    '
    $("#btn-search-p").on("click",function(e){ 
        e.preventDefault();

        let nome = $("#DS_PROC_EXAME").val();
        var url = new URL(
            $().getUrl() + "/medico/agendamentocirurgico/searchprocedurebyparams"
        );
        url.search = new URLSearchParams({
            "DS_PROC_EXAME": nome,
        });
        
        let loading = main.$loading.show();

        $.pjax.reload({
            container:"#p-grid", 
            url:url, 
            replace: false, 
            timeout: 20000, 
        }).always(()=>{
            loading.hide();
        });  
    });

    function updateSearchP(e){
        e.preventDefault();
        let loading = main.$loading.show();

        $.pjax.reload({
            container:"#p-grid", 
            url:e.target.href, 
            replace: false, 
            timeout: 20000, 
        }).always(()=>{
            loading.hide();
        });  
    }
',
    View::POS_END
);


/* @var $this yii\web\View */
/* @var $model app\modules\medicos\models\AgendamentoCirurgicoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cid-search">

    <div class="form-group">
        <label>Descrição</label>
        <input type="text" class="form-control" id="DS_PROC_EXAME" autocomplete="off">
    </div>

    <div class="form-group">
        <?= Html::button('Pesquisar', ['class' => 'btn btn-primary', 'id' => 'btn-search-p']) ?>
    </div>

    <?php Pjax::begin(['id' => 'p-grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'linkOptions' => ['data-pjax' => '0', 'onclick' => 'updateSearchP(event);']
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Nome',
                'value' => function ($data) {
                    return $data['DS_PROC_EXAME'];
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Ações',
                'template' => '{select}',
                'buttons'  => [
                    'select' => function ($url, $model) {
                        return Html::button('Selecionar', [
                            'id' => $model['CD_PROCEDIMENTO_TUSS'],
                            'class' => 'btn btn-primary',
                            'onclick' => "window.parent.selectProcedure('" . $model['CD_PROCEDIMENTO_TUSS'] . "', '" . $model['NR_SEQUENCIA'] . "', '" . $model['DS_PROC_EXAME'] . "');"
                        ]);
                    },
                ],
            ]
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>