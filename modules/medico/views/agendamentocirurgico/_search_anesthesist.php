<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\web\View;

$this->registerJs(
    '
    $("#btn-search-a").on("click",function(e){ 
        e.preventDefault();

        let nome = $("#nm_pessoa_fisica_a").val();
        var url = new URL(
            $().getUrl() + "/medico/agendamentocirurgico/searchanesthesistbyparams"
        );
        url.search = new URLSearchParams({
            "NM_PESSOA_FISICA": nome,
        });

        let loading = main.$loading.show();

        $.pjax.reload({
            container:"#a-grid", 
            url:url, 
            replace: false, 
            timeout: 20000, 
        }).always(()=>{
            loading.hide();
        });  
    });

    function updateSearchA(e){
        e.preventDefault();

        let loading = main.$loading.show();

        $.pjax.reload({
            container:"#a-grid", 
            url:e.target.href, 
            replace: false, 
            timeout: 20000, 
        }).always(()=>{
            loading.hide();
        });  
    }
',
    View::POS_END
);


/* @var $this yii\web\View */
/* @var $model app\modules\medicos\models\AgendamentoCirurgicoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ac-anesthesist-search">

    <div class="form-group">
        <label>Nome</label>
        <input type="text" class="form-control" name="nm_pessoa_fisica" id="nm_pessoa_fisica_a" autocomplete="off">
    </div>

    <div class="form-group">
        <?= Html::button('Pesquisar', ['class' => 'btn btn-primary', 'id' => 'btn-search-a']) ?>
    </div>

    <?php Pjax::begin(['id' => 'a-grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'linkOptions' => ['data-pjax' => '0', 'onclick' => 'updateSearchA(event);']
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Nome',
                'value' => function ($data) {
                    return $data['NM_PESSOA_FISICA'];
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'CRM',
                'value' => function ($data) {
                    return $data['NR_CRM'];
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Ações',
                'template' => '{select}',
                'buttons'  => [
                    'select' => function ($url, $model) {
                        return Html::button('Selecionar', [
                            'id' => $model['CD_PESSOA_FISICA'],
                            'class' => 'btn btn-primary',
                            'onclick' => "window.parent.selectAnesthesist(" . $model['CD_PESSOA_FISICA'] . ", '" . $model['NM_PESSOA_FISICA'] . "');"
                        ]);
                    },
                ],
            ]
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>