<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\medicos\models\AgendamentoCirurgico */

$this->title = 'Solicitação de Centro Cirúrgico/Hemodinâmica';
$this->params['breadcrumbs'][] = ['label' => 'Gerenciar Agendamento Cirúrgico', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agendamento-cirurgico-create">

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>