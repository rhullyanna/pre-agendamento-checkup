<?php

namespace app\modules\admin;

use Yii;
use yii\helpers\Url;
use yii\web\ErrorHandler;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->layout = '@app/modules/admin/views/layouts/main';
        Yii::$app->user->loginUrl = ['/admin/default/login'];
        Yii::$app->setHomeUrl(Url::to(['/admin/default/index']));

        Yii::configure($this, [
            'components' => [
                'errorHandler' => [
                    'class' => ErrorHandler::className(),
                    'errorAction' => '/admin/default/error',
                ]
            ],
        ]);

        $handler = $this->get('errorHandler');
        Yii::$app->set('errorHandler', $handler);
        $handler->register();
    }
}
