<?php

namespace app\modules\admin\models;

use yiibr\brvalidator\CpfValidator;
use app\models\Usuario;

/**
 * This is the model class for table "administrador".
 *
 * @property int $id
 * @property string $nome
 * @property string $cpf
 * @property int $admin_id
 */
class Administrador extends \yii\db\ActiveRecord
{

    const SCENARIO_DEFAULT     = 'default';
    const SCENARIO_UPDATE      = 'update';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'administrador';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'cpf'], 'required'],
            [['admin_id'], 'integer'],
            [['nome'], 'trim'],
            [['nome', 'cpf'], 'unique', 'on' => [self::SCENARIO_DEFAULT, self::SCENARIO_UPDATE]],
            [['nome'], 'string', 'max' => 150],
            [['cpf'], 'string', 'max' => 14],
            [['cpf'], CpfValidator::className()],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEFAULT] =
            ['nome', 'cpf'];
        $scenarios[self::SCENARIO_UPDATE] =
            ['nome', 'cpf'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'cpf' => 'CPF',
            'admin_id' => 'Admin ID',
        ];
    }

    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    public function beforeValidate()
    {
        $this->cpf = preg_replace('/[^0-9]/', '', $this->cpf);
        return parent::beforeValidate();
    }
}
