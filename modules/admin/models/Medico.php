<?php

namespace app\modules\admin\models;

use yiibr\brvalidator\CpfValidator;
use app\models\Usuario;
use Yii;

/**
 * This is the model class for table "medico".
 *
 * @property int $id
 * @property string $nome
 * @property string $cpf
 * @property string $crm
 * @property int $usuario_id
 * @property int $medico_id
 *
 * @property Usuario $usuario
 */
class Medico extends \yii\db\ActiveRecord
{

    const SCENARIO_DEFAULT     = 'default';
    const SCENARIO_UPDATE      = 'update';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'medico';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEFAULT] =
            ['nome', 'cpf', 'crm', 'telefone', 'cd_pessoa_fisica'];
        $scenarios[self::SCENARIO_UPDATE] =
            ['nome', 'cpf', 'crm', 'telefone', 'cd_pessoa_fisica'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'cpf', 'crm'], 'required'],
            [['nome'], 'trim'],
            [['nome', 'cpf'], 'unique', 'on' => [self::SCENARIO_DEFAULT, self::SCENARIO_UPDATE]],
            [['cpf'], CpfValidator::className()],
            [['usuario_id', 'medico_id'], 'integer'],
            [['nome'], 'string', 'max' => 150],
            [['cpf'], 'string', 'max' => 14],
            [['crm'], 'string', 'max' => 15],
            [['telefone'], 'string', 'max' => 18],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'cpf' => 'CPF',
            'crm' => 'CRM',
            'usuario_id' => 'Usuário',
            'medico_id' => 'Médico Tasy ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    public function beforeValidate()
    {
        $this->cpf = preg_replace('/[^0-9]/', '', $this->cpf);
        return parent::beforeValidate();
    }
}
