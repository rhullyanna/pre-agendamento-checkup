<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Medico;
use app\modules\admin\models\MedicoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\PermissaoEnum;
use app\models\Usuario;
use app\modules\medico\models\PessoaFisicaTasy;

/**
 * MedicoController implements the CRUD actions for Medico model.
 */
class MedicoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'view', 'update', 'findmodel', 'alterarsenha', 'searchdoctor', 'searchdoctorbyparams'],
                        'roles' => [PermissaoEnum::ADMIN],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['esquecisenha'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Medico models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MedicoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Medico model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Medico model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Medico();
        $usuario = new Usuario();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $trans = Yii::$app->db->beginTransaction();
            try {
                $usuario->login = $model->cpf;
                $usuario->permissao = PermissaoEnum::MEDICO;
                $usuario->save(false);
                $model->usuario_id = $usuario->id;
                $model->save(false);
                Yii::$app->session->setFlash('success', "Médico cadastrado com sucesso.");
                $trans->commit();
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                $trans->rollBack();
                throw $e;
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Medico model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Medico model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Medico model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Medico the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Medico::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSearchdoctor()
    {
        $this->layout = 'modal';
        $model = new PessoaFisicaTasy();
        $dataProvider = $model->searchDoctor(Yii::$app->request->queryParams);
        return $this->render('_search_doctor', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchdoctorbyparams()
    {
        $this->layout = 'modal';
        $model = new PessoaFisicaTasy();
        $dataProvider = $model->searchDoctor(Yii::$app->request->queryParams);

        return $this->renderAjax('_search_doctor', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
