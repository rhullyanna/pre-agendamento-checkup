<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Administrador;
use app\modules\admin\models\AdministradorSearch;
use app\models\PermissaoEnum;
use app\models\Usuario;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


class CadastroController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'novo', 'view', 'update', 'findmodel', 'alterarsenha'],
                        'roles' => [PermissaoEnum::ADMIN],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['esquecisenha'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Administrador models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdministradorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Administrador model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Administrador model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNovo()
    {
        $model = new Administrador();
        $usuario = new Usuario();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $trans = Yii::$app->db->beginTransaction();
            try {
                $usuario->login = $model->cpf;
                $usuario->permissao = PermissaoEnum::ADMIN;
                $usuario->save(false);
                $model->usuario_id = $usuario->id;
                $model->save(false);
                Yii::$app->session->setFlash('success', "Usuário cadastrado com sucesso.");
                $trans->commit();
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                $trans->rollBack();
                throw $e;
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Administrador model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('success', "Dados atualizados com sucesso.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Administrador model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Administrador model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Administrador the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Administrador::findOne((int) $id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionEsquecisenha()
    {

        $model = new Usuario();
        $model->setScenario(Usuario::SCENARIO_FORGOT_PASSWORD);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $usuario = Usuario::find()->where(['login' => $model->login, 'permissao' => PermissaoEnum::ADMIN])->one();
            if (!$usuario) {
                Yii::$app->session->setFlash('error', 'Usuário não encontrado!');
                $this->refresh();
                return;
            }

            $senha = $usuario->generatePass();
            $usuario->setScenario(Usuario::SCENARIO_FORGOT_PASSWORD);
            $trans = Yii::$app->db->beginTransaction();

            try {
                if ($usuario->validate()) {
                    $usuario->save();
                    /*&& $usuario->enviarSenhaEmail($usuario->admin->email, $senha)*/
                    $trans->commit();
                    Yii::$app->session->setFlash('success', 'Senha enviada para o email cadastrado!');
                } else {
                    print_r($usuario->errors);
                    die;
                    $trans->rollBack();
                    Yii::$app->session->setFlash('error', 'Atenção! Ocorreu um problema durante a requisição, tente novamente mais tarde!');
                }
            } catch (\Exception $e) {
                $trans->rollBack();
                throw $e;
            }

            return $this->redirect(['default/login']);
        } else {
            return $this->render('forgot_password', [
                'model' => $model
            ]);
        }
    }

    public function actionAlterarsenha()
    {

        $model = Usuario::findOne(Yii::$app->user->id);
        $model->setScenario(Usuario::SCENARIO_UPDATE_PASSWORD);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('success', 'Senha alterada com sucesso!');
            return $this->redirect(['default/index']);
        } else {
            return $this->render('update_password', [
                'model' => $model,
            ]);
        }
    }
}
