<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Medico */

$this->title = 'Atualizar Dados de Médico #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gerenciar Médico', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medico-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>