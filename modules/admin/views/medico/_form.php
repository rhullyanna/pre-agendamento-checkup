<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use \yii\bootstrap\Modal;
use yii\web\View;
use app\assets\MedicoAsset;

MedicoAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Medico */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs("
    $(function(){
        $('.modalButtonDoctor').click(function (){
                $.get($(this).attr('href'), function(data) {
                $('#modalDoctor').modal('show').find('#modalContentDoctor').html(data)
            });
            return false;
        });
    }); 
");

$this->registerJs('
    function selectDoctor(id, crm, cpf, nome){
        $("#cd_pessoa_fisica").val(id);
        $("#nome").val(nome);
        $("#crm").val(crm);
        $("#cpf").val(cpf);
        $("#modalDoctor").modal("hide");
    };
', View::POS_END);

?>

<div class="medico-form" id="doctor">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div class="form-group required">
                <div class="input-group">
                    <?= $form->field($model, 'nome')->textInput(['id' => 'nome', 'readonly' => true, 'aria-describedby' => 'basic-addon1']); ?>
                    <span class="input-group-addon" id="basic-addon1">
                        <a class="modalButtonDoctor" href="<?= Url::to(['searchdoctor']); ?>">
                            <i class="glyphicon glyphicon-search"></i>
                        </a>
                    </span>
                    <?= $form->field($model, 'cd_pessoa_fisica')->hiddenInput(['id' => 'cd_pessoa_fisica'])->label(false); ?>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'cpf')->textInput(['id' => 'cpf']) ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'crm')->textInput(['id' => 'crm', 'maxlength' => true]) ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'telefone')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '(99)99999-9999'
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Cancelar', ['default/index'], ['class' => 'btn btn-danger']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<?php Modal::begin([
    'header' => '<h3>Pesquisar Médico</h3>',
    'id' => 'modalDoctor',
    'size' => 'modal-lg',
]);

echo "<div id='modalContentDoctor'></div>";

Modal::end(); ?>