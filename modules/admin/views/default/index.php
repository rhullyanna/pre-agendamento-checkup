<?php

/* @var $this yii\web\View */

$this->title = 'Sistema de Pré-agendamento Checkup';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Bem vindo ao Sistema de Pré-agendamento do Hospital Checkup!</h1>

        <p class="lead">Escolha o serviço desejado abaixo.</p>

    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="panel panel-index admin">
                    <div class="panel-heading">
                        <h3>Administrador</h3>
                    </div>
                    <div class="panel-body">
                        <p>Gerenciamento de acessos ao sistema</p>
                        <p><a class="btn btn-default" href="/admin/cadastro/index">Acessar &raquo;</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="panel panel-index paciente">
                    <div class="panel-heading">
                        <h3>Médico</h3>
                    </div>
                    <div class="panel-body">
                        <p>Gerenciamento de médicos no sistema</p>
                        <p><a class="btn btn-default" href="/admin/medico">Acessar &raquo;</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="panel panel-index medico">
                    <div class="panel-heading">
                        <h3>Paciente</h3>
                    </div>
                    <div class="panel-body">
                        <p>Gerenciamento de pacientes no sistema</p>
                        <p><a class="btn btn-default" href="/admin/paciente">Acessar &raquo;</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>