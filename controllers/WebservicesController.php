<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\paciente\models\Paciente;


class WebservicesController extends Controller
{
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'loaduserinsurancedata', 'loadtasyinsurancedata', 'loadtasyexamdata', 'loadexamdoctordata',
                            'loadexamcalendar', 'loadspecialismcalendar', 'loaduserdata', 'loadtasyuserdata',
                            'loadtasyspecialismdata', 'loadspecialismdoctordata', 'loadexamdoctordata',
                            'loadcaraterdata', 'loadsuppliesdata', 'loadtasyplandata', 'loadtasycatdata', 'loadacomdata',
                            'loadsidedata'
                        ],
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    public function actionLoadtasyinsurancedata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = [];

        $sql = "SELECT 
                    CD_CONVENIO, 
                    OBTER_NOME_CONVENIO(CD_CONVENIO) AS CONVENIO,
                    NR_DIGITOS_CODIGO AS CARTEIRINHA
                FROM CONVENIO 
                WHERE 
                IE_SITUACAO = 'A' AND
                IE_AGENDA_WEB = 'S'
                ORDER BY DS_CONVENIO ASC";
        $command = Yii::$app->db2->createCommand($sql);
        $query = $command->queryAll();

        foreach ($query as $item) {
            $response[] = [
                'id' => $item['CD_CONVENIO'],
                'descricao' => $item['CONVENIO'],
                'nr_carteirinha' => $item['CARTEIRINHA']
            ];
        }

        return $response;
    }

    public function actionLoadtasyplandata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = [];

        $sql = "SELECT CD_CONVENIO, CD_PLANO, DS_PLANO FROM CONVENIO_PLANO 
                WHERE 
                IE_SITUACAO = 'A' AND
                CD_CONVENIO = :convenio";
        $command = Yii::$app->db2->createCommand($sql);
        $command->bindParam(':convenio', $_GET['convenio']);
        $query = $command->queryAll();

        foreach ($query as $item) {
            $response[] = ['id' => $item['CD_PLANO'], 'descricao' => $item['DS_PLANO']];
        }

        return $response;
    }

    public function actionLoadtasycatdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = [];

        $sql = "SELECT CD_CONVENIO, CD_CATEGORIA, DS_CATEGORIA FROM CATEGORIA_CONVENIO
                WHERE 
                IE_SITUACAO = 'A' AND
                CD_CONVENIO = :convenio";
        $command = Yii::$app->db2->createCommand($sql);
        $command->bindParam(':convenio', $_GET['convenio']);
        $query = $command->queryAll();

        foreach ($query as $item) {
            $response[] = ['id' => $item['CD_CATEGORIA'], 'descricao' => $item['DS_CATEGORIA']];
        }

        return $response;
    }

    public function actionLoadtasyexamdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $lista = [];

        $sql = "SELECT 
                DISTINCT(P2.NR_SEQ_PROC_INTERNO), 
                P1.DS_PROC_EXAME, 
                P1.CD_PROCEDIMENTO, 
                P1.IE_ORIGEM_PROCED, 
                P1.NR_SEQ_EXAME_LAB,
                P1.NR_SEQUENCIA
                FROM PROC_INTERNO P1, AGENDA_REGRA P2
                WHERE 
                P1.NR_SEQUENCIA = P2.NR_SEQ_PROC_INTERNO 
                AND P2.IE_PERMITE='S'
                AND (P2.CD_CONVENIO = :convenio OR P2.CD_CONVENIO IS NULL)
                ";

        $sql .= " ORDER BY P1.DS_PROC_EXAME";

        $command = Yii::$app->db2->createCommand($sql);

        if ($_GET['convenio'] != '') {
            $command->bindParam(':convenio', $_GET['convenio']);
        } else {
            $command->bindValue(':convenio', Yii::$app->params['checkupparticular']);
        }
        $query = $command->queryAll();

        foreach ($query as $key => $value) {
            $lista[] = [
                'id' => $value['NR_SEQ_PROC_INTERNO'],
                'descricao' => $value['DS_PROC_EXAME'],
                'cd_procedimento' => $value['CD_PROCEDIMENTO'],
                'ie_origem' => $value['IE_ORIGEM_PROCED'],
                'seq_exame' => $value['NR_SEQ_EXAME_LAB']
            ];
        }

        return $lista;
    }

    public function actionLoadexamdoctordata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = [];

        $sql = "SELECT DISTINCT A.CD_AGENDA,
                OBTER_NOME_PF(A.CD_PESSOA_FISICA) AS MEDICO, 
                A.DS_AGENDA,
                A.CD_PESSOA_FISICA,
                A.DS_ORIENTACAO,
                A.DS_OBSERVACAO
                FROM  AGENDA A, AGENDA_REGRA B
                WHERE
                A.CD_AGENDA = B.CD_AGENDA
                AND B.NR_SEQ_PROC_INTERNO = :nr_seq_proc_interno
                AND A.IE_SITUACAO = 'A'                   
                AND A.IE_AGENDA_WEB = 'S'
                AND B.IE_PERMITE = 'S'";

        $command = Yii::$app->db2->createCommand($sql);
        $command->bindParam(':nr_seq_proc_interno', $_GET['exame']);

        $query = $command->queryAll();

        foreach ($query as $item) {
            $response[] = [
                'id' => $item['CD_AGENDA'],
                'descricao' => $item['MEDICO'],
                'cd_pessoa_fisica' => $item['CD_PESSOA_FISICA'],
                'orientacao' => "{$item['DS_ORIENTACAO']} \n {$item['DS_OBSERVACAO']}"
            ];
        }

        return $response;
    }



    public function actionLoadtasyspecialismdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $lista = [];

        $sql = "SELECT 
                A.CD_ESPECIALIDADE,
                OBTER_DESC_ESPEC_MEDICA(A.CD_ESPECIALIDADE) AS ESPECIALIDADE
                FROM
                AGENDA A,
                REGRA_AGECONS_CONVENIO B
                WHERE
                A.CD_AGENDA = B.CD_AGENDA
                AND B.IE_PERMITE = 'S'
                AND A.IE_SITUACAO = 'A'
                AND A.IE_TIPO_ATENDIMENTO = 8
                AND (B.CD_CONVENIO = :convenio)
                AND A.CD_ESPECIALIDADE IS NOT NULL
                GROUP BY A.CD_ESPECIALIDADE, OBTER_DESC_ESPEC_MEDICA(A.CD_ESPECIALIDADE)
             ";

        $sql .= " ORDER BY OBTER_DESC_ESPEC_MEDICA(A.CD_ESPECIALIDADE)";

        $command = Yii::$app->db2->createCommand($sql);
        if ($_GET['convenio'] != '') {
            $command->bindParam(':convenio', $_GET['convenio']);
        } else {
            $command->bindValue(':convenio', Yii::$app->params['checkupparticular']);
        }

        $query = $command->queryAll();

        foreach ($query as $key => $value) {
            $lista[] = [
                'id' => $value['CD_ESPECIALIDADE'],
                'descricao' => $value['ESPECIALIDADE']
            ];
        }

        return $lista;
    }

    public function actionLoadspecialismdoctordata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = [];

        $sql = "SELECT DISTINCT (A.DS_AGENDA),
                OBTER_NOME_PF(A.CD_PESSOA_FISICA) AS MEDICO,
                A.DS_ORIENTACAO,
                A.DS_OBSERVACAO,
                A.CD_AGENDA,
                A.CD_PESSOA_FISICA
                FROM
                AGENDA A,
                REGRA_AGECONS_CONVENIO B
                WHERE
                A.CD_AGENDA = B.CD_AGENDA
                AND B.IE_PERMITE = 'S'
                AND (B.CD_CONVENIO = :convenio) OR (B.CD_CONVENIO IS NULL)
                AND A.IE_TIPO_ATENDIMENTO = 8
                AND A.IE_SITUACAO = 'A'                   
                AND A.CD_ESPECIALIDADE = :especialidade ";

        $sql .= " ORDER BY OBTER_NOME_PF(A.CD_PESSOA_FISICA)";

        $command = Yii::$app->db2->createCommand($sql);
        $command->bindParam(':especialidade', $_GET['especialidade']);
        if ($_GET['convenio'] != '') {
            $command->bindParam(':convenio', $_GET['convenio']);
        } else {
            $command->bindValue(':convenio', Yii::$app->params['checkupparticular']);
        }

        $query = $command->queryAll();

        foreach ($query as $item) {
            $response[] = [
                'id' => $item['CD_AGENDA'],
                'descricao' => $item['MEDICO'],
                'cd_medico' => $item['CD_PESSOA_FISICA'],
                'orientacao' => "{$item['DS_ORIENTACAO']} \n {$item['DS_OBSERVACAO']}"
            ];
        }

        return $response;
    }


    public function actionLoadexamcalendar()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $dates = [];

        $sql = "SELECT
                    NR_SEQUENCIA,
                    CD_AGENDA,
                    DT_AGENDA,
                    NR_MINUTO_DURACAO,
                    TO_CHAR(HR_INICIO,'DD/MM/YYYY HH24:MI') as data
                FROM
                TASY.AGENDA_PACIENTE
                WHERE
                IE_STATUS_AGENDA = 'L'
                AND DT_AGENDA >= TRUNC(SYSDATE)
                AND CD_AGENDA = :cd_agenda
                AND HR_INICIO BETWEEN SYSTIMESTAMP+2 AND SYSTIMESTAMP+7
                ";

        $command = Yii::$app->db2->createCommand($sql);
        $command->bindParam(':cd_agenda', $_GET['cd_agenda']);
        $query = $command->queryAll();

        foreach ($query as $n => $item) {
            $date_init = \DateTime::createFromFormat('d/m/Y H:i', $item['DATA']);
            $date_end = \DateTime::createFromFormat('d/m/Y H:i', $item['DATA']);
            $date_end = $date_end->add(new \DateInterval('PT' . $item['NR_MINUTO_DURACAO'] . 'M'));

            $infoData = [
                'id' => $n,
                'title' => $date_init->format('H:i'),
                'start' => $date_init->format('Y-m-d H:i'),
                'end' => $date_end->format('Y-m-d H:i'),
                'scheduled' => $date_init->format('H:i') . ' - ' . $date_end->format('H:i'),
                'description' => "{$date_init->format('d/m/Y')} às {$date_init->format('H:i')}",
                'duration' => $item['NR_MINUTO_DURACAO'],
                'sequence' => $item['NR_SEQUENCIA'],
            ];
            array_push($dates, $infoData);
        }

        return $dates;
    }


    public function actionLoadspecialismcalendar()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $dates = [];

        $sql = "SELECT
                    NR_SEQUENCIA,
                    CD_AGENDA,
                    NR_MINUTO_DURACAO,
                    TO_CHAR(DT_AGENDA,'DD/MM/YYYY HH24:MI') as data,
                    NR_SEQ_TURNO,
                    IE_CLASSIF_AGENDA
                FROM
                TASY.AGENDA_CONSULTA
                WHERE
                IE_STATUS_AGENDA = 'L'
                AND CD_AGENDA = :cd_agenda
                AND DT_AGENDA BETWEEN SYSTIMESTAMP+2 AND SYSTIMESTAMP+7
                ";

        $command = Yii::$app->db2->createCommand($sql);
        $command->bindParam(':cd_agenda', $_GET['cd_agenda']);
        $query = $command->queryAll();

        foreach ($query as $n => $item) {
            $date_init = \DateTime::createFromFormat('d/m/Y H:i', $item['DATA']);
            $date_end = \DateTime::createFromFormat('d/m/Y H:i', $item['DATA']);
            $date_end = $date_end->add(new \DateInterval('PT' . $item['NR_MINUTO_DURACAO'] . 'M'));

            $infoData = [
                'id' => $n,
                'title' => $date_init->format('H:i'),
                'start' => $date_init->format('Y-m-d H:i'),
                'end' => $date_end->format('Y-m-d H:i'),
                'scheduled' => $date_init->format('H:i') . ' - ' . $date_end->format('H:i'),
                'description' => "{$date_init->format('d/m/Y')} às {$date_init->format('H:i')}",
                'duration' => $item['NR_MINUTO_DURACAO'],
                'sequence' => $item['NR_SEQUENCIA'],
                'turno' => $item['NR_SEQ_TURNO'],
                'classif' => $item['IE_CLASSIF_AGENDA'],
            ];
            array_push($dates, $infoData);
        }

        return $dates;
    }


    public function actionLoadtasyuserdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $patientData = [];

        // IE_STATUS_EXPORTAR
        $sql = "SELECT  NR_RGA, IE_SEXO, DT_NASCIMENTO, CD_PESSOA_FISICA FROM PESSOA_FISICA WHERE NR_CPF = :cpf";

        $command = Yii::$app->db2->createCommand($sql);
        $command->bindParam(':cpf', $_GET['cpf']);
        $query = $command->queryOne();

        $patientData = [
            'rg' => strval($query['NR_RGA']),
            'sexo' => strval($query['IE_SEXO']),
            'data_nascimento' => strval($query['DT_NASCIMENTO']),
            'paciente_id' => strval($query['CD_PESSOA_FISICA'])
        ];
        return $patientData;
    }

    public function actionLoaduserdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $patient = Paciente::findOne(['cpf' => Yii::$app->user->identity->username]);
        $patientData = [
            'id' => $patient->id,
            'nome' => $patient->nome,
            'cpf' => $patient->cpf,
            'rg' => $patient->rg,
            'sexo' => $patient->sexo,
            'data_nascimento' => $patient->data_nascimento,
        ];
        return $patientData;
    }

    public function actionLoaduserinsurancedata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $patient = Paciente::findOne(['cpf' => Yii::$app->user->identity->username]);
        $patientData = [
            'id' => $patient->id,
            'convenio_id' => $patient->convenio_id == null ? "" : $patient->convenio_id,
            'nome_convenio' => $patient->nome_convenio,
            'numero_convenio' => $patient->numero_convenio,
            'validade_convenio' => $patient->validade_convenio,
        ];
        return $patientData;
    }

    public function actionLoadproceduredata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $lista = [];
        $descricao = strtolower($_GET['term']);

        $sql = "SELECT CD_PROCEDIMENTO_TUSS, NR_SEQUENCIA, DS_PROC_EXAME 
                FROM PROC_INTERNO WHERE lower(DS_PROC_EXAME) LIKE lower('%{$descricao}%') 
                AND CD_PROCEDIMENTO_TUSS IS NOT NULL
                ORDER BY DS_PROC_EXAME";

        $command = Yii::$app->db2->createCommand($sql);

        $query = $command->queryAll();

        foreach ($query as $key => $item) {
            $lista[$key]['id'] = "{$item['CD_PROCEDIMENTO_TUSS']}";
            $lista[$key]['label'] = $item['DS_PROC_EXAME'];
            $lista[$key]['nr_seq_proc_interno'] = $item['NR_SEQUENCIA'];
        }

        return $lista;
    }

    public function actionLoadcaraterdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = [];

        $sql = "SELECT CD_DOMINIO, VL_DOMINIO, DS_VALOR_DOMINIO FROM VALOR_DOMINIO WHERE CD_DOMINIO = 1016 ORDER BY DS_VALOR_DOMINIO";

        $command = Yii::$app->db2->createCommand($sql);

        $query = $command->queryAll();

        foreach ($query as $item) {
            $response[] = ['id' => $item['VL_DOMINIO'], 'descricao' => $item['DS_VALOR_DOMINIO']];
        }

        return $response;
    }

    public function actionLoadacomdata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = [];

        $sql = "
            SELECT CD_TIPO_ACOMODACAO, DS_TIPO_ACOMODACAO 
            FROM TIPO_ACOMODACAO
            WHERE IE_SITUACAO = 'A'
            ORDER BY DS_TIPO_ACOMODACAO ASC
        ";

        $command = Yii::$app->db2->createCommand($sql);

        $query = $command->queryAll();

        foreach ($query as $key => $item) {
            $response[] = ['id' => $item['CD_TIPO_ACOMODACAO'], 'descricao' => $item['DS_TIPO_ACOMODACAO']];
        }

        return $response;
    }

    public function actionLoadsidedata()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $response = [];

        $sql = "SELECT VL_DOMINIO,  DS_VALOR_DOMINIO  FROM VALOR_DOMINIO where cd_dominio = 1372";

        $command = Yii::$app->db2->createCommand($sql);

        $query = $command->queryAll();

        foreach ($query as $key => $item) {
            $response[] = ['id' => $item['VL_DOMINIO'], 'descricao' => $item['DS_VALOR_DOMINIO']];
        }

        return $response;
    }
}
