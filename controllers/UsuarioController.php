<?php

namespace app\controllers;

use Yii;
use app\models\Usuario;
use app\models\UsuarioSearch;
use app\models\PermissaoEnum;
use app\models\StatusEnum;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['esquecisenha', 'alterarsenha'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['alterarsenha', 'esquecisenha'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['reativar', 'desativar'],
                        'roles' => [PermissaoEnum::ADMIN],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['esquecisenha'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionReativar($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->status = StatusEnum::ATIVO;
        $model->save(false);
        Yii::$app->session->setFlash('success', 'Usuário reativado com sucesso.');
        return $this->redirect($request->getReferrer());
    }

    public function actionDesativar($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->status = StatusEnum::INATIVO;
        $model->save(false);
        Yii::$app->session->setFlash('success', 'Usuário desativado com sucesso.');
        return $this->redirect($request->getReferrer());
    }
}
