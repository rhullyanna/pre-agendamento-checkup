<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => env('DB_ORACLE_HOST'),
    'username' => env('DB_ORACLE_USER'),
    'password' => env('DB_ORACLE_PASS'),

    'charset' => 'utf8',
    'on afterOpen' => function ($event) {
        $event->sender->createCommand("ALTER SESSION SET NLS_NUMERIC_CHARACTERS = ',.'")->execute();
        $event->sender->createCommand("ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI:SS'")->execute();
        $event->sender->createCommand("ALTER SESSION SET NLS_SORT = WEST_EUROPEAN")->execute();
        $event->sender->createCommand("alter session set nls_comp=linguistic")->execute();
        $event->sender->createCommand("alter session set nls_sort=binary_ai")->execute();
    },

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
