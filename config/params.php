<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'checkupuser' => 'CHECKUPWEB',
    'checkuplocal' => 1,
    'checkupparticular' => 50,
    'checkupformaagend' => 8,
    'checkupagendc' => 336
];
