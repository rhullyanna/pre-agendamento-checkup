<?php

namespace app\components;

use app\models\Log;
use app\models\LogAcaoEnum;
use Yii;

class LogActiveRecord extends \yii\db\ActiveRecord
{

    protected $log;
    protected $delete = false;

    public function getLog()
    {
        if ($this->log === null) {
            $this->log = new log();
        }
        return $this->log;
    }

    public function beforeDelete()
    {
        $this->delete = true;
        $this->log();
        return parent::beforeDelete();
    }

    public function log()
    {
        $this->getLog();

        $this->log->tabela = $this->tableName();
        $this->log->usuario_id = Yii::$app->user->identity->id;

        if ($this->log->acao == '') {
            if ($this->isNewRecord) {
                $this->log->acao = LogAcaoEnum::INSERT;
            } else if ($this->delete) {
                $this->log->acao = LogAcaoEnum::DELETE;
            } else {
                $this->log->acao = LogAcaoEnum::UPDATE;
            }
        }
        $this->log->fk_codigo = $this->getPrimaryKey();
        $this->log->save(false);
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->log !== null) {
            $this->log();
        }
        return parent::afterSave($insert, $changedAttributes);
    }
}
