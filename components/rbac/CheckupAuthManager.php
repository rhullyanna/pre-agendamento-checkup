<?php
namespace app\components\rbac;
use yii\rbac\DbManager;
use app\models\Usuario;

class CheckupAuthManager extends DbManager {
	
	public function checkAccess($userId, $itemName, $params=[]){

		$row = Usuario::find()->where('id = :id and permissao = :permissao', [':id'=>$userId,':permissao'=>(int)$itemName])->one();
		return $row!==null;	
	}

	public function isAssigned($itemName,$userId)
	{		
		$row = Usuario::find()->where('id = :id and permissao = :permissao', [':id'=>$userId,':permissao'=>(int)$itemName])->one();
		return $row->count()!==false;
	}
}
