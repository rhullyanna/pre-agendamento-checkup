"use strict";

Vue.use(VueLoading);
Vue.component("loading", VueLoading);

var main = new Vue({
  el: "#cirurgico",
  data() {
    return {
      urlView: $().getUrl() + "/medico/agendamentocirurgico/view",
      urlCarater: $().getUrl() + "/webservices/loadcaraterdata",
      urlInsurances: $().getUrl() + "/webservices/loadtasyinsurancedata",
      urlDoctorData:
        $().getUrl() + "/medico/agendamentocirurgico/loaddoctordata",
      urlValidateData:
        $().getUrl() + "/medico/agendamentocirurgico/validatepostdata",
      urlPlans: $().getUrl() + "/webservices/loadtasyplandata",
      urlAcoms: $().getUrl() + "/webservices/loadacomdata",
      insuranceList: [],
      caraterList: [],
      procedureList: [],
      planList: [],
      rlList: [],
      acomodationList: [],
      addedProcedures: [],
      addedProcedure: {
        procedimento_id: "",
        nome_procedimento: "",
        convenio_id: "",
        nome_convenio: "",
        plano_id: "",
        nome_plano: "",
        medico_id: "",
        nome_medico: "",
        quantidade: "",
        nr_seq_proc_interno: "",
      },
      proceduresjson: "",
      addedPlanList: [],
      addedInsuranceList: [],
      insurance: "",
      rl: "",
      plan: "",
      acomodation: "",
      files: {},
      filesJson: "",
      file: "",
      carater: "",
      user: {
        nome: document.getElementById("nome_paciente").value,
      },
      doctor: {},
      erroForm: false,
      errorsList: [],
      procError: false,
      procErrorMessage: "",
    };
  },

  methods: {
    addProcedure() {
      this.procError = false;
      this.procErrorMessage = "";
      if (
        this.addedProcedure.convenio_id == "" &&
        this.addedProcedure.plano_id == "" &&
        this.addedProcedure.procedimento_id == ""
      ) {
        this.procError = true;
        this.procErrorMessage =
          "É necessário informar Convênio, Plano  e Procedimento Adicional";
        return;
      }
      let exists = this.addedProcedures.find(
        (x) => x.procedimento_id == this.addedProcedure.procedimento_id
      );
      if (exists != undefined) {
        this.procError = true;
        this.procErrorMessage = "Procedimento já inserido";
        return;
      }

      this.setConvenioAdc(this.addedProcedure.convenio_id);
      this.setPlanoAdc(this.addedProcedure.plano_id);

      this.addedProcedures.push(this.addedProcedure);

      this.addedProcedure = {
        procedimento_id: "",
        nome_procedimento: "",
        convenio_id: "",
        nome_convenio: "",
        plano_id: "",
        nome_plano: "",
        medico_id: "",
        nome_medico: "",
        quantidade: "",
        nr_seq_proc_interno: "",
      };
      this.proceduresjson = JSON.stringify(this.addedProcedures);

      $("#insuranceAddedField").val("");
      $("#nome_procedimento_adicional").val("");
    },

    removeProcedure(proc, index) {
      this.addedProcedures = this.addedProcedures.filter(
        (x) => x.procedimento_id != proc
      );
      this.proceduresJson = JSON.stringify(this.addedProcedures);
    },

    loadAddedDependencies() {
      this.loadAddedPlan();
    },

    loadInsurance() {
      var myInit = { method: "GET", mode: "cors", cache: "default" };
      fetch(this.urlInsurances, myInit).then((response) => {
        if (response.ok) {
          response.json().then((list) => {
            if (list.length > 0) {
              this.insuranceList = list;
            }
          });
        }
      });
    },

    loadAddedInsurance() {
      var myInit = { method: "GET", mode: "cors", cache: "default" };
      fetch(this.urlInsurances, myInit).then((response) => {
        if (response.ok) {
          response.json().then((list) => {
            if (list.length > 0) {
              this.addedInsuranceList = list;
            }
          });
        }
      });
    },

    loadCarater: function () {
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };
      return fetch(this.urlCarater, myInit).then((response) => {
        if (response.ok) {
          response.json().then((list) => {
            this.caraterList = list;
          });
        }
      });
    },

    loadPlan() {
      var url = new URL(this.urlPlans);
      url.search = new URLSearchParams({
        convenio: this.insurance,
      });
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };

      var myInit = { method: "GET", mode: "cors", cache: "default" };
      fetch(url, myInit).then((response) => {
        if (response.ok) {
          response.json().then((list) => {
            if (list.length > 0) {
              this.planList = list;
            }
          });
        }
      });
    },

    loadAddedPlan() {
      let loader = this.$loading.show();
      var url = new URL(this.urlPlans);
      url.search = new URLSearchParams({
        convenio: this.addedProcedure.convenio_id,
      });
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };

      var myInit = { method: "GET", mode: "cors", cache: "default" };
      fetch(url, myInit).then(
        (response) => {
          if (response.ok) {
            response.json().then((list) => {
              if (list.length > 0) {
                this.addedPlanList = list;
                loader.hide();
              }
            });
          }
        },
        (error) => {
          loader.hide();
        }
      );
    },

    loadAcom() {
      var url = new URL(this.urlPlans);
      url.search = new URLSearchParams({
        convenio: this.insurance,
      });
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };

      var myInit = { method: "GET", mode: "cors", cache: "default" };
      fetch(this.urlAcoms, myInit).then((response) => {
        if (response.ok) {
          response.json().then((list) => {
            if (list.length > 0) {
              this.acomodationList = list;
            }
          });
        }
      });
    },

    loadUserDoctorData: function () {
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };
      fetch(this.urlDoctorData, myInit).then((response) => {
        if (response.ok) {
          response.json().then((doctorData) => {
            this.doctor = doctorData;
          });
        }
      });
    },

    setCarater: function () {
      let item = this.caraterList.find((d) => d.id == this.carater);
      if (item !== undefined) {
        $("#nome_carater").val(item.descricao);
      } else {
        $("#nome_carater").val("");
      }
    },

    setConvenio: function () {
      let item = this.insuranceList.find((d) => d.id == this.insurance);
      if (item !== undefined) {
        $("#nome_convenio").val(item.descricao);
      } else {
        $("#nome_convenio").val("");
      }
    },

    setConvenioAdc: function (convenio) {
      let item = this.addedInsuranceList.find((d) => d.id == convenio);
      if (item !== undefined) {
        this.addedProcedure.nome_convenio = item.descricao;
      } else {
        this.addedProcedure.nome_convenio = "";
      }
    },

    setPlano: function () {
      let item = this.planList.find((d) => d.id == this.plan);
      if (item !== undefined) {
        $("#nome_plano").val(item.descricao);
      } else {
        $("#nome_plano").val("");
      }
    },

    setPlanoAdc: function (plano) {
      let item = this.addedPlanList.find((d) => d.id == plano);
      if (item !== undefined) {
        this.addedProcedure.nome_plano = item.descricao;
      } else {
        this.addedProcedure.nome_plano = "";
      }
    },

    setAcomodacao: function () {
      let item = this.acomodationList.find((d) => d.id == this.acomodation);
      if (item !== undefined) {
        $("#nome_acomodacao").val(item.descricao);
      } else {
        $("#nome_acomodacao").val("");
      }
    },

    validate: function () {
      this.errorsForm = false;

      let formData = new FormData(
        document.querySelector("#agendamento-cirurgico-form")
      );

      var myInit = {
        method: "POST",
        mode: "cors",
        credentials: "same-origin",
        processData: false,
        body: formData,
      };

      let loader = this.$loading.show();

      fetch(this.urlValidateData, myInit)
        .then((response) => {
          if (response.ok) {
            response.json().then((obj) => {
              if (obj.success) {
                var urlView = new URL(this.urlView);
                urlView.search = new URLSearchParams({
                  id: obj.id,
                });

                window.location = urlView;
              } else {
                loader.hide();
                this.erroForm = true;
                this.errorsList = obj.errors;
                $("html, body").animate(
                  { scrollTop: $("#errorsForm").position().top },
                  "slow"
                );
              }
            });
          }
        })
        .catch((error) => {
          console.log("Error:", error);
          loader.hide();
        });
    },
  },

  mounted: function () {
    this.$nextTick(function () {
      let loader = this.$loading.show();
      Promise.all([
        this.loadUserDoctorData(),
        this.loadInsurance(),
        this.loadCarater(),
        this.loadAcom(),
        this.loadAddedInsurance(),
      ]).then(() => {
        setTimeout(() => {
          loader.hide();
        }, 1000);
      });
    });
  },

  watch: {
    insurance: function (value) {
      if (value) {
        this.plan = "";
        this.cat = "";

        let loader = this.$loading.show();
        Promise.all([this.loadPlan()]).then(() => {
          setTimeout(() => {
            loader.hide();
          }, 1000);
        });
      }
      this.setConvenio();
    },

    carater: function (value) {
      if (value) {
        this.setCarater();
      }
    },

    plan: function (value) {
      if (value) {
        this.setPlano();
      }
    },

    acomodation: function (value) {
      if (value) {
        this.setAcomodacao();
      }
    },
  },
});
