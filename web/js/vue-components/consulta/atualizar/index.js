"use strict";

new Vue({
  el: "#consulta",
  data: {
    urlCalendar: $().getUrl() + "/webservices/loadtasyconsultationcalendar",

    id: document.getElementById("id").value,
    type: document.getElementById("tipo_id").value,
    specialism: document.getElementById("especialidade_id").value,
    doctor: document.getElementById("medico_id").value,
    time: document.getElementById("horario").value,
    dates: [],
    calendarLoaded: false,
    calendarError: false,
    calendarErrorMessage: ""
  },

  methods: {
    loadCalendar: function() {
      var url = new URL(this.urlCalendar);
      url.search = new URLSearchParams({
        especialidade_id: this.specialism,
        medico_id: this.doctor
      });

      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default"
      };
      fetch(url, myInit).then(response => {
        if (response.ok) {
          response.json().then(list => {
            if (list.length == 0) {
              this.calendarError = true;
              this.calendarErrorMessage = "Nenhuma informação a apresentar";
              return;
            }
            this.calendarLoaded = true;
            this.dates = list;
            this.displayCalendar();
            document.getElementById("fieldsetCalendar").scrollIntoView(true);
          });
        }
      });
      ("");
    },

    displayCalendar: function() {
      var self = this;
      var calendarEl = document.getElementById("calendar");
      calendarEl.innerHTML = "";

      var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ["dayGrid", "timeGrid"],
        height: "auto",
        defaultView: "timeGrid",
        locale: "pt-br",
        header: {
          left: "",
          center: "title",
          right: ""
        },
        allDaySlot: false,
        minTime: "08:00:00",
        maxTime: "16:51:00",
        visibleRange: function(currentDate) {
          var startDate = new Date(currentDate.valueOf());
          var endDate = new Date(currentDate.valueOf());

          startDate.setDate(startDate.getDate() + 2);
          endDate.setDate(endDate.getDate() + 7);
          return { start: startDate, end: endDate };
        },
        events: [
          {
            groupId: 999,
            title: "08:00",
            start: "2020-03-14T08:00:00",
            end: "2020-03-14T08:10:00"
          },
          {
            groupId: 999,
            title: "08:10",
            start: "2020-03-14T08:10:00",
            end: "2020-03-14T08:20:00"
          },
          {
            groupId: 999,
            title: "08:20",
            start: "2020-03-14T08:20:00",
            end: "2020-03-14T08:30:00"
          },
          {
            groupId: 999,
            title: "08:30",
            start: "2020-03-14T08:30:00",
            end: "2020-03-14T08:40:00"
          },
          {
            groupId: 999,
            title: "08:40",
            start: "2020-03-14T08:40:00",
            end: "2020-03-14T08:50:00"
          },
          {
            groupId: 999,
            title: "08:50",
            start: "2020-03-14T08:50:00",
            end: "2020-03-14T09:00:00"
          }
        ],

        eventClick: function(info) {
          var d = self.formatDate(info.event.start, "dd/MM/yyyy hh:mm:ss");
          document.getElementById("horario").value = d;
          alert(
            "Horário Selecionado: " + document.getElementById("horario").value
          );
        }
      });
      setTimeout(() => {
        calendar.render();
      }, 500);
    },

    formatDate: function(x, y) {
      var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
      };
      y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval("z." + v.slice(-1))).slice(-2);
      });

      return y.replace(/(y+)/g, function(v) {
        return x
          .getFullYear()
          .toString()
          .slice(-v.length);
      });
    }
  },
  mounted: function() {
    this.$nextTick(function() {
      this.loadCalendar();
    });
  }
});
