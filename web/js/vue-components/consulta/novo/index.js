"use strict";

Vue.use(VueLoading);
Vue.component("loading", VueLoading);

var main = new Vue({
  el: "#consulta",
  data: {
    urlInsurances: $().getUrl() + "/webservices/loadtasyinsurancedata",
    urlSpecialismTypes:
      $().getUrl() + "/webservices/loadtasyspecialismtypedata",
    urlSpecialisms: $().getUrl() + "/webservices/loadtasyspecialismdata",
    urlDoctors: $().getUrl() + "/webservices/loadspecialismdoctordata",
    urlCalendar: $().getUrl() + "/webservices/loadspecialismcalendar",
    urlUserInsuranceData: $().getUrl() + "/webservices/loaduserinsurancedata",
    urlValidateData:
      $().getUrl() + "/paciente/agendamentoconsulta/validatepostdata",
    urlView: $().getUrl() + "/paciente/agendamentoconsulta/view",

    imageInsurance: document.getElementById("convImageHidden").value,
    id: document.getElementById("id").value,
    insuranceList: [],
    specialismList: [],
    doctorList: "",
    dates: [],
    withInsurance: "",
    insurance: "",
    type: 0,
    specialism: "",
    cd_agenda: "",
    duration: "",
    personalData: "",
    calendarLoaded: false,
    calendarError: false,
    calendarErrorMessage: "",
    notFound: false,
    notFoundMessage: "",
    imageError: false,
    imageMessage: "",
    specialismScheduled: "",
    isSpecialismScheduled: false,
    scheduleError: false,
    scheduleErrorMessage: "",
    user: {},
    userInsurance: { convenio_id: "" },
    erroForm: false,
    errorsList: [],
    orientations: null,
  },

  methods: {
    loadInsurance: function () {
      let loader = this.$loading.show();
      var myInit = { method: "GET", mode: "cors", cache: "default" };
      fetch(this.urlInsurances, myInit)
        .then((response) => {
          if (response.ok) {
            response.json().then((list) => {
              this.insuranceList = list;
            });
          }
        })
        .finally(() => {
          loader.hide();
        });
    },

    setConvenio() {
      if (this.insurance != "") {
        $("#convenio_tasy_nome").val(
          $("#insuranceField option:selected").text()
        );
      } else {
        $("#convenio_tasy_nome").val("");
      }
    },

    disableSpecialismField() {
      return this.withInsurance === "";
    },

    loadSpecialism: function () {
      document.getElementById("specialismField").disabled = false;

      this.setConvenio();

      this.calendarErrorMessage = "";
      this.calendarError = false;
      let loader = this.$loading.show();
      var url = new URL(this.urlSpecialisms);
      url.search = new URLSearchParams({
        convenio: this.insurance,
      });
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };
      var self = this;

      fetch(url, myInit)
        .then((response) => {
          if (response.ok) {
            response.json().then((list) => {
              self.specialismList = list;
            });
          }
        })
        .finally(() => {
          loader.hide();
        });
    },

    loadDoctor: function () {
      let loader = this.$loading.show();
      var url = new URL(this.urlDoctors);
      url.search = new URLSearchParams({
        especialidade: this.specialism,
        convenio: this.userInsurance.convenio_id,
      });
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };
      return Promise.resolve(
        fetch(url, myInit)
          .then((response) => {
            if (response.ok) {
              response.json().then((list) => {
                this.doctorList = list;
              });
            }
          })
          .finally(() => {
            loader.hide();
          })
      );
    },

    loadCalendar: function () {
      this.calendarError = false;
      this.calendarErrorMessage = "";

      if (
        this.specialism == "" ||
        this.specialism == "0" ||
        this.cd_agenda == "" ||
        this.cd_agenda == "0"
      ) {
        this.calendarError = true;
        this.calendarErrorMessage =
          "É necessário informar a especialidade e o médico";
        return;
      }

      var url = new URL(this.urlCalendar);
      url.search = new URLSearchParams({
        cd_agenda: this.cd_agenda,
      });

      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };
      let loader = this.$loading.show();
      fetch(url, myInit).then((response) => {
        if (response.ok) {
          response
            .json()
            .then((list) => {
              loader.hide();
              if (list.length == 0) {
                this.calendarError = true;
                this.calendarErrorMessage =
                  "Nenhuma consulta encontrada com estes parâmetros";
                return;
              }
              this.calendarLoaded = true;
              this.dates = list;
              this.displayCalendar();
              document.getElementById("fieldsetCalendar").scrollIntoView(true);
            })
            .catch((error) => {
              loader.hide();
              console.log(error);
            });
        }
      });
      ("");
    },

    displayCalendar: function () {
      var self = this;
      var calendarEl = document.getElementById("calendar");
      calendarEl.innerHTML = "";
      this.specialismScheduled = "";
      this.isSpecialismScheduled = false;
      this.scheduleError = false;

      var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ["dayGrid", "timeGrid"],
        height: "auto",
        defaultView: "timeGrid",
        locale: "pt-br",
        header: {
          left: "",
          center: "title",
          right: "",
        },
        allDaySlot: false,
        minTime: "07:00:00",
        maxTime: "22:00:00",
        // expandRows: true,
        // height: "100%",
        hiddenDays: [0],
        displayEventTime: false,
        visibleRange: function (currentDate) {
          var startDate = new Date(currentDate.valueOf());
          var endDate = new Date(currentDate.valueOf());

          startDate.setDate(startDate.getDate() + 2);
          endDate.setDate(endDate.getDate() + 7);
          return { start: startDate, end: endDate };
        },
        events: this.dates,

        eventClick: function (info) {
          var d = self.formatDate(info.event.start, "dd/MM/yyyy hh:mm:ss");
          if (
            confirm(
              "Horário Selecionado: " + info.event.extendedProps.description
            )
          ) {
            $(".fc-event").removeClass("selected-event");
            self.specialismScheduled = d;
            self.duration = info.event.extendedProps.duration;
            document.getElementById("seqHidden").value =
              info.event.extendedProps.sequence;
            document.getElementById("nr_seq_turno").value =
              info.event.extendedProps.turno;
            document.getElementById("ie_classif_agenda").value =
              info.event.extendedProps.classif;
            self.isSpecialismScheduled = true;

            setTimeout(() => {
              document.getElementById("horario").scrollIntoView({
                behavior: "smooth",
                block: "center",
                inline: "start",
              });
            }, 500);
            info.el.classList.add("selected-event");
          }
        },
      });
      setTimeout(() => {
        calendar.render();
        if (this.dates.length > 100) {
          calendar.contentHeight = 800;
        }
      }, 500);
    },

    formatDate: function (x, y) {
      var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds(),
      };
      y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
        return ((v.length > 1 ? "0" : "") + eval("z." + v.slice(-1))).slice(-2);
      });

      return y.replace(/(y+)/g, function (v) {
        return x.getFullYear().toString().slice(-v.length);
      });
    },

    loadUserInsuranceData: function () {
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };
      fetch(this.urlUserInsuranceData, myInit).then((response) => {
        if (response.ok) {
          response.json().then((userInsurance) => {
            this.userInsurance = userInsurance;
          });
        }
      });
    },

    checkInsurance: function (event) {
      if (event.target.value) {
        this.withInsurance = event.target.value == "1";
      } else {
        this.withInsurance = "";
      }
    },

    processFileInsurance(event) {
      this.imageError = false;
      this.imageMessage = "";
      var input = event.target;
      if (input.files && input.files[0]) {
        let format = input.files[0].name.match(/\.(jpg|jpeg|png|pdf)$/);
        if (format == null) {
          this.imageError = true;
          this.imageMessage = "Formato inválido para upload.";
          return;
        }
        var reader = new FileReader();
        reader.onload = (e) => {
          this.imageInsurance = e.target.result;
          document.getElementById("convImageHidden").value = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
      }
    },

    validate: function () {
      this.errorsForm = false;

      let formData = new FormData(
        document.querySelector("#agendamento-consulta-form")
      );

      var myInit = {
        method: "POST",
        mode: "cors",
        credentials: "same-origin",
        body: formData,
      };
      let loader = this.$loading.show();

      fetch(this.urlValidateData, myInit)
        .then((response) => {
          if (response.ok) {
            response.json().then((obj) => {
              if (obj.success) {
                var urlView = new URL(this.urlView);
                urlView.search = new URLSearchParams({
                  id: obj.id,
                });
                window.location = urlView;
              } else {
                loader.hide();
                this.erroForm = true;
                this.errorsList = obj.errors;
                $("html, body").animate(
                  { scrollTop: $("#errorsForm").position().top },
                  "slow"
                );
              }
            });
          }
        })
        .catch((error) => console.log("Error:", error));
    },

    setCDMedico: function () {
      let item = this.doctorList.find((d) => d.id == this.cd_agenda);
      if (item !== undefined) {
        $("#cd_medico").val(item.cd_medico);
        $("#nome_medico").val(item.descricao);
        this.orientations = item.orientacao;
      } else {
        $("#cd_medico").val("");
      }
    },

    setSpecialistInfo: function () {
      let item = this.specialismList.find((d) => d.id == this.specialism);
      if (item !== undefined) {
        $("#descricaoHidden").val(item.descricao);
        this.loadDoctor();
      } else {
        $("#descricaoHidden").val("");
      }
    },
  },
  mounted: function () {
    this.$nextTick(function () {
      $("#validade_convenio").mask("99/99/9999");
      $("#data_nascimento").mask("99/99/9999");
    });
  },
  watch: {
    withInsurance: function (value) {
      this.calendarLoaded = false;
      this.specialism = "";
      this.cd_agenda = "";
      this.specialismScheduled = "";
      this.isSpecialismScheduled = false;
      this.doctorList = [];
      this.specialismList = [];
      $("#nome_medico").val("");
      $("#specialismField").val("");

      if (value == "1") {
        this.loadInsurance();
      } else if (value == "0") {
        this.userInsurance = { convenio_id: "" };
        this.insurance = "";
        this.loadSpecialism();
        this.imageInsurance = "";
        document.getElementById("numeroconvenio").value = "";
        document.getElementById("validade_convenio").value = "";
        document.getElementById("convImageHidden").value = "";
      }
    },
    insurance: function (value) {
      let ins = this.insuranceList.find(
        (x) => x.id == value && x.nr_carteirinha != null
      );
      if (ins != undefined) {
        $("#nr_carteirinha").val(ins.nr_carteirinha);
        $("#numero_convenio").attr(
          "placeholder",
          `Número contendo ${ins.nr_carteirinha} dígitos`
        );
      } else {
        $("#numero_convenio").removeAttr("placeholder");
      }
      this.userInsurance.convenio_id = value;
      this.calendarLoaded = false;
      this.specialism = "";
      this.cd_agenda = "";
      this.orientations = null;
      $("#specialismField").val("");
      this.specialismScheduled = "";
      this.doctorList = [];
    },

    specialism: function (value) {
      if (value) {
        this.calendarLoaded = false;
        this.specialismScheduled = "";
        this.cd_agenda = "";
        this.isSpecialismScheduled = false;
        this.loadDoctor();
      }
    },
    cd_agenda: function (value) {
      this.calendarLoaded = false;
      this.specialismScheduled = "";
      this.isSpecialismScheduled = false;
      this.setCDMedico();
    },
  },
});
