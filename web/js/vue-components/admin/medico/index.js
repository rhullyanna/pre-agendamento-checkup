"use strict";

Vue.use(VueLoading);
Vue.component("loading", VueLoading);

var main = new Vue({
  el: "#doctor",
  mounted: function () {
    this.$nextTick(function () {
      $("#cpf").mask("999.999.999-99");
    });
  },
});
