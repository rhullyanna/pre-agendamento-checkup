"use strict";

Vue.use(VueLoading);
Vue.component("loading", VueLoading);

var app = new Vue({
  el: "#paciente",
  data: {
    urlValidateData: $().getUrl() + "/paciente/cadastro/validatepostdata",
    urlID: $().getUrl() + "/webservices/loadtasyuserdata",
    id: document.getElementById("id").value,
    cpf: document.getElementById("cpf").value,
    patient_id: document.getElementById("cd_pessoa_fisica").value,
    imageDocument: document.getElementById("docImageHidden").value,
    errorsList: [],
    imageError: false,
    imageMessage: "",
  },
  methods: {
    processFileDocument(event) {
      this.imageError = false;
      this.imageMessage = "";
      var input = event.target;
      if (input.files && input.files[0]) {
        let format = input.files[0].name.match(/\.(jpg|jpeg|png)$/);
        if (format == null) {
          this.imageError = true;
          this.imageMessage = "Formato inválido para upload.";
          return;
        }
        var reader = new FileReader();
        reader.onload = (e) => {
          this.imageDocument = e.target.result;
          document.getElementById("docImageHidden").value = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
      }
    },

    submit: function () {
      this.errorsList = [];
      let formData = new FormData(document.querySelector("#paciente-form"));

      var myInit = {
        method: "POST",
        mode: "cors",
        credentials: "same-origin",
        body: formData,
      };
      fetch(this.urlValidateData, myInit)
        .then((response) => {
          if (response.ok) {
            response.json().then((obj) => {
              if (obj.success) {
                document.querySelector("#paciente-form").submit();
              } else {
                this.errorsList = obj.errors;
                $("html, body").animate(
                  { scrollTop: $("#errorsForm").position().top },
                  "slow"
                );
              }
            });
          }
        })
        .catch((error) => console.log("Error:", error));
    },

    getIdFromTasy: function (pcpf) {
      if (pcpf) {
        let cleancpf = pcpf.replace(/\D/g, "");
        var url = new URL(this.urlID);
        url.search = new URLSearchParams({ cpf: cleancpf });

        var myInit = {
          method: "GET",
          mode: "cors",
          cache: "default",
        };
        fetch(url, myInit).then((response) => {
          if (response.ok) {
            response.json().then((user) => {
              this.patient_id = user.cd_pessoa_fisica;
            });
          }
        });
      }
    },
  },

  mounted: function () {
    this.$nextTick(function () {
      $("#cpf").mask("999.999.999-99");
      $("#validade_convenio").mask("99/99/9999");
    });
  },
  watch: {
    cpf: function (val) {
      if (val.length == 14) {
        this.getIdFromTasy(val);
      }
    },
  },
});
