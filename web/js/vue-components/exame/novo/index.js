"use strict";

Vue.use(VueLoading);
Vue.component("loading", VueLoading);

var main = new Vue({
  el: "#exame",
  data: {
    urlInsurances: $().getUrl() + "/webservices/loadtasyinsurancedata",
    urlExamTypes: $().getUrl() + "/webservices/loadtasyexamtypedata",
    urlExams: $().getUrl() + "/webservices/loadtasyexamdata",
    urlPlans: $().getUrl() + "/webservices/loadtasyplandata",
    urlCats: $().getUrl() + "/webservices/loadtasycatdata",
    urlCalendar: $().getUrl() + "/webservices/loadexamcalendar",
    urlDoctor: $().getUrl() + "/webservices/loadexamdoctordata",
    urlUserData: $().getUrl() + "/webservices/loaduserdata",
    urlUserInsuranceData: $().getUrl() + "/webservices/loaduserinsurancedata",
    urlValidateData:
      $().getUrl() + "/paciente/agendamentoexame/validatepostdata",
    urlView: $().getUrl() + "/paciente/agendamentoexame/view",
    imageInsurance: document.getElementById("convImageHidden").value,
    medicalRequest: document.getElementById("medImageHidden").value,
    id: document.getElementById("id").value,
    duration: document.getElementById("qt_duracao").value,
    insuranceList: [],
    examList: [],
    planList: [],
    catList: [],
    doctorList: [],
    dates: [],
    withInsurance: "",
    insurance: document.getElementById("insuranceField").value,
    userInsurance: { convenio_id: "" },
    exam: "",
    plan: "",
    cat: "",
    cd_agenda: document.getElementById("doctorField").value,
    personalData: "",
    calendarLoaded: false,
    calendarError: false,
    calendarErrorMessage: "",
    notFound: false,
    notFoundMessage: "",
    imageError: false,
    imageMessage: "",
    examScheduled: "",
    isExamScheduled: false,
    scheduleError: false,
    scheduleErrorMessage: "",
    user: {},
    erroForm: false,
    errorsList: [],
    disabledByInsurance: false,
    orientations: null,
  },

  methods: {
    loadInsurance() {
      let loader = this.$loading.show();
      var myInit = { method: "GET", mode: "cors", cache: "default" };
      fetch(this.urlInsurances, myInit)
        .then((response) => {
          if (response.ok) {
            response.json().then((list) => {
              if (list.length > 0) {
                this.insuranceList = list.filter((i) => i.id != 50);
              }
            });
          }
        })
        .finally(() => {
          loader.hide();
        });
    },

    loadPlan() {
      var url = new URL(this.urlPlans);
      url.search = new URLSearchParams({
        convenio: this.insurance,
      });
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };

      var myInit = { method: "GET", mode: "cors", cache: "default" };
      fetch(url, myInit).then((response) => {
        if (response.ok) {
          response.json().then((list) => {
            if (list.length > 0) {
              this.planList = list;
            }
          });
        }
      });
    },

    loadCat() {
      var url = new URL(this.urlCats);
      url.search = new URLSearchParams({
        convenio: this.insurance,
      });
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };

      var myInit = { method: "GET", mode: "cors", cache: "default" };
      fetch(url, myInit).then((response) => {
        if (response.ok) {
          response.json().then((list) => {
            if (list.length > 0) {
              this.catList = list;
            }
          });
        }
      });
    },

    setConvenio() {
      if (this.insurance != "") {
        $("#convenio_tasy_nome").val(
          $("#insuranceField option:selected").text()
        );
      } else {
        $("#convenio_tasy_nome").val("");
      }
    },

    disableExamField() {
      return this.disabledByInsurance || this.withInsurance === "";
    },

    loadExam() {
      this.setConvenio();
      this.loadPlan();
      this.loadCat();
      this.calendarError = false;
      this.calendarErrorMessage = "";
      this.disabledByInsurance = false;

      let loader = this.$loading.show();
      var url = new URL(this.urlExams);
      url.search = new URLSearchParams({
        convenio: this.insurance,
      });
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };
      var self = this;

      fetch(url, myInit)
        .then((response) => {
          if (response.ok) {
            response.json().then((list) => {
              this.examList = list;
            });
          }
        })
        .finally(() => {
          loader.hide();
        });
    },

    loadDoctor: function () {
      let loader = this.$loading.show();
      var url = new URL(this.urlDoctor);
      url.search = new URLSearchParams({ exame: this.exam });
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };
      return Promise.resolve(
        fetch(url, myInit)
          .then((response) => {
            if (response.ok) {
              response.json().then((list) => {
                this.doctorList = list;
              });
            }
          })
          .finally(() => {
            loader.hide();
          })
      );
    },

    loadCalendar: function () {
      this.calendarError = false;

      if (
        this.checkInsurance == "" ||
        this.exam == "" ||
        this.exam == "0" ||
        this.cd_agenda == ""
      ) {
        this.calendarError = true;
        this.calendarErrorMessage =
          "É necessário informar o Tipo de Convênio, o exame e o médico(a)";
        return;
      }

      var url = new URL(this.urlCalendar);
      url.search = new URLSearchParams({
        cd_agenda: this.cd_agenda,
      });

      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };

      let loader = this.$loading.show();
      fetch(url, myInit).then((response) => {
        if (response.ok) {
          response
            .json()
            .then((list) => {
              loader.hide();
              if (list.length == 0) {
                this.calendarError = true;
                this.calendarErrorMessage =
                  "Nenhum horário encontrado com estes parâmetros";
                return;
              }
              this.calendarLoaded = true;
              this.dates = list;
              this.displayCalendar();
              document.getElementById("fieldsetCalendar").scrollIntoView(true);
            })
            .catch((error) => {
              loader.hide();
              console.log(error);
            });
        }
      });
      ("");
    },

    displayCalendar: function () {
      var self = this;
      var calendarEl = document.getElementById("calendar");
      calendarEl.innerHTML = "";
      this.examScheduled = "";
      this.isExamScheduled = false;
      this.scheduleError = false;

      var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ["dayGrid", "timeGrid"],
        height: "auto",
        defaultView: "timeGrid",
        locale: "pt-br",
        header: {
          left: "",
          center: "title",
          right: "",
        },
        allDaySlot: false,
        minTime: "07:00:00",
        maxTime: "17:51:00",
        hiddenDays: [0],
        displayEventTime: false,
        visibleRange: function (currentDate) {
          var startDate = new Date(currentDate.valueOf());
          var endDate = new Date(currentDate.valueOf());

          startDate.setDate(startDate.getDate() + 2);
          endDate.setDate(endDate.getDate() + 7);
          return { start: startDate, end: endDate };
        },
        events: this.dates,

        eventClick: function (info) {
          var d = self.formatDate(info.event.start, "dd/MM/yyyy hh:mm:ss");
          if (
            confirm(
              "Horário Selecionado: " + info.event.extendedProps.description
            )
          ) {
            $(".fc-event").removeClass("selected-event");
            self.examScheduled = d;
            self.duration = info.event.extendedProps.duration;
            document.getElementById("seqagendaHidden").value =
              info.event.extendedProps.sequence;
            self.isExamScheduled = true;
            setTimeout(() => {
              document.getElementById("horario").scrollIntoView({
                behavior: "smooth",
                block: "center",
                inline: "start",
              });
            }, 500);
            info.el.classList.add("selected-event");
          }
        },
      });
      setTimeout(() => {
        calendar.render();
        if (this.dates.length > 100) {
          calendar.contentHeight = 800;
        }
      }, 500);
    },

    formatDate: function (x, y) {
      var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds(),
      };
      y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
        return ((v.length > 1 ? "0" : "") + eval("z." + v.slice(-1))).slice(-2);
      });

      return y.replace(/(y+)/g, function (v) {
        return x.getFullYear().toString().slice(-v.length);
      });
    },

    loadUserInsuranceData: function () {
      var self = this;
      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };
      fetch(this.urlUserInsuranceData, myInit).then((response) => {
        if (response.ok) {
          response.json().then((userInsurance) => {
            if (
              self.userInsurance.convenio_id != "" &&
              userInsurance.convenio_id == self.userInsurance.convenio_id
            ) {
              this.userInsurance = userInsurance;
            }
          });
        }
      });
    },

    checkInsurance: function (event) {
      if (event.target.value) {
        this.withInsurance = event.target.value == "1";
      } else {
        this.withInsurance = "";
      }
    },

    processFileMedical(event) {
      this.imageError = false;
      this.imageMessage = "";
      var input = event.target;
      if (input.files && input.files[0]) {
        let format = input.files[0].name.match(/\.(jpg|jpeg|png|pdf)$/);
        if (format == null) {
          this.imageError = true;
          this.imageMessage = "Formato inválido para upload.";
          return;
        }
        var reader = new FileReader();
        reader.onload = (e) => {
          this.medicalRequest = e.target.result;
          document.getElementById("medImageHidden").value = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
      }
    },

    processFileInsurance(event) {
      this.imageError = false;
      this.imageMessage = "";
      var input = event.target;
      if (input.files && input.files[0]) {
        let format = input.files[0].name.match(/\.(jpg|jpeg|png)$/);
        if (format == null) {
          this.imageError = true;
          this.imageMessage = "Formato inválido para upload.";
          return;
        }
        var reader = new FileReader();
        reader.onload = (e) => {
          this.imageInsurance = e.target.result;
          document.getElementById("convImageHidden").value = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
      }
    },

    validate: function () {
      this.errorsForm = false;

      let formData = new FormData(
        document.querySelector("#agendamento-exame-form")
      );

      var myInit = {
        method: "POST",
        mode: "cors",
        credentials: "same-origin",
        body: formData,
      };
      let loader = this.$loading.show();
      fetch(this.urlValidateData, myInit)
        .then((response) => {
          if (response.ok) {
            response.json().then((obj) => {
              if (obj.success) {
                var urlView = new URL(this.urlView);
                urlView.search = new URLSearchParams({
                  id: obj.id,
                });

                window.location = urlView;
              } else {
                loader.hide();
                this.erroForm = true;
                this.errorsList = obj.errors;
                $("html, body").animate(
                  { scrollTop: $("#errorsForm").position().top },
                  "slow"
                );
              }
            });
          }
        })
        .catch((error) => console.log("Error:", error));
    },

    setCDMedico: function () {
      let item = this.doctorList.find((d) => d.id == this.cd_agenda);
      if (item !== undefined) {
        $("#cd_medico").val(item.cd_pessoa_fisica);
        $("#nome_medico").val(item.descricao);

        this.orientations = item.orientacao;
      } else {
        $("#cd_medico").val("");
      }
    },

    setExamInfo: function () {
      let item = this.examList.find((d) => d.id == this.exam);
      if (item !== undefined) {
        $("#cdprocedimentoHidden").val(item.cd_procedimento);
        $("#descricaoHidden").val(item.descricao);
        $("#ieorigemHidden").val(item.ie_origem);
        $("#seqexameHidden").val(item.seq_exame);
        this.loadDoctor();
      } else {
        $("#descricaoHidden").val("");
        $("#cdprocedimentoHidden").val("");
        $("#ieorigemHidden").val("");
        $("#seqexameHidden").val("");
      }
    },
  },

  mounted: function () {
    this.$nextTick(function () {
      $("#validade_convenio").mask("99/99/9999");
      $("#data_nascimento").mask("99/99/9999");
    });
  },

  watch: {
    withInsurance: function (value) {
      this.calendarLoaded = false;
      this.exam = "";
      this.cd_agenda = "";
      this.plan = "";
      this.cat = "";
      this.examScheduled = "";
      this.isExamScheduled = false;
      this.examList = [];
      this.doctorList = [];
      $("#examField").val("");
      $("#nome_medico").val("");

      this.duration = "";
      if (value == "1") {
        this.loadInsurance();
      } else if (value == "0") {
        this.userInsurance = { convenio_id: "" };
        this.insurance = "";
        this.imageInsurance = "";
        document.getElementById("numeroconvenio").value = "";
        document.getElementById("validade_convenio").value = "";
        document.getElementById("convImageHidden").value = "";
        this.loadExam();
      }
    },
    insurance: function (value) {
      let ins = this.insuranceList.find(
        (x) => x.id == value && x.nr_carteirinha != null
      );
      if (ins != undefined) {
        $("#nr_carteirinha").val(ins.nr_carteirinha);
        $("#numero_convenio").attr(
          "placeholder",
          `Número contendo ${ins.nr_carteirinha} dígitos`
        );
      } else {
        $("#numero_convenio").removeAttr("placeholder");
      }

      this.userInsurance.convenio_id = value;
      this.calendarLoaded = false;
      this.isExamScheduled = false;
      this.exam = "";
      this.cd_agenda = "";
      this.plan = "";
      this.cat = "";
      this.orientations = null;
      $("#examField").val("");
      this.duration = "";
      this.examScheduled = "";
      this.doctorList = [];
    },

    exam: function (value) {
      this.calendarLoaded = false;
      this.isExamScheduled = false;
      this.cd_agenda = "";
      this.examScheduled = "";
      $("#nome_medico").val("");
    },
    cd_agenda: function (value) {
      this.calendarLoaded = false;
      this.examScheduled = "";
      this.isExamScheduled = false;
      this.setCDMedico();
    },
  },
});
