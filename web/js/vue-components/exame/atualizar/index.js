"use strict";

Vue.use(VueLoading);
Vue.component("loading", VueLoading);

new Vue({
  el: "#exame",
  data: {
    urlCalendar: $().getUrl() + "/webservices/loadtasydoctorcalendar",
    id: document.getElementById("id").value,
    dates: [],
    exam: document.getElementById("nr_seq_proc_interno").value,
    agenda: document.getElementById("cd_agenda").value,
    time: document.getElementById("horario").value,
    calendarLoaded: false,
    calendarError: false,
    calendarErrorMessage: "",
  },

  methods: {
    loadCalendar: function () {
      var url = new URL(this.urlCalendar);
      url.search = new URLSearchParams({
        cd_agenda: this.agenda,
      });

      var myInit = {
        method: "GET",
        mode: "cors",
        cache: "default",
      };

      let loader = this.$loading.show();
      fetch(url, myInit).then((response) => {
        if (response.ok) {
          response.json().then((list) => {
            loader.hide();
            if (list.length == 0) {
              this.calendarError = true;
              this.calendarErrorMessage =
                "Nenhum horário encontrado com estes parâmetros";
              return;
            }
            this.calendarLoaded = true;
            this.dates = list;
            this.displayCalendar();
          });
        }
      });
      ("");
    },

    displayCalendar: function () {
      var self = this;
      var calendarEl = document.getElementById("calendar");
      calendarEl.innerHTML = "";

      var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ["dayGrid", "timeGrid"],
        height: "auto",
        defaultView: "timeGrid",
        locale: "pt-br",
        header: {
          left: "",
          center: "title",
          right: "",
        },
        allDaySlot: false,
        minTime: "08:00:00",
        maxTime: "16:51:00",
        visibleRange: function (currentDate) {
          var startDate = new Date(currentDate.valueOf());
          var endDate = new Date(currentDate.valueOf());

          startDate.setDate(startDate.getDate() + 2);
          endDate.setDate(endDate.getDate() + 7);
          return { start: startDate, end: endDate };
        },
        events: this.dates,

        eventClick: function (info) {
          var d = self.formatDate(info.event.start, "dd/MM/yyyy hh:mm:ss");
          self.time = d;
          alert("Horário Selecionado: " + self.time);
        },
      });
      setTimeout(() => {
        calendar.render();
      }, 500);
    },

    formatDate: function (x, y) {
      var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds(),
      };
      y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
        return ((v.length > 1 ? "0" : "") + eval("z." + v.slice(-1))).slice(-2);
      });

      return y.replace(/(y+)/g, function (v) {
        return x.getFullYear().toString().slice(-v.length);
      });
    },
  },
  mounted: function () {
    this.$nextTick(function () {
      this.loadCalendar();
    });
  },
});
