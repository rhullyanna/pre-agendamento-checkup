/*!
 * Created by Patrick Tapajós - patrick.tapajos@pmm.am.gov.br
 * Date: 08/05/2017
 * Description: File with a function for SGM to set url by hostname to be consumed in vue components
 */

let nome_sistema = "yiibasicapp";

(function($) {
  $.fn.getUrl = function() {
    var url = document.location.hostname;

    if (nome_sistema.indexOf(url) != -1) {
      url = "http://" + url + ":8000";
    } else if ("localhost".indexOf(url) != -1) {
      url = "http://" + url + ":8000/" + nome_sistema + "";
    } else {
      url = "http://" + url;
    }
    return url;
  };
})(jQuery);
